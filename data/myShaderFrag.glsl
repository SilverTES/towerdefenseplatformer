#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D tex; // 0
uniform vec2 center; // Mouse position
uniform float time; // effect elapsed time
uniform vec3 shockParams; // 10.0, 0.8, 0.1

uniform vec2 surface; // texture resolution

void main()
{
    vec2 uv = gl_TexCoord[0].xy;
    vec2 texCoord = uv;

    //float square = min(surface.x,surface.y);
    //vec2 position = vec2(center.x/square, center.y/square);

    vec2 position = center/surface;
    float dist = distance(uv, position);

    //float dist = distance(vec2(uv.x,uv.y), vec2(position.x,position.y));

    if ((dist <= (time + shockParams.z)) && (dist >= (time - shockParams.z)) )
    {
        float diff = (dist - time);
        float powDiff = 1.0 - pow(abs(diff*shockParams.x), shockParams.y);
        float diffTime = diff  * powDiff;
        vec2 diffUV = normalize(uv - position);
        texCoord = uv + (diffUV * diffTime);
    }

    gl_FragColor = texture2D(tex, texCoord);
}

