#include "MyGame.h"

#define CREATE_CLIP(poolClip, name) poolClip[name] = new Clip(name);

int MyGame::makeClip()
{

    // Declare main clips here !
    // Attention : don't declare clone clips here !

    // Root
    CREATE_CLIP(_clip,"root");


    // GUI
    CREATE_CLIP(_clip,"window");
    CREATE_CLIP(_clip,"toolTip");
    CREATE_CLIP(_clip,"playerHUD");
    CREATE_CLIP(_clip,"layerMenu");

    CREATE_CLIP(_clip,"configGamePad");
    CREATE_CLIP(_clip,"configPlayer");

    // Transition Clip
    CREATE_CLIP(_clip,"door");

    // GameState
    CREATE_CLIP(_clip,"gameState");

    CREATE_CLIP(_clip,"title");
    CREATE_CLIP(_clip,"credits");
    CREATE_CLIP(_clip,"saveScreen");
    CREATE_CLIP(_clip,"inGame");
    CREATE_CLIP(_clip,"options");
    CREATE_CLIP(_clip,"selectStage");

    // GamePlay
    CREATE_CLIP(_clip,"camera");
    CREATE_CLIP(_clip,"layer0");
    CREATE_CLIP(_clip,"layer1");
    CREATE_CLIP(_clip,"layerFG");
    CREATE_CLIP(_clip,"layerHUD");
    CREATE_CLIP(_clip,"tilemap0");
    CREATE_CLIP(_clip,"tilemapFG");

    // Player
    CREATE_CLIP(_clip,"hero");

    // Item
    CREATE_CLIP(_clip,"laser");
    CREATE_CLIP(_clip,"canonShot");
    CREATE_CLIP(_clip,"canon");
    CREATE_CLIP(_clip,"exit");
    CREATE_CLIP(_clip,"ammo");
    CREATE_CLIP(_clip,"portal");
    CREATE_CLIP(_clip,"teleport");
    CREATE_CLIP(_clip,"atom");

    // SFX
    CREATE_CLIP(_clip,"score");
    CREATE_CLIP(_clip,"explosion");
    CREATE_CLIP(_clip,"warning");
    CREATE_CLIP(_clip,"nextWave");
    CREATE_CLIP(_clip,"laserLine");

    // Enemy
    CREATE_CLIP(_clip,"enemy");


    // Stage
    CREATE_CLIP(_clip,"stage");

    makeClipGui(); std::cout << "Create Gui OK !\n";
    makeClipRoot(); std::cout << "Create Root OK !\n";
    makeClipInGame(); std::cout << "Create inGame OK !\n";
    makeClipEnemy(); std::cout << "Create Enemy OK !\n";
    makeClipSFX(); std::cout << "Create SFX OK !\n";
    makeClipItem(); std::cout << "Create Item OK !\n";
    makeClipPlayer(); std::cout << "Create Player OK !\n";
    makeClipStage(); std::cout << "Create Stage OK !\n";

    return mlog(" Make Clip OK ! \n",0);
}
