#include "MyGame.h"

void MyGame::makeClipItem()
{
    _clip["laser"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _asset->GET_FONT("gameFont"), _mouse)
    ->setType(CLIP_ARM)
    ->setActive(true)
    ->setSize(8,4)
    ->setPivot(4,2)
    ->setPosition(0,0,3)
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,8,8})
    //->setNumber("direction",0)
    //->attach<Component::Velocity>()
    ->attach(new Component::Velocity())
    ->setNumber("life",24)
    ->setNumber("damage",4)
    ->setNumber("ownerID", -1)
    ->UPDATE(
    {
        _X += _VELOCITY->_vx;
        _Y += _VELOCITY->_vy;

        if (std::abs(_VELOCITY->_vx) < _VELOCITY->_vxMax)
        {
            _VELOCITY->_vx += Misc::sign(_VELOCITY->_vx) * _VELOCITY->_ax;
        }

        if (std::abs(_VELOCITY->_vy) < _VELOCITY->_vyMax)
        {
            _VELOCITY->_vy += Misc::sign(_VELOCITY->_vy) * _VELOCITY->_ay;
        }

        _THIS->updateCollideZone(0,_RECT);

        ON_FRAME(_NUMBER("life"))
        {
            KILL_THIS();
        }
        GOTO_NEXT_FRAME();


        ON_COLLIDE_ZONE(0)
        {
            ON_COLLIDE_ZONE_CLIP_TYPE(0,CLIP_ENEMY,0)
            {
                _msgQueue->post(DAMAGE, new MsgData::Damage(_NUMBER("damage"),_VELOCITY->_vx), _PARENT->index(_ID_ZONE_COLLIDE_BY(0)) );

                al_play_sample(_asset->GET_SAMPLE("gunShot"), 0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                //_msgQueue->post(SCORE, new MsgData::Score(10), _PARENT->index(_NUMBER("ownerID")));

                KILL_THIS();
            }

            ON_COLLIDE_ZONE_CLIP_TYPE(0,CLIP_PLAYER,0)
            {
                if (_ID_ZONE_COLLIDE_BY(0) != _NUMBER("ownerID"))
                {
                    _msgQueue->post(HEAL, new MsgData::Heal(4), _PARENT->index(_ID_ZONE_COLLIDE_BY(0)) );

                    al_play_sample(_asset->GET_SAMPLE("gunShot"), 0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                    //_msgQueue->post(SCORE, new MsgData::Score(10), _PARENT->index(_NUMBER("ownerID")));

                    KILL_THIS();
                }

            }

        }

        // Test Collide Wall

        int posMapX = std::floor((_X+24)/_clip["tilemap0"]->TILEMAP2D->_tileW);
        int posMapY = std::floor(_Y/_clip["tilemap0"]->TILEMAP2D->_tileH);

        if (_clip["tilemap0"]->TILEMAP2D->getTileCollidable(posMapX-1, posMapY))
            if (Collision2D::pointRect(Vec2{_X,_Y}, _clip["tilemap0"]->TILEMAP2D->getTileRect(posMapX-1,posMapY, 8)) )
                KILL_THIS();

        if (_clip["tilemap0"]->TILEMAP2D->getTileCollidable(posMapX+1, posMapY))
            if (Collision2D::pointRect(Vec2{_X,_Y}, _clip["tilemap0"]->TILEMAP2D->getTileRect(posMapX+1,posMapY, 8)) )
                KILL_THIS();




    })
    ->RENDER(
    {
        //Draw::rectFill(_RECT, al_map_rgb(250,150,25));
        //Draw::rect(_RECT, al_map_rgb(250,10,0),0);

        //al_draw_filled_ellipse(_ABSX+_OX, _ABSY+_OY,8,4, al_map_rgba(250,250,250,50));
        al_draw_bitmap(_asset->GET_BITMAP("laser"), _ABSX-4, _ABSY-3,0);


//            DRAW_TEXTF
//            (
//                al_map_rgb(50,250,100),
//                _ABSX+_OX, _ABSY - 8,
//                -1,
//                "%i",
//                //_COMMAND->_player->_name.c_str(),
//                _NUMBER("ownerID")
//            );

        _THIS->showRect(al_map_rgba(115,120,0,50))
        ->showPivot(al_map_rgb(255,0,255),2);
    });
    //};

    _clip["canonShot"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _asset->GET_FONT("gameFont"), _mouse)
    ->setType(CLIP_ARM)
    ->setActive(true)
    ->setSize(8,4)
    ->setPivot(4,2)
    ->setPosition(0,0,3)
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,8,8})
    //->setNumber("direction",0)
    //->attach<Component::Velocity>()
    ->attach(new Component::Velocity())
    ->VELOCITY->setVAX(.2)
    ->VELOCITY->setVAY(.2)
    ->setNumber("life",24)
    ->setNumber("damage",4)
    ->setNumber("level",1)
    ->setNumber("targetID",-1)
    ->UPDATE(
    {

        if (_NUMBER("targetID") > -1)
        {
            if (nullptr != _PARENT->index(_NUMBER("targetID")))
            {
                if (_PARENT->index(_NUMBER("targetID"))->_type == CLIP_ENEMY)
                {
                    _VELOCITY->_vecX = _PARENT->index(_NUMBER("targetID"))->_x - _X;
                    _VELOCITY->_vecY = _PARENT->index(_NUMBER("targetID"))->_y - _Y;
                }
                else
                {
                    KILL_THIS(); // if target is not enemy kill this shot !
                }
            }
            else
            {
                KILL_THIS();
            }
        }
        else
        {
            KILL_THIS(); // if target dead or null kill this shot !
        }

        if (std::abs(_VELOCITY->_ax) < _VELOCITY->_vxMax)
        {
            _VELOCITY->_ax += _VELOCITY->_vax;
            //_VELOCITY->_vx += Misc::sign(_VELOCITY->_vx) * _VELOCITY->_ax;
            _VELOCITY->_vx = std::cos(-std::atan2(_VELOCITY->_vecX, _VELOCITY->_vecY) + Misc::Rad90) * _VELOCITY->_ax;

        }

        if (std::abs(_VELOCITY->_ay) < _VELOCITY->_vyMax)
        {
            _VELOCITY->_ay += _VELOCITY->_vay;
            //_VELOCITY->_vy += Misc::sign(_VELOCITY->_vy) * _VELOCITY->_ay;
            _VELOCITY->_vy = std::sin(-std::atan2(_VELOCITY->_vecX, _VELOCITY->_vecY) + Misc::Rad90) * _VELOCITY->_ay;

        }

        _X += _VELOCITY->_vx;
        _Y += _VELOCITY->_vy;




        _THIS->updateCollideZone(0,_RECT);

        ON_FRAME(_NUMBER("life"))
        {
            KILL_THIS();
        }
        GOTO_NEXT_FRAME();


        ON_COLLIDE_ZONE(0)
        {
            ON_COLLIDE_ZONE_CLIP_NAME(0,"miniAlien",0)
            {
                if (_ID_ZONE_COLLIDE_BY(0) == _NUMBER("targetID")) // Check if target have same ID of canon target
                {
                    _msgQueue->post(DAMAGE, new MsgData::Damage(_NUMBER("damage"),0), _PARENT->index(_ID_ZONE_COLLIDE_BY(0)) );
                    al_play_sample(_asset->GET_SAMPLE("laserBlast"), 0.1, 0.0, 3.0*_NUMBER("level"), ALLEGRO_PLAYMODE_ONCE, NULL);
                    KILL_THIS();
                }
            }
            ON_COLLIDE_ZONE_CLIP_NAME(0,"maxiAlien",0)
            {
                if (_ID_ZONE_COLLIDE_BY(0) == _NUMBER("targetID")) // Check if target have same ID of canon target
                {
                    _msgQueue->post(DAMAGE, new MsgData::Damage(_NUMBER("damage"),0), _PARENT->index(_ID_ZONE_COLLIDE_BY(0)) );
                    al_play_sample(_asset->GET_SAMPLE("laserBlast"), 0.1, 0.0, 3.0*_NUMBER("level"), ALLEGRO_PLAYMODE_ONCE, NULL);
                    KILL_THIS();
                }
            }

            ON_COLLIDE_ZONE_CLIP_NAME(0,"destructor",0)
            {
                if (_ID_ZONE_COLLIDE_BY(0) == _NUMBER("targetID")) // Check if target have same ID of canon target
                {
                    _msgQueue->post(DAMAGE, new MsgData::Damage(_NUMBER("damage"),0), _PARENT->index(_ID_ZONE_COLLIDE_BY(0)) );
                    al_play_sample(_asset->GET_SAMPLE("laserBlast"), 0.1, 0.0, 3.0*_NUMBER("level"), ALLEGRO_PLAYMODE_ONCE, NULL);
                    KILL_THIS();
                }
            }
        }


    })
    ->setRender(CODE
    {
        //Draw::rectFill(_RECT, al_map_rgb(250,150,25));
        //Draw::rect(_RECT, al_map_rgb(250,10,0),0);

        VAR lx = _VELOCITY->_vx*4;
        VAR ly = _VELOCITY->_vy*4;

        if (_NUMBER("level") == 1)
        {
            //al_draw_filled_circle(_ABSX+_OX, _ABSY+_OY,1, al_map_rgb(20,250,50));
            Draw::lineAA
            (
                _ABSX+_OX, _ABSY+_OY,
                _ABSX+_OX-lx, _ABSY+_OY-ly,
                RGBA{20,250,50,255},
                1
            );
        }

        if (_NUMBER("level") == 2)
        {
            //al_draw_filled_circle(_ABSX+_OX, _ABSY+_OY,2, al_map_rgb(250,50,50));
            Draw::lineAA
            (
                _ABSX+_OX, _ABSY+_OY,
                _ABSX+_OX-lx, _ABSY+_OY-ly,
                RGBA{250,50,50,255},
                2
            );
        }

        if (_NUMBER("level") == 3)
        {
            //al_draw_filled_circle(_ABSX+_OX, _ABSY+_OY,3, al_map_rgb(250,250,50));
            Draw::lineAA
            (
                _ABSX+_OX, _ABSY+_OY,
                _ABSX+_OX- lx, _ABSY+_OY-ly,
                RGBA{250,250,50,255},
                3
            );
        }

        //al_draw_bitmap(_asset->GET_BITMAP("laser"), _ABSX-4, _ABSY-3,0);

        _THIS->showRect(al_map_rgba(115,120,0,50))
        ->showPivot(al_map_rgb(255,0,255),2);
    });
    //};

    _clip["canon"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _asset->GET_FONT("gameFont"), _mouse)
    ->setType(CLIP_BUILDING)
    //->setActive(true)
    //->setPosition(48+12,120+12,2)
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,24,24})
    ->setCollideZone(1,Rect{0,0,24,24})
    //->attach(new Component::Draggable())

    ->setNumber("level",1)
    ->setNumber("id",-1)
    ->setNumber("delay",0)
    ->setNumber("repair",0)
    ->setNumber("alarm",100)
    ->setNumber("warning",0)
    ->setNumber("warningID",-1)
    ->setNumber("showInfo",0)

    ->setPivot(12,12)
    ->setSize(24,24)

    //->attach<Component::Jumper>()
    ->attach(new Component::Jumper())
    ->JUMPER->setGravity(0.35)
    ->JUMPER->setSpeedMax(2,8)
    ->JUMPER->setTileMap(_clip["tilemap0"]->TILEMAP2D)

    //->attach<Component::Unit>()
    ->attach(new Component::Unit())
    ->UNIT->setEnergy(80)
    ->UNIT->setEnergyBar(20)
    ->UNIT->setEnergyColor(al_map_rgb(250,250,0))
    ->UNIT->setEnergyColorBG(al_map_rgb(250,0,0))
    ->UPDATE(
    {
        _JUMPER->update();
        _UNIT->update();
        //_DRAGGABLE->update();

        //_THIS->updateCollideZone(0,_RECT);
        if (_NUMBER("level") == 1) _THIS->updateCollideZone(1, Rect{_X-80,_Y-80,160,160});
        if (_NUMBER("level") == 2) _THIS->updateCollideZone(1, Rect{_X-100,_Y-80,200,160});
        if (_NUMBER("level") == 3) _THIS->updateCollideZone(1, Rect{_X-120,_Y-80,240,160});

        _THIS->updateCollideZone(0,_RECT);

        _NUMBER("showInfo") = 0;

        ON_COLLIDE_ZONE(0)
        {
            ON_COLLIDE_ZONE_CLIP_NAME(0,"miniAlien",0)
            {

                //printf("Enemy Detected : %i \n", _ID_ZONE_COLLIDE_BY(0));
//                    if (_NUMBER("id") < 0)
//                        _NUMBER("id") = _ID_ZONE_COLLIDE_BY(0);
                Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));
                if (nullptr != clip)
                {

                    al_play_sample(_asset->GET_SAMPLE("towerHit"), 0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    _UNIT->_energy -= clip->_mapNumber["level"]*2;
                    //_msgQueue->post(DEAD, new MsgData::Dead(), clip);

                    if (nullptr != clip->JUMPER)
                    {
                        if (_X < clip->_x)
                            clip->JUMPER->_vx =  clip->JUMPER->_moveX;
                        else
                            clip->JUMPER->_vx = -clip->JUMPER->_moveX;
                    }
                }


            }

            ON_COLLIDE_ZONE_CLIP_NAME(0,"maxiAlien",0)
            {
                //printf("Enemy Detected : %i \n", _ID_ZONE_COLLIDE_BY(0));
//                    if (_NUMBER("id") < 0)
//                        _NUMBER("id") = _ID_ZONE_COLLIDE_BY(0);
                Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));
                if (nullptr != clip)
                {
                    al_play_sample(_asset->GET_SAMPLE("towerHit"), 0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    _UNIT->_energy -= clip->_mapNumber["level"]*2;
                    //_msgQueue->post(DEAD, new MsgData::Dead(), clip);
                    if (nullptr != clip->JUMPER)
                    {
                        if (_X < clip->_x)
                            clip->JUMPER->_vx = clip->JUMPER->_moveX;
                        else
                            clip->JUMPER->_vx = -clip->JUMPER->_moveX;
                    }
                }
            }

            ON_COLLIDE_ZONE_CLIP_NAME(0,"destructor",0)
            {
                //printf("Enemy Detected : %i \n", _ID_ZONE_COLLIDE_BY(0));
//                    if (_NUMBER("id") < 0)
//                        _NUMBER("id") = _ID_ZONE_COLLIDE_BY(0);
                Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));
                if (nullptr != clip)
                {
                    //al_play_sample(_asset->GET_SAMPLE("towerHit"), 0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    _UNIT->_energy = 0;
                    //_msgQueue->post(DEAD, new MsgData::Dead(), clip);
                    if (nullptr != clip->JUMPER)
                    {
                        if (_X < clip->_x)
                            clip->JUMPER->_vx = clip->JUMPER->_moveX;
                        else
                            clip->JUMPER->_vx = -clip->JUMPER->_moveX;
                    }
                }
            }

            ON_COLLIDE_ZONE_CLIP_TYPE(0,CLIP_PLAYER,0)
            {
                Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));
                if (nullptr != clip)
                {
                    if (clip->COMMAND->onButton(PAD_UP) && !_UNIT->_isBuild)
                    {
                        if (clip->HERO->_nbAmmo > 0 &&
                            _UNIT->_energy < _UNIT->_energyMax)
                        {
                            _UNIT->_energy += 8;
                            if (_UNIT->_energy >= _UNIT->_energyMax)
                                _UNIT->_energy = _UNIT->_energyMax;
                            else
                                --clip->HERO->_nbAmmo;

                            if (_NUMBER("repair") == 0)
                            {
                                _NUMBER("repair") = 1;

                                al_play_sample(_asset->GET_SAMPLE("repair"), 0.4, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                            }
                        }
                        else
                        {
                            _NUMBER("repair") = 0;
                        }
                    }
                }

                _NUMBER("showInfo") = 1;
            }
            else
            {
                _NUMBER("repair") = 0;
            }

        }

        if (_NUMBER("id") < 0)
        {
            ON_COLLIDE_ZONE(1)
            {
                ON_COLLIDE_ZONE_CLIP_NAME(1,"miniAlien",0)
                {
                    //printf("Enemy Detected : %i \n", _ID_ZONE_COLLIDE_BY(0));
                    if (_NUMBER("id") < 0)
                        _NUMBER("id") = _ID_ZONE_COLLIDE_BY(1);
                }

                ON_COLLIDE_ZONE_CLIP_NAME(1,"maxiAlien",0)
                {
                    //printf("Enemy Detected : %i \n", _ID_ZONE_COLLIDE_BY(0));
                    if (_NUMBER("id") < 0)
                        _NUMBER("id") = _ID_ZONE_COLLIDE_BY(1);
                }

                ON_COLLIDE_ZONE_CLIP_NAME(1,"destructor",0)
                {
                    //printf("Enemy Detected : %i \n", _ID_ZONE_COLLIDE_BY(0));
                    if (_NUMBER("id") < 0)
                        _NUMBER("id") = _ID_ZONE_COLLIDE_BY(1);
                }

            }
        }

        Clip* clip = _PARENT->index(_NUMBER("id"));
        if (nullptr != clip && !_UNIT->_isBuild)
        {
            if (nullptr != clip->UNIT && clip->_type == CLIP_ENEMY)
                if (_NUMBER("delay") > 24)
                {
                        VAR vecX = clip->_x - _X;
                        VAR vecY = clip->_y - _Y;

                        MAKE_CLONE(_clip["canonShot"],"canonShotCopy")
                        ->setNumber("targetID", clip->id())
                        ->setNumber("damage",_NUMBER("level")*4)
                        ->setNumber("level",_NUMBER("level"))
                        ->setNumber("life",40+_NUMBER("level")*2)
                        ->setX(_X)
                        ->setY(_Y)
//                            ->VELOCITY->setAX(.4)
//                            ->VELOCITY->setAY(.4)
//                            ->VELOCITY->setVX(0)
//                            ->VELOCITY->setVY(0)
                        ->VELOCITY->setVecX(vecX)
                        ->VELOCITY->setVecY(vecY)
                        ->VELOCITY->setVXMax(4)
                        ->VELOCITY->setVYMax(4);

                        //clip->UNIT->_energy -= _NUMBER("level");
                        //_msgQueue->post(DAMAGE, new MsgData::Damage(_NUMBER("level")*4, 0), _PARENT->index(_NUMBER("id")) );


                    _NUMBER("delay") = 0;

                }

            if (!Collision2D::rectRect(clip->_rect, _THIS->getCollideZone(1)->_rect))
                _NUMBER("id") = -1;

            ++_NUMBER("delay");
        }
        else
        {
            _NUMBER("id") = -1; // if Mob dead , locked Id return to -1;
        }


        if (_UNIT->_energy <= 0 && !_UNIT->_isBuild)
        {
            _clip["tilemap0"]->TILEMAP2D->setTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,BUILDABLE,1);
            _clip["tilemap0"]->TILEMAP2D->setTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,EMPTY,1);
            _clip["tilemap0"]->TILEMAP2D->setTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,ID,-1);

            Clip* clip = _PARENT->index(_NUMBER("warningID"));
            if (nullptr != clip)
            {
                _PARENT->del(_NUMBER("warningID"));
            }
            _NUMBER("warningID") = -1;

            al_play_sample(_asset->GET_SAMPLE("towerkill"),
                           0.8, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

            _centerX = _ABSX + _OX;
            _centerY = _ABSY + _OY;
            _maxRadius = 0.1;
            _radius = 0.0;
            _speed = 0.01;
            _useShader = true;


            KILL_THIS();
        }



        if (_UNIT->_energy <20 && !_UNIT->_isBuild)
        {
            if (_NUMBER("warning") == 0)
                _NUMBER("warning") = 1;

            ++_NUMBER("alarm");

            if (_NUMBER("alarm") > (_UNIT->_energy*5)+4)
            {
                _NUMBER("alarm") = 0;
                al_play_sample(_asset->GET_SAMPLE("alarm"),
                               0.4, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);

            }
            //_UNIT->setEnergyColor(al_map_rgb(255,20,10));
        }
        else
        {
            //_UNIT->setEnergyColor(al_map_rgb(50,200,120));
            _NUMBER("alarm") = 100;
            _NUMBER("warning") = 0;

            Clip* clip = _PARENT->index(_NUMBER("warningID"));
            if (nullptr != clip)
            {
                _PARENT->del(_NUMBER("warningID"));
                _NUMBER("warningID") = -1;
            }

        }

        if (_NUMBER("warning") == 1 && !_UNIT->_isBuild)
        {
            _NUMBER("warning") = 2;

            Clip* clip = MAKE_CLONE(_clip["warning"], "warning")
            ->setPosition(_X, _Y,5);

            _NUMBER("warningID") = clip->_id;
        }

        if (_UNIT->_beginBuild)
        {
            _UNIT->_beginBuild = false;
            al_play_sample(_asset->GET_SAMPLE("isBuild"), 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        }

        if (_UNIT->_isBuild)
        {
            if (_UNIT->_tempoBuild == 0)
                al_play_sample(_asset->GET_SAMPLE("clock"), 0.2, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        }

        if (_UNIT->_endBuild)
        {
            _UNIT->_endBuild = false;

            if (_NUMBER("level")==1) al_play_sample(_asset->GET_SAMPLE("build"), 0.5, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            if (_NUMBER("level")==2) al_play_sample(_asset->GET_SAMPLE("build"), 0.5, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            if (_NUMBER("level")==3) al_play_sample(_asset->GET_SAMPLE("build2"), 0.6, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        }


    })
    ->RENDER(
    {
        if (_NUMBER("showInfo")==1)
        {
            al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
            Draw::rectFill(_THIS->getCollideZone(1)->_rect, al_map_rgba(0,250,250,8));
            Draw::grid(_THIS->getCollideZone(1)->_rect, 8, 8, al_map_rgba(0,250,150,8));
            al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

            al_draw_textf
            (
                _mainFont,
                al_map_rgb(205,150,0),
                _ABSX+_OX, _ABSY+_OY-24, -1,
                "%i/%i",
                _UNIT->_energy,
                _UNIT->_energyMax
            );
        }

        switch (_NUMBER("level"))
        {
            case 1:
                !_UNIT->_isBuild ?
                al_draw_bitmap(_asset->GET_BITMAP("canon"), _ABSX, _ABSY,0):
                al_draw_tinted_bitmap(_asset->GET_BITMAP("canon"),al_map_rgba(100,100,100,100), _ABSX, _ABSY,0);
                break;
            case 2:
                !_UNIT->_isBuild ?
                al_draw_bitmap(_asset->GET_BITMAP("canon2"), _ABSX, _ABSY,0):
                al_draw_tinted_bitmap(_asset->GET_BITMAP("canon2"),al_map_rgba(100,100,100,100), _ABSX, _ABSY,0);
                break;
            case 3:
                !_UNIT->_isBuild ?
                al_draw_bitmap(_asset->GET_BITMAP("canon3"), _ABSX, _ABSY,0):
                al_draw_tinted_bitmap(_asset->GET_BITMAP("canon3"),al_map_rgba(100,100,100,100), _ABSX, _ABSY,0);
                break;
            default:
                break;
        }

        if (Clip::_showClipInfo)
            Draw::rect(_THIS->getCollideZone(1)->_rect, al_map_rgba(50,250,0,25),0);

        _THIS->showRect(al_map_rgba(115,120,0,50))
        ->showPivot(al_map_rgb(255,0,255),2);

        _UNIT->render();

        // Debug
        if (Collision2D::pointRect(Vec2{_xMouse, _yMouse}, _RECT))
            _THIS->showComponent(_mainFont,_xMouse, _yMouse+32, al_map_rgb(255,25,150));

//            DRAW_TEXTF
//            (
//                al_map_rgb(220,80,40),
//                _ABSX+_OX, _ABSY-8, -1,
////                "N%i",
////                _NUMBER("level")
////                "%i",
////                _NUMBER("id")
//                "%i",
//                _NUMBER("warningID")
//            );


//            DRAW_TEXTF
//            (
//                al_map_rgb(205,150,0),
//                _ABSX+_OX, _ABSY+_OY+16, -1,
//                "isBuild : %i",
//                _UNIT->_isBuild
//            );



    });
    //};

    _clip["exit"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _mainFont, _mouse)
    //->setActive(true)
    ->setType(CLIP_BUILDING)
    ->setPosition(_screenW/2,_screenH - 52,1)
    ->setSize(48,32)
    ->setPivot(24,16)
    //->attach<Component::Unit>()
    ->attach(new Component::Unit())
    ->UNIT->setEnergy(24)
    ->UNIT->setEnergyBar(32)
    ->UNIT->setEnergyColor(al_map_rgb(250,250,150))
    ->UNIT->setEnergyColorBG(al_map_rgb(250,20,15))
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,32,32})

    ->setNumber("inBank", 0)
    ->UPDATE(
    {
        _THIS->updateCollideZone(0,_RECT);

        ON_MESSAGE()
        {
            ON_MESSAGE_TYPE(DAMAGE)
            {
                int damage = static_cast<MsgData::Damage*>(_MESSAGE->_data)->_damage;
                _UNIT->_energy -= damage;
                al_play_sample(_asset->GET_SAMPLE("alert"),
                               0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                _centerX = _ABSX + _OX;
                _centerY = _ABSY + _OY;
                _maxRadius = 0.8;
                _radius = 0.0;
                _speed = 0.01;
                _useShader = true;

                KILL_MESSAGE();
            }
        }

        ON_COLLIDE_ZONE_CLIP_TYPE(0, CLIP_PLAYER,0)
        {
            Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));
            if (nullptr != clip)
            {
                if (clip->COMMAND->onButton(PAD_UP)) // Add to bank
                {
                    //std::cout << "ADD IN BANK \n";

                    if (clip->HERO->_nbAmmo>0)
                    {

//                            if (_UNIT->_energy < _UNIT->_energyMax)
//                            {
//                                _UNIT->_energy += 8;
//                            }
//
//                            if (_UNIT->_energy >= _UNIT->_energyMax)
//                                _UNIT->_energy = _UNIT->_energyMax;
//                            else
//                                --clip->HERO->_nbAmmo;
//
//
//                            if (_UNIT->_energy == _UNIT->_energyMax)
                            ++clip->HERO->_nbPoint;
                            --clip->HERO->_nbAmmo;

                        if (_NUMBER("inBank") == 0)
                        {
                            _NUMBER("inBank") = 1;

                            al_play_sample(_asset->GET_SAMPLE("repair"), 0.4, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        }
                    }
                    else
                    {
                        _NUMBER("inBank") = 0;
                    }
                }

                if (clip->COMMAND->onButton(PAD_DOWN)) // GET from bank
                {
                    //std::cout << "GET FROM BANK \n";

                    if (clip->HERO->_nbPoint>0)
                    {

//                            if (_UNIT->_energy < _UNIT->_energyMax)
//                            {
//                                _UNIT->_energy += 8;
//                            }
//
//                            if (_UNIT->_energy >= _UNIT->_energyMax)
//                                _UNIT->_energy = _UNIT->_energyMax;
//                            else
//                                --clip->HERO->_nbAmmo;
//
//
//                            if (_UNIT->_energy == _UNIT->_energyMax)
                            --clip->HERO->_nbPoint;
                            ++clip->HERO->_nbAmmo;

                        if (_NUMBER("inBank") == 0)
                        {
                            _NUMBER("inBank") = 1;

                            al_play_sample(_asset->GET_SAMPLE("repair"), 0.4, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        }
                    }
                    else
                    {
                        _NUMBER("inBank") = 0;
                    }
                }




            }

        }
        else
        {
            _NUMBER("inBank") = 0;
        }

        _UNIT->update();
        if (_UNIT->_energy <= 0)
        {
            _pause = true;
            _gameOver = true;
            KILL_THIS();
        }

    })
    ->RENDER(
    {
        al_draw_bitmap(_asset->GET_BITMAP("exit"), _ABSX, _ABSY,0);

        if (_start == 2)
            _UNIT->render();

        _THIS->showRect(al_map_rgba(115,120,0,50))
        ->showPivot(al_map_rgb(255,0,255),2)
        ->renderCollideZone(0, al_map_rgb(0,250,100));
    });
    //};

    _clip["ammo"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _mainFont, _mouse)
    ->setType(CLIP_ITEM)
    ->setActive(true)
    ->setPosition(400,488,2)
    ->setSize(16,16)
    ->setPivot(8,8)
    //->attach<Component::Draggable>()
    ->attach(new Component::Draggable())
    ->setMouse(_mouse)
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,16,16})
    ->setNumber("move",0)
    ->setNumber("moveY",-1)
    ->setNumber("nbAmmo",4)
    ->UPDATE(
    {
        ON_FRAME(1200)
        {
            KILL_THIS();
        }
        GOTO_NEXT_FRAME();

        _THIS->updateCollideZone(0,_RECT);

        ON_COLLIDE_ZONE_CLIP_TYPE(0, CLIP_PLAYER,0)
        {

            _msgQueue->post(AMMO, new MsgData::Item(_NUMBER("nbAmmo")), _PARENT->index(_ID_ZONE_COLLIDE_BY(0)) );

            MAKE_CLONE(_clip["score"], "cloneScore")
            ->setX(_X)
            ->setY(_Y-4)
            ->setString("message",std::to_string(_NUMBER("nbAmmo")))
            ->setNumber("startY",_Y)
            ->setNumber("goalY", _Y-24)
            ->setNumber("r",250)
            ->setNumber("g",250)
            ->setNumber("b",10)
            ->setNumber("nbPoint", _NUMBER("nbAmmo"));

            al_play_sample(_asset->GET_SAMPLE("ammoGun"),
                           0.3, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);

            KILL_THIS();
        }

        _DRAGGABLE->update();

        _Y += _NUMBER("moveY");

        ++_NUMBER("move");

        if (_NUMBER("move")>4)
        {
            _NUMBER("move") = 0;
            _NUMBER("moveY") = -_NUMBER("moveY");
        }

    })
    ->RENDER(
    {
        al_draw_bitmap(_asset->GET_BITMAP("ammo"), std::floor(_ABSX), std::floor(_ABSY),0);
    });
    //};

    _clip["portal"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _mainFont, _mouse)
    ->setType(CLIP_BUILDING)
    ->setActive(true)
    ->setPosition(80,40,2)
    ->setSize(32,32)
    ->setPivot(16,16)
    //->attach<Component::Draggable>()
    ->attach(new Component::Draggable())
    ->setMouse(_mouse)
    ->DRAGGABLE->setLimitRect(Rect{0,0,(VAR)_screenW,(VAR)_screenH})
    ->UPDATE(
    {
        _DRAGGABLE->update();

    })
    ->RENDER(
    {
        al_draw_bitmap(_asset->GET_BITMAP("portal"),_ABSX,_ABSY,0);

            al_draw_textf
            (
                _mainFont,
            al_map_rgb(250,150,0),
            _ABSX + _OX,
            _ABSY + _OY+_RECT._h/2,
            -1,
            "%s",
            _NAME
        );

        _THIS->showRect(al_map_rgba(115,120,0,50))
        ->showPivot(al_map_rgb(255,0,255),2);


//        // Debug
//        if (Collision2D::pointRect(Vec2{_xMouse, _yMouse}, _RECT))
//            _THIS->showComponent(_xMouse, _yMouse+32, al_map_rgb(255,25,150));

    });
    //};

    _clip["teleport"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _mainFont, _mouse)
    ->setType(CLIP_ITEM)
    ->setActive(true)
    ->setPosition(_screenW/2,_screenH-52,2)
    ->setSize(24,32)
    ->setPivot(12,16)
    //->attach<Component::Draggable>()
    ->attach(new Component::Draggable())
    //->attach<Component::Unit>()
    ->attach(new Component::Unit())
    ->UNIT->setEnergy(40)
    ->UNIT->setEnergyBar(20)
    ->UNIT->setEnergyColor(al_map_rgb(250,10,220))
    //->attach<Component::Jumper>()
    ->attach(new Component::Jumper())
    ->JUMPER->setGravity(0.35)
    ->JUMPER->setSpeedMax(2,8)
    ->JUMPER->setTileMap(_clip["tilemap0"]->TILEMAP2D)
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,24,32})
    ->UPDATE(
    {
        _THIS->updateCollideZone(0,_RECT);

        _DRAGGABLE->update();
        _UNIT->update();

        if (!_DRAGGABLE->_isDrag)
            _JUMPER->update();
        else
            _JUMPER->_isJump = true;


        ON_COLLIDE_ZONE(0)
        {
            VAR x = VAR(_screenW/2);
            VAR y = 0;


            ON_COLLIDE_ZONE_CLIP_NAME(0,"miniAlien",0)
            {
                Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));
                if (nullptr != clip)
                {
                    al_play_sample(_asset->GET_SAMPLE("star"),
                                   0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                    clip->setPosition(x,y,2);

                    _UNIT->_energy -= clip->_mapNumber["level"];

                }
            }
            ON_COLLIDE_ZONE_CLIP_NAME(0,"maxiAlien",0)
            {
                Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));
                if (nullptr != clip)
                {
                    al_play_sample(_asset->GET_SAMPLE("star"),
                                   0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                    clip->setPosition(x,y,2);

                    _UNIT->_energy -= clip->_mapNumber["level"];

                }

            }
            ON_COLLIDE_ZONE_CLIP_NAME(0,"destructor",0)
            {
                Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));
                if (nullptr != clip)
                {
                    al_play_sample(_asset->GET_SAMPLE("star"),
                                   0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                    clip->setPosition(x,y,2);

                    _UNIT->_energy -= clip->_mapNumber["level"];

                }

            }
        }

        if (_UNIT->_energy <= 0 )
        {
            al_play_sample(_asset->GET_SAMPLE("towerkill"),
                           0.8, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

            _centerX = _ABSX + _OX;
            _centerY = _ABSY + _OY;
            _maxRadius = 0.2;
            _radius = 0.0;
            _speed = 0.01;
            _useShader = true;

            //_THIS->setPosition(_screenW/2,_screenH-52,2);
            //_UNIT->setEnergy(40);

            KILL_THIS();
        }

    })
    ->RENDER(
    {
        al_draw_bitmap(_asset->GET_BITMAP("teleport"),_ABSX,_ABSY,0);
        _UNIT->render();

    });
    //};

    _clip["atom"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _mainFont, _mouse)
    ->setType(CLIP_ARM)
    ->setActive(true)
    ->setSize(24,24)
    ->setPivot(12,12)
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,24,12})
    ->setZ(5)
    ->setNumber("solY", 100)
    ->setNumber("startY",0)
    ->setNumber("goalY", 0)
    ->UPDATE(
    {
        _THIS->updateCollideZone(0,_RECT);

        ON_FRAME(0)
        {
            al_play_sample(_asset->GET_SAMPLE("atomiser"), 0.25, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        }

        if (_CURFRAME > 2 && _CURFRAME < 4 )
        {
            ON_COLLIDE_ZONE_CLIP_TYPE(0,CLIP_ENEMY,0)
            {

                //std::cout << "--- Atom Hit ENEMY ---!\n";
                Clip* enemy = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));

                if (nullptr != enemy)
                {
                    int damage = 200;
                    //_msgQueue->post(DAMAGE, new MsgData::Damage(damage,0), enemy );

                    enemy->UNIT->_energy -= damage;
                    enemy->UNIT->_isHit = 8;

                    al_play_sample(_asset->GET_SAMPLE("gunShot"), 0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                    enemy->JUMPER->_jump = false;
                    enemy->JUMPER->doJump(-4);

                    MAKE_CLONE(_clip["score"], "cloneScore")
                    ->setX(_X)
                    ->setY(_Y-4)
                    ->setString("message",std::to_string(damage))
                    ->setNumber("startY",_Y)
                    ->setNumber("goalY", _Y-24)
                    ->setNumber("r",200)
                    ->setNumber("g",50)
                    ->setNumber("b",10)
                    ->setNumber("nbPoint", 10);

                }


            }
        }


        ON_PLAY()
        {

            _Y = Tween::elasticEaseOut
            (
                _CURFRAME,
                _NUMBER("startY"),
                _NUMBER("goalY") - _NUMBER("startY"),
                80
            );

        }

        ON_FRAME(25)
        {
            KILL_THIS();
        }


        GOTO_NEXT_FRAME();

    })
    ->RENDER(
    {

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
        al_draw_filled_rectangle(_ABSX, _ABSY+4, _ABSX+24, _NUMBER("startY")+4,al_map_rgba(255,100,155,250-_CURFRAME*10));

        al_draw_tinted_bitmap(_asset->GET_BITMAP("atom"),al_map_rgba(250,250,250,250-_CURFRAME*10),_ABSX,_ABSY,0);

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

    });
    //};


}
