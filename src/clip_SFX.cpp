#include "MyGame.h"

void MyGame::makeClipSFX()
{
    _clip["score"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _asset->GET_FONT("gameFont"), _mouse)
    ->setType(CLIP_SFX)
    ->setActive(true)
    ->setZ(100)
    //->attach<Component::Velocity>()
    ->attach(new Component::Velocity())
    ->VELOCITY->setVY(-1)
    ->setNumber("nbPoint",0)
    ->setString("message","")
    ->setNumber("r",100)
    ->setNumber("g",100)
    ->setNumber("b",100)
    ->UPDATE(
    {
        _Z = _Y;
        ON_PLAY()
        {
            if (_NUMBER("nbPoint") < 20)
            {
                //_Y += VELOCITY->_y;
                _Y = Tween::elasticEaseOut
                (
                    _CURFRAME,
                    _NUMBER("startY"),
                    _NUMBER("goalY") - _NUMBER("startY"),
                    64
                );

            }
            else
            {
                //_Y += VELOCITY->_y*2;
                _Y = Tween::quadraticEaseOut
                (
                    _CURFRAME,
                    _NUMBER("startY"),
                    (_NUMBER("goalY")-24) - _NUMBER("startY"),
                    64
                );
            }
        }

        ON_FRAME(64)
        {
            KILL_THIS();
        }

        GOTO_NEXT_FRAME();

    })
    ->RENDER(
    {
//{
//            if (_NUMBER("nbPoint") < 20)
//            {
//                DRAW_TEXTF
//                (
//                    al_map_rgb(220,180,140),
//                    _ABSX, _ABSY, -1,
//                    "%i",
//                   _NUMBER("nbPoint")
//                );
//            }
//            else
//            {
//                DRAW_TEXTF
//                (
//                    al_map_rgb(250,80,0),
//                    _ABSX, _ABSY, -1,
//                    "KILL +%i",
//                    _NUMBER("nbPoint")
//                );
//            }
//}
        al_draw_textf
        (
            _mainFont,
            al_map_rgb(_NUMBER("r"),_NUMBER("g"),_NUMBER("b")),
            _ABSX, _ABSY, -1,
            "%s",
            _STRING("message").c_str()
        );

    });
    //};

    _clip["explosion"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _asset->GET_FONT("gameFont"), _mouse)
    ->setType(CLIP_SFX)
    ->setActive(true)
    ->setPivot(32,48)
    ->setSize(64,64)
    //->attach<Component::Velocity>()
    ->attach(new Component::Velocity())
    //->attach<Component::Animate>()
    ->attach(new Component::Animate())
    ->ANIMATE->setAnimation(_animeBoom)
    ->ANIMATE->setSequence(_spriteBoom)
    ->ANIMATE->startAt(0,2,1,0,17)
    ->UPDATE(
    {
        _Z = _Y;

        _ANIMATE->update();
        if (_ANIMATE->_atEnd)
            KILL_THIS();
    })
    ->RENDER(
    {
        if (nullptr != _ANIMATE)
        {
            _ANIMATE->render();
        }
//{
//            DRAW_TEXTF
//            (
//                al_map_rgb(205,150,0),
//                _ABSX+_OX, _ABSY+_OY, -1,
//                "%i",
//                _ID
//            );

        //_THIS->showPivot(al_map_rgb(55,100,255), 2)
        //->showRect(al_map_rgb(55,200,155));
//}
    });
    //};

    _clip["warning"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _mainFont, _mouse)
    ->setType(CLIP_SFX)
    ->setActive(true)
    ->setPosition(0,0,5)
    ->setSize(24,16)
    ->setPivot(12,8)
    ->setNumber("tempo",0)
    ->setNumber("show",0)
    ->UPDATE(
    {
        ++_NUMBER("tempo");

        if (_NUMBER("tempo") > 16)
        {
            _NUMBER("tempo") = 0;

            if (_NUMBER("show") == 0)
                _NUMBER("show") = 1;
            else
                _NUMBER("show") = 0;
        }
    })
    ->RENDER(
    {
        if (_NUMBER("show") == 1)
        {
            Draw::lineAA(0,_ABSY+_OY, _screenW, _ABSY+_OY, RGBA{255,0,0,50},1);
            Draw::lineAA(_ABSX+_OX,0, _ABSX+_OX, _screenH, RGBA{255,0,0,50},1);

            //Draw::line(0,_ABSY+_OY, _screenW, _ABSY+_OY, al_map_rgba(255,0,0,50),0);
            //Draw::line(_ABSX+_OX,0, _ABSX+_OX, _screenH, al_map_rgba(255,0,0,50),0);

            al_draw_bitmap(_asset->GET_BITMAP("warning"), _ABSX, _ABSY-24,0);

        }
    });
    //};

    _clip["nextWave"]//{
    ->setParent(_clip["layerHUD"])
//    ->setup(_window, _mainFont, _mouse)
    ->setType(CLIP_SFX)
    //->setActive(true)
    ->setPosition(_screenW/2,_screenH/2,0)
    ->setSize(320,32)
    ->setPivot(160,24)
    ->setNumber("tempo",0)
    ->setNumber("show",0)
    ->setNumber("life",160)
    ->UPDATE(
    {
        ON_FRAME(1)
        {
            std::cout << "START NEXTWAVE !\n";
        }

        //ON_FRAME(_NUMBER("life"))
        ON_FRAME(160)
        {
            KILL_THIS();
        }

        ++_NUMBER("tempo");

        if (_NUMBER("tempo") > 32)
        {
            al_play_sample(_asset->GET_SAMPLE("wave"),
                           0.3, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            _NUMBER("tempo") = 0;

            _NUMBER("show") = 1 - _NUMBER("show");
        }

        GOTO_NEXT_FRAME();

    })
    ->RENDER(
    {
        if (_NUMBER("show") == 1)
        {
            Draw::rectFill(_RECT+Rect{-320,-10,_RECT._w+320-1,_RECT._h-10}, al_map_rgba(0,10,20,200));
            //Draw::rect(_RECT+Rect{-320,-10,_RECT._w+320-1,_RECT._h-10}, al_map_rgba(200,10,20,200),0);
            al_draw_tinted_bitmap(_asset->GET_BITMAP("nextWave"),al_map_rgba(255,255,255,150), _ABSX, _ABSY,0);
        }
    });
    //};

    _clip["laserLine"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _mainFont, _mouse)
    ->setType(CLIP_SFX)
    ->setActive(true)
    ->setZ(5)
    //->attach<Component::Line>()
    ->attach(new Component::Line())
    ->setNumber("alpha",0)
    ->UPDATE(
    {
        ON_FRAME(120)
        {
            KILL_THIS();
        }

        _NUMBER("alpha") = 240-_CURFRAME*2;

        if (_NUMBER("alpha")<=0)
            _NUMBER("alpha") = 0;

        GOTO_NEXT_FRAME();
    })
    ->RENDER(
    {
        Draw::lineAA
        (
            _LINE->_x1,
            _LINE->_y1,
            _LINE->_x2,
            _LINE->_y2,
            RGBA{255,0,0,(unsigned char)_NUMBER("alpha")},
            _LINE->_thickness

        );
    });
    //};
}
