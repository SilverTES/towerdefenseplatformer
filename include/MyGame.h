#ifndef MYGAME_H
#define MYGAME_H

int const MAX_PLAYER = 4;

#include <MugenEngine.h>
#include "gameComponent.h"

#define _ROOT _clip["root"]


enum PlayerID
{
    PLAYER_1 = 0,
    PLAYER_2,
    PLAYER_3,
    PLAYER_4,
    PLAYER_MAX
};

enum TileProperty
{
    ID,
    BUILDABLE,
    EMPTY
};

enum MessageType
{
    NOTHING = -1,
    SCORE,
    HEAL,
    DAMAGE,
    DEAD,
    AMMO,
    BUTTON

};

enum ClipType
{
    CLIP_ROOT = 0,
    CLIP_PLAYER,
    CLIP_LAYER,
    CLIP_ENEMY,
    CLIP_BUILDING,
    CLIP_ARM,
    CLIP_ITEM,
    CLIP_SFX,
    CLIP_GUI

};

enum PlayerRole
{
    ROLE_BASIC = 0,
    ROLE_SNIPER,
    ROLE_BOMBER,
    ROLE_ATOMISER,
    ROLE_FREEZER,
    ROLE_BURNER,
    ROLE_MAX
};


namespace MsgData
{
    struct Score : public MessageData
    {
        int _score;

        Score (int score)
        {
            _score = score;
        }
    };

    struct Heal : public MessageData
    {
        int _energy = 0;

        Heal (int energy)
        {
            _energy = energy;
        }
    };

    struct Damage : public MessageData
    {
        int _damage = 0;
        int _vx;

        Damage (int damage, int vx = 0)
        {
            _damage = damage;
            _vx = vx;
        }
    };

    struct Dead : public MessageData
    {
        bool _dead = true;
    };

    struct Item : public MessageData
    {
        int _nb = 0;

        Item (int nb)
        {
            _nb = nb;
        }
    };

    struct Button : public MessageData
    {
        int _status = 0;
        std::string _message = "";
        Button (int status, std::string message)
        {
            _status = status;
            _message = message;
        }
    };

}

class MyGame : public MugenEngine
{
    public:

        int init() override;
        int done() override;
        void update() override;
        void render() override;

    protected:

        int initAsset();
        int doneAsset();
        int makeClip();

    private:

        // Stage selection

        int _currentStage = -1; //
        //int _nbPlayer = MAX_PLAYER;
        int _nbPlayer = 1;



        // ShockWave effect !
        ALLEGRO_SHADER* _myShader = nullptr;
        bool _useShader = false;

        float _centerX = 0;
        float _centerY = 0;
        float _radius = 0;
        float _maxRadius = 0.8;
        float _speed = 0;


        ALLEGRO_SAMPLE_ID _idMusic;

        MessageQueue* _msgQueue = nullptr;

        Asset::Manager* _asset = nullptr;

        Animation* _animeBoom = nullptr;
        Sequence* _spriteBoom = nullptr;

        Draw::Font* _mainFont = nullptr;
        Draw::Bitmap* _mouseCursor = nullptr;

        Json _gameConfig;

        Json _level;

        std::map<int, std::string> _mapEnemy =
        {
            std::make_pair(0,"mob0"),
            std::make_pair(1,"snail")
        };


        // Players
        Clip* _clipPlayer[MAX_PLAYER] = {nullptr};
        int _bank = 0; // Banque commune entre les joueurs


        bool _mouseButtonR;
        bool _mouseButtonL;
        bool _shoot = false;
        // Key for window management

        bool _keyFull;
        bool _keySwitch;
        bool _keyPause;

        bool _keyAlt;

        // Events
        ALLEGRO_EVENT_QUEUE* _eventQueue;
        ALLEGRO_EVENT _event;

        // FrameRate
        Framerate* _framerate;

        const char *_name;
        bool _isVsync = false;
        bool _isSmooth = false;
        bool _isFullScreen = false;
        int _screenW;
        int _screenH;
        int _scaleWin;
        int _scaleFull;

        // Mouse
        float _xMouse;
        float _yMouse;
        Input::Mouse* _mouse;

        bool _pause = false;
        int _start = 0;
        bool _gameOver = false;
        bool _timerTic = false;

        Clip::Pool _clip;

        Collision2D::GridSystem* _grid = nullptr;

        VAR angle1 = 0;
        VAR angle2 = 0;
        VAR angle3 = 0;

        Json _inputConfigJson;

        void makeClipRoot();
        void makeClipGui();
        void makeClipPlayer();
        void makeClipEnemy();
        void makeClipInGame();
        void makeClipStage();
        void makeClipItem();
        void makeClipSFX();

        bool _limitFPS = true;

        // Direct FPS
        double _oldTime = 0;
        double _newTime = 0;
        double _deltaTime =0;
        int _fps = 0;
        int _currentFps = 0;
        double _averageFPS = 0;
        std::vector<double> _vecFps;

};

#endif // MYGAME_H
