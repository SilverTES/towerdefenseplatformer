#include "MyGame.h"

void MyGame::makeClipStage()
{

    _clip["stage"]//{
    ->INIT(
    {
        al_play_sample(_asset->GET_SAMPLE("music"), 0.5, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, &_idMusic);
        al_stop_sample(&_idMusic);


        _clip["tilemap0"]->TILEMAP2D->loadTiledJSON
        (
            "data/mapZero.json", 0, 0, "tileSetLevelMap", _asset, "data/",
            {
                std::make_pair(ID,"id"),
                std::make_pair(BUILDABLE,"isBuildable"),
                std::make_pair(EMPTY,"isEmpty"),
            }
        );

        _clip["tilemapFG"]->TILEMAP2D->loadTiledJSON
        (
            "data/mapZero.json", 1, 1, "tileSetFG", _asset, "data/",
            {
                std::make_pair(ID,"id"),
                std::make_pair(BUILDABLE,"isBuildable"),
                std::make_pair(EMPTY,"isEmpty"),
            }
        );

    })
    ->appendTo(_clip["inGame"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,8)
    //->attach<Component::Stage>()
    ->attach(new Component::Stage())
    ->STAGE->loadStage("data/levelZero.json")

    ->setClip("parent", _clip["layer0"])
    ->setClip("nextWave", _clip["nextWave"])
    ->setClip("portal", _clip["portal"])
    ->setClip("exit", _clip["exit"])
    ->setClip("miniAlien", _clip["miniAlien"])
    ->setClip("maxiAlien", _clip["maxiAlien"])
    ->setClip("destructor", _clip["destructor"])
    ->setClip("player", _clip["player"])

    ->STAGE->setKeyState(&_keyState)
    ->STAGE->setControllerJSON(_inputConfigJson["controller"])
    ->STAGE->setPlayer(_clipPlayer)
    ->STAGE->initStage("4P")

    ->setNumber("start",0)
    ->setNumber("jukebox",0)
    ->setNumber("music",3)
    ->setNumber("keyJ", 0)

    ->setNumber("keyReload",0)
    ->UPDATE(
    {
        _STAGE->update();

//        _COMPONENT(Stage)->update();

        if (_STAGE->_stageState == 0)
        {
            _NUMBER("start") = 0;
        }

        if (_start == 1 || (KEY_PRESS(ALLEGRO_KEY_PAD_ENTER) && _NUMBER("start")==0))
        {
            _NUMBER("start") = 1;

//            _STAGE->initStage("begin4P");
//            _STAGE->setPlayer(_clipPlayer);

            _STAGE->startStage("1P");

            _start = 2;
            printf("STAGE START !\n");

        }

//        if (!KEY_PRESS(ALLEGRO_KEY_F12)) _NUMBER("keyReload") = 0;
//        if (KEY_PRESS(ALLEGRO_KEY_F12) && _NUMBER("keyReload") == 0) // Debug Force Reload !
//        {
//            _NUMBER("keyReload") = 1;
//            _STAGE->stopStage();
//
//            _clip["layer0"]->delAllType(CLIP_ENEMY);
//            _clip["layer0"]->delAllType(CLIP_BUILDING);
//            _clip["layer0"]->delAllType(CLIP_ARM);
//            _clip["layer0"]->delAllType(CLIP_ITEM);
//            _clip["layer0"]->delAllType(CLIP_SFX);
//            //_clip["layer0"]->delAllType(CLIP_PLAYER);
//
//            for (auto i = 0; i < MAX_PLAYER; ++i)
//            {
//                _clipPlayer[i]->setPosition
//                (
//                    _clipPlayer[i]->HERO->_startX,
//                    _clipPlayer[i]->HERO->_startY,
//                    4
//                )
//                ->JUMPER->setJump(true)->JUMPER->doFall();
//
//                _clipPlayer[i]->_init(_clipPlayer[i]);
//            }
//
//            printf("STAGE RELOAD !\n");
//
//            _clip["tilemap0"]->TILEMAP2D->reLoadTileProperty();
//
//            _STAGE->initStage("begin");
//
//            _NUMBER("start") = 0;
//
//        }


        // JukeBox
        if (_NUMBER("start")==1)
        {
            if (!KEY_PRESS(ALLEGRO_KEY_J)) _NUMBER("keyJ") = 0;
            if (KEY_PRESS(ALLEGRO_KEY_J) && _NUMBER("keyJ") == 0)
            {
                _NUMBER("keyJ") = 1;
                _NUMBER("jukebox") = 0;
            }

            --_NUMBER("jukebox");

            if (_NUMBER("jukebox") < 0)
            {

                //if (nullptr != &_idMusic)
                    al_stop_sample(&_idMusic);


                ++_NUMBER("music");

                if (_NUMBER("music") > 3)
                    _NUMBER("music") = 0;

                //printf("music = %i !\n", _NUMBER("music"));

                ALLEGRO_SAMPLE* sample = nullptr;
                float volume = 0;

                if (_NUMBER("music") == 0)
                {
                    sample = _asset->GET_SAMPLE("music");
                    volume = 0.35;
                }
                if (_NUMBER("music") == 1)
                {
                    sample = _asset->GET_SAMPLE("music2");
                    volume = 0.25;
                }
                if (_NUMBER("music") == 2)
                {
                    sample = _asset->GET_SAMPLE("music3");
                    volume = 0.4;
                }

                if (_NUMBER("music") == 3)
                {
                    sample = _asset->GET_SAMPLE("music4");
                    volume = 0.4;
                }

                al_play_sample(sample, volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, &_idMusic);

                unsigned int duration = al_get_sample_length(sample) / al_get_sample_frequency(sample);

                //std::cout << "Music duration = " << duration*60 << "\n";

                _NUMBER("jukebox") = duration*60;
            }
        }

    })
    ->RENDER(
    {
        _STAGE->render();

//        DRAW_TEXTF
//        (
//            al_map_rgb(250,250,0),
//            _screenW/2,2,
//            -1,
//            "Enemy = %i",
//            _STAGE->_nbActiveEnemy
//        );

//        if (_STAGE->_isStageEnd)
//        {
//            DRAW_TEXTF
//            (
//                al_map_rgb(20,250,0),
//                _screenW/2,_screenH/2,
//                -1,
//                "- VICTORY - STAGE %s CLEAR -",
//                _STAGE->_currentStageType.c_str()
//            );
//        }

    });
    //};
}
