#ifndef GAMECOMPONENT_H_INCLUDED
#define GAMECOMPONENT_H_INCLUDED

#include "MyGame.h"

#define COMPONENT(component) get<Component::component>()
#define _COMPONENT(component) _THIS->get<Component::component>()

namespace Component
{
    #define NAVISTATE get<Component::NaviState>()
    #define _NAVISTATE _THIS->get<Component::NaviState>()
    struct NaviState : public Component::Helper<NaviState>
    {
        std::deque<Clip*> _stackClip;

        Clip* clearState();
        Clip* pushState(Clip* clip);
        Clip* popState();
        Clip* current();
        bool stateEmpty();
        unsigned stateSize();

    };


    #define NAVINODE get<Component::NaviNode>()
    #define _NAVINODE _THIS->get<Component::NaviNode>()
    struct NaviNode : public Component::Helper<NaviNode>
    {
        std::map<int, Clip*> _mapNode;

        Clip* setNode(int direction, Clip* clip);

    };

    #define NAVIGATE get<Component::Navigate>()
    #define _NAVIGATE _THIS->get<Component::Navigate>()
    struct Navigate : public Component::Helper<Navigate>
    {
        // Manage navigation between clips who contain focusable GUI
        bool _isNavigate = false; // status : false : parent navigate , true : navigate this !
        Clip *_prevNaviClip = nullptr; // if nullptr current clip is probably "root" !

        bool _toPrev = false;
        bool _toNext = false;

        int _idFocusGUI = -1; // -1 focus nothing !
        int _prevIdFocusGUI = -1; // previous FocusedGui !
        bool _onChangeFocus = false; // true if focus changed

        Input::Button::ButtonEvent _confirmButton;

        Component::NaviState* _naviState = nullptr;

        Clip* setNaviState(Component::NaviState* naviState);
        Clip* setFocus(int idFocusGUI);
        Clip* setNavigate(bool isNavigate, int idFocusGUI = -1, Clip* prevNaviClip = nullptr);
        Clip* toPrevNavigate(int idFocusGUI = -1);

        // Movement of focus
        Clip* toPrevGUI();
        Clip* toNextGUI();

        Clip* toDirection(int direction);

        // Focused Gui
        Clip* focusedGUI(); // return Clip* focused GUI !

        bool onChangeFocus();

        bool onConfirmButton(bool const& button, int delayToRepeat = 30, int delayRepeat = 4);
        bool onConfirmButton();
        bool isConfirmButton();

        void update();
    };

    #define CONFIG_GAMEPAD get<Component::ConfigGamePad>()
    #define _CONFIG_GAMEPAD _THIS->get<Component::ConfigGamePad>()
    struct ConfigGamePad : public Component::Helper<ConfigGamePad>
    {
        bool _isWait = true;
        bool _isTest = false;
        bool _isRec  = false;
        bool _isRecOK = false;
        bool _isRecSafe = false; // if Record button is safe , ex: just after select "setup"
        bool _endConfig = true; // true if config/test button is finish !

        int _prevRecButton = 0;
        int _nextRecButton = 0;
        int _currentRecButton = 0;

        int _tempoEndTest = 0; // Hold any buttons to quit Test config !

        Draw::Color _color = al_map_rgb(250,200,0);
        int _alpha = 0;
        int _vAlpha = 25;

        std::map<int, bool> _mapButton;
        std::map<int, Rect> _mapRect;

        ::Player* _player = nullptr;
        ::Controller* _controller = nullptr;
        Draw::Bitmap* _bitmap = nullptr;

        Clip* initButton();
        Clip* setBitmap(Draw::Bitmap* bitmap);
        Clip* setPlayer(::Player* player);
        Clip* setButtonExec(bool const& buttonExec); // Is the button for validate/run/excute GUI !

        Clip* wait(); // Wait Do nothing :no rec, no test !
        Clip* recButton();  // begin rec button !
        Clip* testButton(); // begin test button !

        void update();
        void render();

    };

    #define HERO get<Component::Hero>()
    #define _HERO _THIS->get<Component::Hero>()
    struct Hero : public Component::Helper<Hero>
    {
        //Player State
        bool _isRun = false; // is start stage

        //Player Position
        VAR _startX = 0;
        VAR _startY = 0;

        bool _shoot = false;
        int _create = 0;
        int _teleport = 0;
        int _refund = 0;

        // Interaction with enemy
        bool _isBlinking = false; // is Blinking or not !
        int _tempoBlink = 0; // Tempo blinking of player when invulnerable
        int _kill = 0;     // Touched by enemy
        int _hitEnemy = 0; // Invulnerability when hit an enemy
        int _hitTimer = 100;
        int _looseControl = 80;
        VAR _hitEnemyVX = 0;
        VAR _hitEnemyX = 0;    // get enemy position x when hit player !
        VAR _hitEnemyY = 0;    // get enemy position y when hit player !

        int _nbAmmo = 0;
        int _nbAmmoMax = 240;
        int _nbPoint = 0;


        // Speciality
        int _playerRole = 0;

        // Sniper
        bool _startSniper = false;
        VAR _sniperX = 0;
        VAR _sniperY = 0;
        VAR _tempoSniper = 0;
        VAR _speedSniper = 100;

        // Atomiser
        bool _startAtom = false;
        int _atomDirection = 0; // direction of the atom
        int _atomPosX = 0; // position of the shockwave
        int _atomPosMapX = 0;
        int _atomPosMapY = 0;
        int _tempoAtom = 0; //
        int _speedAtom = 10; // tempo to next atom
        int _atomSize = 12; //

        Clip* setRun(bool const& isRun);
        Clip* setRole(int playerRole);
        Clip* setStartX(VAR x);
        Clip* setStartY(VAR y);
        Clip* setStartPosition(VAR x, VAR y);

    };


    #define UNIT get<Component::Unit>()
    #define _UNIT _THIS->get<Component::Unit>()
    struct Unit : public Component::Helper<Unit>
    {
        bool _isBuild = false;
        bool _beginBuild = false;
        bool _endBuild = false;
        int _tempoBuild = 0;
        int _tempoBuildMax = 0;
        int _stepBuildEnergy = 0;

        int _isHit = 0;

        int _owner;
        int _power;
        int _powerMax;

        int _energy;
        int _energyMax;

        Rect _rectP;
        Rect _rectE;

        int _energyBar = 32;

        Draw::Color _energyColor = al_map_rgb(255,255,255);
        Draw::Color _energyColorBG = al_map_rgb(255,255,255);

        Clip* startBuild(int tempoBuildMax, int stepBuildEnergy = 1);

        Clip* setOwner(int owner = -1);

        Clip* setPower(int power = 0);
        Clip* setPowerMax(int powerMax);

        Clip* setEnergy(int energy = 0);
        Clip* setEnergyMax(int energyMax);

        Clip* setEnergyBar(int energyBar);
        Clip* setEnergyColor(Draw::Color energyColor);
        Clip* setEnergyColorBG(Draw::Color energyColorBG);

        void update();
        void render();
    }__attribute__((packed));

    #define JUMPER get<Component::Jumper>()
    #define _JUMPER _THIS->get<Component::Jumper>()
    struct Jumper : public Component::Helper<Jumper>
    {
        bool _keyJump = false;
        bool _jump = false;

        bool _isJump = false;
        bool _isFall = false;
        bool _isLand = true;
        //bool _isForceFall = false;
        //int _tempoForceFall = 0;

        bool _moveL = false;
        bool _moveR = false;

        bool _canMoveL = false;
        bool _canMoveR = false;

        bool _hitU = false;
        bool _hitD = false;
        bool _hitL = false;
        bool _hitR = false;

        int _slow = 0;
        int _slowMax = 0;

        VAR _gravity = 0;

        VAR _moveX = 0;
        VAR _vy = 0;
        VAR _vx = 0;
        VAR _vyMax = 0;
        VAR _vxMax = 0;
        VAR _dy = 0;
        VAR _dx = 0;

        int _posMapX = 0;
        int _posMapY = 0;

        int _posMapAbsX = 0;
        int _posMapAbsY = 0;

        // 4 corners
        VAR _left = 0;
        VAR _top = 0;
        VAR _right = 0;
        VAR _bottom = 0;

        int _leftMap = 0;
        int _topMap = 0;
        int _rightMap = 0;
        int _bottomMap = 0;

        bool _isLimitMoveRect = false;
        Rect _limitMoveRect = {0,0,0,0};

        // Map2D
        Component::TileMap2D* _tileMap2D = nullptr;

        Clip* setGravity(VAR gravity);
        Clip* setSlowMax(int slowMax);
        Clip* setTileMap(Component::TileMap2D* tileMap2D);
        Clip* setSpeedMax(VAR vxMax, VAR vyMax);
        Clip* setSpeed(VAR vx, VAR vy);
        Clip* setMoveX(VAR moveX);
        Clip* setLimitMoveRect(Rect const& limitMoveRect);

        Clip* setJump(bool isJump);
        Clip* doJump(VAR vy = -1);
        Clip* doFall();
        Clip* doGravity();
        Clip* doLand(VAR y);
        Clip* doMove(VAR dx);
        Clip* doFriction(VAR fx);
        Clip* doCollision();

        void update();
        void render();

    }__attribute__((packed));

    #define STAGE get<Component::Stage>()
    #define _STAGE _THIS->get<Component::Stage>()
    struct Stage : public Component::Helper<Stage>
    {
        bool _onStageWin = false;
        bool _onStageEnd = false;
        bool _isStageEnd = false;

        int _nbActiveEnemy = 0;
        int _stageState = 0;
        bool _canGotoNextWave = false; // Allow next wave to come !

        bool _ticInterval = false;
        int _countInterval = 0;
        int _interval = 0;

        bool _ticWaveTempo = false;
        int _countWaveTempo = 0;
        int _waveTempo = 0;

        ALLEGRO_KEYBOARD_STATE* _keyState = nullptr;
        Json _controllerJson;

        Json _stage;
        Json _currentStage;        // current Json playing Wave !
        unsigned _currentWave = 0;   // current Wave in the playing level !
        std::string _currentStageType = "";
        std::string _currentWaveName = "";

        unsigned _lineTask = 0;
        std::deque<Json> _vecTask;

        std::vector<Clip*> _vecPortal;
        std::vector<Clip*> _vecExit;

        // Players
        Clip* _clipPlayer[MAX_PLAYER] = {nullptr};

        Clip* setPlayer(Clip* clipPlayer[]);

        Clip* loadStage(std::string const& stageFileName);
        Clip* initStage(std::string const& loadStage);
        Clip* setInterval(int interval);
        Clip* setWaveTempo(int waveTempo);
        Clip* setKeyState(ALLEGRO_KEYBOARD_STATE* keyState);
        Clip* setControllerJSON(Json const& controllerJson);
        Clip* startStage(std::string const& stageType);
        Clip* stopStage();
        Clip* gotoNextWave(); // if prev Wave is finish this method can force to Next Wave before Wait finish !


        bool preprocessTask(Json const& vecTask);
        bool doTask();

        void update();
        void render();

    };

    #define GAMESTATE get<Component::GameState>()
    #define _GAMESTATE _THIS->get<Component::GameState>()
    struct GameState : public Component::Helper<GameState>
    {
        bool _isChange = false;
        int _state = 0;

        int _atFrame = 0; // play currentClip at specific frame !

        Clip* _clipTransition = nullptr;

        Clip* _currentClip = nullptr;
        Clip* _prevClip = nullptr;

        Clip* setTransitionClip(Clip* clipTransition);
        Clip* setCurrentClip(Clip* currentClip);
        Clip* setState(int state);
        Clip* run(Clip* clip, int frame = 0, int atFrame = 0);

        bool onState(int state);
        bool onPos(int pos);


        void update();

    };

}

#endif // GAMECOMPONENT_H_INCLUDED
