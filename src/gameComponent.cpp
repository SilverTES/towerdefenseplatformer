#include "gameComponent.h"

namespace Component
{

//{ NaviState
    Clip* NaviState::clearState()
    {
        _stackClip.clear();
        return _clip;
    }
    Clip* NaviState::pushState(Clip* clip)
    {
        _stackClip.push_back(clip);
        return _clip;
    }
    Clip* NaviState::popState()
    {
        if (!_stackClip.empty())
            _stackClip.pop_back();

        return _clip;
    }
    Clip* NaviState::current()
    {
        if (_stackClip.empty())
            return nullptr;
        else
            return _stackClip.back();
    }
    bool NaviState::stateEmpty()
    {
        return _stackClip.empty();
    }
    unsigned NaviState::stateSize()
    {
        return _stackClip.size();
    }
//}

//{ NaviNode
    Clip* NaviNode::setNode(int direction, Clip* clip)
    {
        _mapNode[direction] = clip;
        return _clip;
    }
//}

//{ Navigate
    Clip* Navigate::setNaviState(Component::NaviState* naviState)
    {
        _naviState = naviState;
        return _clip;
    }
    Clip* Navigate::setFocus(int idFocusGUI)
    {
        _idFocusGUI = idFocusGUI;
        return _clip;
    }
    Clip* Navigate::setNavigate(bool isNavigate, int idFocusGUI, Clip* prevNaviClip)
    {
        _isNavigate = isNavigate;

        if (idFocusGUI != -1)
            _idFocusGUI = idFocusGUI;

        if (_isNavigate)
        {
            // pushNaviState if current naviSTATE is different to current naviGate
            if (nullptr != _naviState)
            {
                if (_clip != _naviState->current())
                    _naviState->pushState(_clip);
            }

            // check if prevNaviClip is valid Navigable Clip !
            if (nullptr != prevNaviClip)
            {
                if (nullptr != prevNaviClip->NAVIGATE)
                {
                    _prevNaviClip = prevNaviClip;
                    _prevNaviClip->NAVIGATE->setNavigate(false);
                }
            }
        }
        else
        {
            // stop navigation !
        }

        return _clip;
    }
    Clip* Navigate::toPrevNavigate(int idFocusGUI)
    {
        if (nullptr != _prevNaviClip)
        {
            setNavigate(false);
            _prevNaviClip->NAVIGATE->setNavigate(true, idFocusGUI);

//            if (idFocusGUI != -1)
//                _prevNaviClip->NAVIGATE->_idFocusGUI = idFocusGUI;
        }

        return _clip;
    }
    Clip* Navigate::toPrevGUI()
    {
        if (_isNavigate)
        {
            _toPrev = true;

            while (1)
            {
                --_idFocusGUI;

                if (_idFocusGUI < _clip->firstId()) _idFocusGUI = _clip->lastActiveId();

                if (nullptr != _clip->index(_idFocusGUI))
                    if (nullptr != _clip->index(_idFocusGUI)->GUI)
                        break;
            };

        }
        return _clip;
    }
    Clip* Navigate::toNextGUI()
    {
        if (_isNavigate)
        {
            _toNext = true;

            while (1)
            {
                ++_idFocusGUI;

                if (_idFocusGUI > _clip->lastId()) _idFocusGUI = _clip->firstActiveId();

                if (nullptr != _clip->index(_idFocusGUI))
                    if (nullptr != _clip->index(_idFocusGUI)->GUI)
                        break;
            };


        }
        return _clip;
    }
    Clip* Navigate::toDirection(int direction)
    {
        if (_isNavigate)
        {
            // Test if clip have NAVINODE component !
            if (nullptr != _clip->index(_idFocusGUI)->NAVINODE)
            {
                if (nullptr != _clip->index(_idFocusGUI)->NAVINODE->_mapNode[direction])
                {
                    _idFocusGUI = _clip->index(_idFocusGUI)->NAVINODE->_mapNode[direction]->id();

                }
            }
        }
        return _clip;
    }
    Clip* Navigate::focusedGUI()
    {
        return _clip->index(_idFocusGUI);
    }
    bool Navigate::onChangeFocus()
    {
        return _onChangeFocus;
    }
    bool Navigate::onConfirmButton(bool const& button, int delayToRepeat, int delayRepeat)
    {
        _confirmButton._isPress = button;
        _confirmButton._onPress = _isNavigate && Input::Button::oncePress("confirm", button);

        if (nullptr != _clip->index(_idFocusGUI))
        {
            if (nullptr != _clip->index(_idFocusGUI)->GUI) // check if focused clip have Component::Gui !
            {
                // Set the focused gui when button pressed !
                _clip->index(_idFocusGUI)->GUI->_onPress = _confirmButton._onPress;
                _clip->index(_idFocusGUI)->GUI->_isPress = _confirmButton._isPress;
            }
        }

        return _confirmButton._onPress;
    }
    bool Navigate::onConfirmButton()
    {
        return _confirmButton._onPress;
    }
    bool Navigate::isConfirmButton()
    {
        return _confirmButton._isPress;
    }
    void Navigate::update()
    {
        // Find focused GUI and set it !
        for (auto& it: _clip->groupAt(CLIP_GUI))
        {
            if (nullptr != it)
            {
                if (nullptr != it->GUI)
                {
                    if (it->_id != _idFocusGUI)
                        it->GUI->_isFocus = false;
                    else
                        it->GUI->_isFocus = true;
                }
            }
        }

        if (_idFocusGUI != _prevIdFocusGUI && _isNavigate)
        {
            _onChangeFocus = true;
            _prevIdFocusGUI = _idFocusGUI;
        }
        else
            _onChangeFocus = false;

    }
//}

//{ ConfigGamePad
    Clip* ConfigGamePad::setBitmap(Draw::Bitmap* bitmap)
    {
        _bitmap = bitmap;
        return _clip;
    }
    Clip* ConfigGamePad::initButton()
    {
        _mapRect[PAD_START]  = Rect{138,45,12,6};
        _mapRect[PAD_SELECT] = Rect{106,45,12,6};

        _mapRect[PAD_UP]     = Rect{42,29,12,13};
        _mapRect[PAD_DOWN]   = Rect{42,61,12,13};
        _mapRect[PAD_LEFT]   = Rect{26,46,13,12};
        _mapRect[PAD_RIGHT]  = Rect{57,46,13,12};

        _mapRect[PAD_A]      = Rect{218,46,12,12};
        _mapRect[PAD_B]      = Rect{202,62,12,12};
        _mapRect[PAD_X]      = Rect{202,30,12,12};
        _mapRect[PAD_Y]      = Rect{186,46,12,12};

        _mapRect[PAD_L]      = Rect{37,0,42,5};
        _mapRect[PAD_R]      = Rect{177,0,42,5};

        _mapButton[PAD_START]  = false;
        _mapButton[PAD_SELECT] = false;

        _mapButton[PAD_UP]     = false;
        _mapButton[PAD_DOWN]   = false;
        _mapButton[PAD_LEFT]   = false;
        _mapButton[PAD_RIGHT]  = false;

        _mapButton[PAD_A]      = false;
        _mapButton[PAD_B]      = false;
        _mapButton[PAD_X]      = false;
        _mapButton[PAD_Y]      = false;

        _mapButton[PAD_L]      = false;
        _mapButton[PAD_R]      = false;

        return _clip;
    }
    Clip* ConfigGamePad::setPlayer(::Player* player)
    {
        _player = player;
        _controller = _player->getController();
        return _clip;
    }
    Clip* ConfigGamePad::setButtonExec(bool const& buttonExec)
    {
        if (!_isRecSafe)
            _isRecSafe = !buttonExec;

        return _clip;
    }

    Clip* ConfigGamePad::wait()
    {
        _isRec = false;
        _isTest = false;
        _isWait = true;
        return _clip;
    }

    Clip* ConfigGamePad::recButton()
    {
        _isRec = true;
        _isTest = false;
        _currentRecButton = PAD_START;
        _prevRecButton = PAD_START;

        Controller::mapButton(_player, _currentRecButton);

        _endConfig = false;

        return _clip;
    }
    Clip* ConfigGamePad::testButton()
    {
        _isRec = false;
        _isTest = true;
        _currentRecButton = 0;

        Controller::cancelMapButton(_player, _currentRecButton);

        _endConfig = false;

        return _clip;
    }
    void ConfigGamePad::update()
    {
        _isRecOK = false;
        _isWait = !(_isRec || _isTest);

        int _anyButton = 0;

        for (auto& it: _mapButton)
        {
            it.second = _controller->getButton(it.first);

            if (it.second)
                ++_anyButton;
        }

        if (_anyButton > 0)
            _tempoEndTest += _anyButton;
        else
            _tempoEndTest = 0;


        if (_tempoEndTest > 256)
        {
            _tempoEndTest = 0;
            // Stop TEST !
            _isTest = false;
            _endConfig = true;
        }

        if (_isRec)
        {

            if (_isRecSafe) // || _controller->mapIdButton() == PAD_A)
                _controller->pollButton();

            _color = al_map_rgba(255,10,110,_alpha);

            if (nullptr != _controller)
            {
                if (!_controller->getButton(_prevRecButton) && _nextRecButton == 1)
                {
                    _nextRecButton = 0;
                    Controller::mapButton(_player, _currentRecButton);
                }

                if (_controller->isAssignButtonOK() && _nextRecButton == 0)
                {
                    _isRecOK = true;

                    if (_currentRecButton < MAX_BUTTONS - 1)
                    {
                        _nextRecButton = 1;
                        _prevRecButton = _currentRecButton;
                        ++_currentRecButton;
                    }
                    else
                    {
                        // Stop REC !
                        _isRec = false;
                        _isRecSafe = false;
                        _endConfig = true;

                        //std::cout << " END RECORD \n";
                    }


                }
            }

        }

        if (_isTest)
        {
            _color = al_map_rgba(200,250,250,200);

            if (_controller->getButton(PAD_L) &&
                _controller->getButton(PAD_START) )
            {
                // Stop TEST !
                _isTest = false;
                _endConfig = true;
            }

        }


        _alpha += _vAlpha;

        if (_alpha < 0)
        {
            _alpha = 0;
            _vAlpha = 25;
        }

        if (_alpha > 255)
        {
            _alpha = 255;
            _vAlpha = -25;
        }


    }
    void ConfigGamePad::render()
    {

        if (_endConfig)
        {
            al_draw_bitmap(_bitmap, _clip->absX(), _clip->absY(),0);
        }
        else
        {
            al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);

            if (_isRec)
            {

                Rect rect = _mapRect[_currentRecButton]+Rect{_clip->absX(),_clip->absY(),0,0};
                Draw::rectFill(rect, _color);

                al_draw_bitmap(_bitmap, _clip->absX(), _clip->absY(),0);


                VAR mx = rect._x+rect._w/2;
                VAR my = rect._y+rect._h/2;

                Rect rect2 = rect+Rect{-4,-4,8,8};

                Draw::Color color = al_map_rgba(250,50,10,255-_alpha);

                Draw::rect(rect2, color,0);

                VAR cenX = rect2._w/2 + _alpha/40;
                VAR cenY = rect2._h/2 + _alpha/40;
                VAR len = 8;
                VAR sz = 3;

                Draw::line(mx-cenX, my, mx-cenX-len, my, color,sz);
                Draw::line(mx+cenX, my, mx+cenX+len, my, color,sz);

                Draw::line(mx, my-cenY-1, mx, my-cenY-len-1, color,sz);
                Draw::line(mx, my+cenY, mx, my+cenY+len, color,sz);


            }

            if (_isTest)
            {

                Draw::rectFill(Rect{_clip->absX(), _clip->absY()-5,(VAR)_tempoEndTest,4}, al_map_rgb(250,200,50));
                Draw::rect(Rect{_clip->absX(), _clip->absY()-5,256,4}, al_map_rgb(50,100,150),0);

                for (auto& it: _mapButton)
                {
                    if (it.second)
                        Draw::rectFill(_mapRect[it.first]+Rect{_clip->absX(),_clip->absY(),0,0}, _color);
                }

                al_draw_bitmap(_bitmap, _clip->absX(), _clip->absY(),0);

                for (auto& it: _mapButton)
                {
                    if (it.second)
                    {
                        Rect rect = _mapRect[it.first]+Rect{_clip->absX(),_clip->absY(),0,0};

                        Draw::rectFill(rect, _color);
                        al_draw_bitmap(_bitmap, _clip->absX(), _clip->absY(),0);


                        VAR mx = rect._x+rect._w/2;
                        VAR my = rect._y+rect._h/2;

                        Rect rect2 = rect+Rect{-4,-4,8,8};

                        Draw::Color color = al_map_rgba(50,50,150,255-_alpha);

                        Draw::rect(rect2, color,0);

                        VAR cenX = rect2._w/2 + _alpha/40;
                        VAR cenY = rect2._h/2 + _alpha/40;
                        VAR len = 8;
                        VAR sz = 3;

                        Draw::line(mx-cenX, my, mx-cenX-len, my, color,sz);
                        Draw::line(mx+cenX, my, mx+cenX+len, my, color,sz);

                        Draw::line(mx, my-cenY-1, mx, my-cenY-len-1, color,sz);
                        Draw::line(mx, my+cenY, mx, my+cenY+len, color,sz);
                    }
                }
            }

            al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
        }



    }
//}

//{ Hero
    Clip* Hero::setRun(bool const& isRun)
    {
        _isRun = isRun;
        return _clip;
    }
    Clip* Hero::setRole(int playerRole)
    {
        _playerRole = playerRole;
        return _clip;
    }
    Clip* Hero::setStartPosition(VAR x, VAR y)
    {
        _startX = x;
        _startY = y;
        return _clip;
    }
    Clip* Hero::setStartX(VAR x)
    {
        _startX = x;
        return _clip;
    }
    Clip* Hero::setStartY(VAR y)
    {
        _startY = y;
        return _clip;
    }
//}

//{ Unit
    Clip* Unit::setOwner(int owner)
    {
        _owner = owner;
        return _clip;
    }
    Clip* Unit::setPower(int power)
    {
        _power = _powerMax = power;
        return _clip;
    }
    Clip* Unit::setPowerMax(int powerMax)
    {
        _powerMax = powerMax;
        return _clip;
    }
    Clip* Unit::setEnergy(int energy)
    {
        _energy = _energyMax = energy;
        return _clip;
    }
    Clip* Unit::setEnergyMax(int energyMax)
    {
        _energyMax = energyMax;
        return _clip;
    }
    Clip* Unit::setEnergyBar(int energyBar)
    {
        _energyBar = energyBar;
        return _clip;
    }
    Clip* Unit::setEnergyColor(Draw::Color energyColor)
    {
        _energyColor = energyColor;
        return _clip;
    }
    Clip* Unit::setEnergyColorBG(Draw::Color energyColorBG)
    {
        _energyColorBG = energyColorBG;
        return _clip;
    }
    Clip* Unit::startBuild(int tempoBuildMax, int stepBuildEnergy)
    {
        //std::cout << "< Start Build >";

        _tempoBuildMax = tempoBuildMax;
        _tempoBuild = 0;
        _stepBuildEnergy = stepBuildEnergy;
        _isBuild = true;
        _beginBuild = true;
        _energy = 0;
        return _clip;
    }
    void Unit::update()
    {
        if (_isBuild)
        {
            ++_tempoBuild;
            if (_tempoBuild >= _tempoBuildMax)
            {
                _tempoBuild = 0;
                _energy += _stepBuildEnergy;

                if (_energy >= _energyMax)
                {
                    _energy = _energyMax;
                    _isBuild = false;
                    _endBuild = true;
                }
            }
        }


        _rectE._x = _clip->absX() + _clip->_oX - _energyBar/2;
        //_rectE._y = _clip->absY() + _clip->_rect._h + 4;
        _rectE._y = _clip->absY() - 4;
        _rectE._w = _energyBar;
        _rectE._h = 2;

    }
    void Unit::render()
    {
        int x = _rectE._x;
        int y = _rectE._y;
        int w = _rectE._w;
        int h = _rectE._h;

        int nrj = (_rectE._w*_energy)/_energyMax;

        al_draw_filled_rectangle(.5+x, .5+y, .5+x+w, .5+y+h, _energyColorBG);
        al_draw_filled_rectangle(.5+x, .5+y, .5+x+nrj, .5+y+h, _energyColor);
        al_draw_rectangle(.5+x, .5+y, .5+x+w, .5+y+h, al_map_rgb(0,0,0),0);
    }
//}

//{ Jumper
    Clip* Jumper::setGravity(VAR gravity)
    {
        _gravity = gravity;
        return _clip;
    }
    Clip* Jumper::setSlowMax(int slowMax)
    {
        _slowMax = slowMax;
        return _clip;
    }
    Clip* Jumper::setTileMap(Component::TileMap2D* tileMap2D)
    {
        _tileMap2D = tileMap2D;
        return _clip;
    }
    Clip* Jumper::setSpeedMax(VAR vxMax, VAR vyMax)
    {
        _vxMax = vxMax;
        _vyMax = vyMax;
        return _clip;
    }
    Clip* Jumper::setSpeed(VAR vx, VAR vy)
    {
        _vx = vx;
        _vy = vy;
        return _clip;
    }
    Clip* Jumper::setMoveX(VAR moveX)
    {
        _moveX = moveX;
        return _clip;
    }
    Clip* Jumper::setLimitMoveRect(Rect const& limitMoveRect)
    {
        _isLimitMoveRect = true;
        _limitMoveRect = limitMoveRect;
        return _clip;
    }
    Clip* Jumper::setJump(bool isJump)
    {
        _isJump = isJump;
        return _clip;
    }
    Clip* Jumper::doJump(VAR vy)
    {
        if (!_jump)
        {
            _jump = true;
            _isLand = false;

            if (vy == -1)
                _vy = -_vyMax;
            else
                _vy = vy;

            _dy = _gravity;

        }
        return _clip;
    }
    Clip* Jumper::doFall()
    {
        if (!_isFall)
        {
            _jump = true;
            _isLand = false;
            if (_vy < 0) _vy = 0;
            _dy = _gravity;
        }
        return _clip;
    }
    Clip* Jumper::doGravity()
    {
        _vy += _dy;

        _isFall = _vy > 0;
        _isJump = _vy < 0;

        if (_isJump && _vy >=0) doFall();
        if (_vy > _vyMax) _vy = _vyMax;
        return _clip;
    }
    Clip* Jumper::doLand(VAR y)
    {
        if (!_isLand)
        {
            _isLand = true;
            _jump = false;
            _vy = 0;
            _dy = 0;
            _clip->_y = y;

            //_isForceFall = false;
        }
        return _clip;
    }
    Clip* Jumper::doMove(VAR dx)
    {
        //if (!_jump)
        {
            _dx = dx;
            _vx += _dx;

            _moveL = _vx<0;
            _moveR = _vx>0;

            if (_moveL && _vx < -_vxMax) _vx = -_vxMax;
            if (_moveR && _vx >  _vxMax) _vx =  _vxMax;
        }
        return _clip;
    }
    Clip* Jumper::doFriction(VAR fx)
    {
        if (_moveL)
        {
            _vx += fx;
            if (_vx >= 0) _vx = 0;
        }
        if (_moveR)
        {
            _vx -= fx;
            if (_vx <= 0) _vx = 0;
        }
        return _clip;
    }
    Clip* Jumper::doCollision()
    {

        // Test Collision 4 Direction , Test 4 Corners

        _left   = _clip->_x - _clip->_oX;
        _top    = _clip->_y - _clip->_oY;

        _right  = _left + _clip->_rect._w-1; // Important -1 for test the collision of TileMap at Right
        _bottom = _top + _clip->_rect._h-1; // Important -1 for test the collision of TileMap at Bottom

        _leftMap   = std::floor(_left/_tileMap2D->_tileW);
        _topMap    = std::floor(_top/_tileMap2D->_tileH);
        _rightMap  = std::floor(_right/_tileMap2D->_tileW);
        _bottomMap = std::floor(_bottom/_tileMap2D->_tileH);

        // Test LEFT
        if (_tileMap2D->getTileCollidable(_leftMap-1, _topMap) == 1 || _tileMap2D->getTileCollidable(_leftMap-1, _bottomMap) == 1)
            if (Collision2D::pointRect(Vec2{_left,_top}, _tileMap2D->getTileRect(_leftMap-1, _topMap, 2+std::abs(_vx)) ) ||
                Collision2D::pointRect(Vec2{_left,_bottom}, _tileMap2D->getTileRect(_leftMap-1, _bottomMap, 2+std::abs(_vx)) ) )
            {
                _hitL = true;
            }
        // Test RIGHT
        if (_tileMap2D->getTileCollidable(_rightMap+1, _topMap) == 1 || _tileMap2D->getTileCollidable(_rightMap+1, _bottomMap) == 1)
            if (Collision2D::pointRect(Vec2{_right,_top}, _tileMap2D->getTileRect(_rightMap+1, _topMap, 2+_vx)) ||
                Collision2D::pointRect(Vec2{_right,_bottom}, _tileMap2D->getTileRect(_rightMap+1, _bottomMap, 2+_vx)) )
            {
                _hitR = true;
            }
        // Test UP
        if (_tileMap2D->getTileCollidable(_leftMap, _topMap-1) == 1 || _tileMap2D->getTileCollidable(_rightMap, _topMap-1) == 1)
            if (Collision2D::pointRect(Vec2{_left,_top}, _tileMap2D->getTileRect(_leftMap, _topMap-1,   -_vy)) ||
                Collision2D::pointRect(Vec2{_right,_top}, _tileMap2D->getTileRect(_rightMap, _topMap-1,  -_vy)))
            {
                _hitU = true;

            }
        // Test DOWN
        if (_tileMap2D->getTileCollidable(_leftMap, _bottomMap+1) || _tileMap2D->getTileCollidable(_rightMap, _bottomMap+1) )
            if (Collision2D::pointRect(Vec2{_left,_bottom}, _tileMap2D->getTileRect(_leftMap, _bottomMap+1,  2+_vy)) ||
                Collision2D::pointRect(Vec2{_right,_bottom}, _tileMap2D->getTileRect(_rightMap, _bottomMap+1,  2+_vy)))
            {
                _hitD = true;

            }

        if (!_tileMap2D->getTileCollidable(_leftMap, _bottomMap+1) && !_tileMap2D->getTileCollidable(_rightMap, _bottomMap+1) )
        {
            //printf("< VOID >");
            if (_isLand)
            {
                doFall();
            }
        }

//        if (_isForceFall)
//        {
//            _hitD = false;
//            if (_isLand)
//            {
//                doFall();
//            }
//
//            ++_tempoForceFall;
//            if (_tempoForceFall > 8)
//            {
//                _tempoForceFall = 0;
//                _isForceFall = false;
//            }
//        }

        return _clip;
    }
    void Jumper::update()
    {

        // Reset All collisions !
        _hitU = false;
        _hitD = false;
        _hitL = false;
        _hitR = false;

        _canMoveL = true;
        _canMoveR = true;

        _posMapX = std::floor(_clip->_x/_tileMap2D->_tileW);
        _posMapY = std::floor(_clip->_y/_tileMap2D->_tileH);

        doCollision();

        if (_isLimitMoveRect)
        {
            if (_clip->_rect._x < _limitMoveRect._x)
                _hitL = true;

            if (_clip->_rect._x +_clip->_rect._w > _limitMoveRect._x+_limitMoveRect._w)
                _hitR = true;

            if (_clip->_rect._y < _limitMoveRect._y)
                _hitU = true;

            if (_clip->_rect._y+_clip->_rect._h >  _limitMoveRect._y+_limitMoveRect._h)
                _hitD = true;
        }

        if (_hitL)
        {
            //printf("< L >");
            _canMoveL = false;
            if (_moveL) _vx = 0;
        }
        if (_hitR)
        {
            //printf("< R >");
            _canMoveR = false;
            if (_moveR) _vx = 0;
        }

        if (_hitU)
        {
            //printf("< U >");
            //_clip->_y = (_topMap-1) * _tileMap2D->_tileH + _tileMap2D->_tileH;
            if (_isJump)
            {
                //printf("< U > : %i, %i \n", _posMapX, _topMap-1);
                doFall();
            }
        }

        if (_hitD)
        {
            if (_isFall)
            {
                //printf("< D > : %i, %i \n", _posMapX, _bottomMap+1);
                doLand(_tileMap2D->getTileRect(_leftMap, _bottomMap+1,4)._y-_clip->_rect._h+_clip->_oY+4);
            }
                //doLand(_clip->_y);
        }

        doGravity();


        // Move the Jumper !
        ++_slow;

        if (_slow > _slowMax)
        {
            _clip->_x += _vx;
            _slow = 0;
        }

        _clip->_y += _vy;



    }
    void Jumper::render()
    {

        if (Clip::_showClipInfo)
        {
            Draw::rect(_tileMap2D->getTileRect(_posMapX-1,_posMapY)+Rect{_clip->_parent->absX(),_clip->_parent->absY(),0,0},al_map_rgb(50,150,40),0);
            Draw::rect(_tileMap2D->getTileRect(_posMapX+1,_posMapY)+Rect{_clip->_parent->absX(),_clip->_parent->absY(),0,0},al_map_rgb(50,40,150),0);
            Draw::rect(_tileMap2D->getTileRect(_posMapX,_posMapY-1)+Rect{_clip->_parent->absX(),_clip->_parent->absY(),0,0},al_map_rgb(150,150,40),0);
            Draw::rect(_tileMap2D->getTileRect(_posMapX,_posMapY+1)+Rect{_clip->_parent->absX(),_clip->_parent->absY(),0,0},al_map_rgb(150,40,150),0);

            Draw::rect(_tileMap2D->getTileRect(_posMapX,_posMapY)+Rect{_clip->_parent->absX(),_clip->_parent->absY(),0,0},al_map_rgb(250,0,40),0);

            Draw::rect(_tileMap2D->getTileRect(_posMapX,_posMapY,24*2)+Rect{_clip->_parent->absX(),_clip->_parent->absY(),0,0},al_map_rgb(250,0,40),0);
        }


    }
//}

//{ Stage
    Clip* Stage::setPlayer(Clip* clipPlayer[])
    {
        //int nbPlayer = (int)(sizeof(clipPlayer)/sizeof(*clipPlayer));

        for (auto i = 0; i < MAX_PLAYER; ++i)
        {

            _clipPlayer[i] = clipPlayer[i];
        }

        std::cout << "- Player copied !!! \n";
//
//        for (auto i = 0; i < MAX_PLAYER; ++i)
//        {
//            std::cout << " Stage  Player "<< i << " = " << _clipPlayer[i]->_name << "\n";
//            std::cout << " Global Player "<< i << " = " << clipPlayer[i]->_name << "\n";
//        }

        return _clip;
    }
    Clip* Stage::loadStage(std::string const& stageFileName)
    {
        _stage = File::loadJson(stageFileName);

        return _clip;
    }
    Clip* Stage::setInterval(int interval)
    {
        _interval = interval;
        return _clip;
    }
    Clip* Stage::setWaveTempo(int waveTempo)
    {
        _waveTempo = waveTempo;
        return _clip;
    }
    Clip* Stage::setKeyState(ALLEGRO_KEYBOARD_STATE* keyState)
    {
        _keyState = keyState;
        return _clip;
    }
    Clip* Stage::setControllerJSON(Json const& controllerJson)
    {
        _controllerJson = controllerJson;
        return _clip;
    }
    Clip* Stage::initStage(std::string const& loadStage)
    {
        // Always clear vectors before begin stage, need when reload stage !
        _vecPortal.clear();
        _vecExit.clear();

        if (nullptr != _stage["init"][loadStage])
        {
            while (!preprocessTask(_stage["init"][loadStage]))
            {
                doTask();
            }

        }

        return _clip;
    }
    Clip* Stage::startStage(std::string const& stageType)
    {
        _isStageEnd = false;
        _onStageEnd = false;

        _stageState = 1;
        _lineTask = 0;
        _currentStageType = stageType;
        _currentStage = _stage["level"][_currentStageType];

        _currentWave = 0;
        _currentWaveName = _currentStage[_currentWave];

        //std::cout << "-- level Size = " << _currentLevel.size() << "\n";

        return _clip;
    }
    Clip* Stage::stopStage()
    {
        _isStageEnd = false;
        _onStageEnd = false;

        _stageState = 0;
        _vecTask.clear();

        return _clip;
    }
    Clip* Stage::gotoNextWave()
    {
        _canGotoNextWave = true;

        return _clip;
    }
    bool Stage::preprocessTask(Json const& vecTask)
    {
        if (_vecTask.empty())
        {
            // add Task to vecTask
            if (nullptr != vecTask)
            {
                if (_lineTask > vecTask.size()-1)
                {
                    std::cout << "--- END OF WAVE/TASK ---\n";
                    _lineTask = 0;
                    _stageState = 3;
                    return true;

                    _vecTask.clear();
                }

                if (nullptr != vecTask[_lineTask])
                {
                    Json currentTask = vecTask[_lineTask];
                    //std::cout << "." << _lineTask << " : Task : " << currentTask << "\n";

                    if (nullptr != currentTask["addUnit"])
                    {
                        std::string strUnit = currentTask["addUnit"][0];
                        int nbUnit = Misc::getRandom(strUnit,"..");

                        std::string strInterval = currentTask["addUnit"][2];
                        for (int i = 0; i < nbUnit; ++i)
                        {
                            int interval = Misc::getRandom(strInterval,"..");
                            currentTask["addUnit"][2] = std::to_string(interval);

                            _vecTask.push_back(currentTask);
                        }
                    }
                    else if (nullptr != currentTask["wait"])
                    {
                        _vecTask.push_back(currentTask);
                    }
                    else if (nullptr != currentTask["message"])
                    {
                        _vecTask.push_back(currentTask);
                    }
                    else if (nullptr != currentTask["addPortal"])
                    {
                        _vecTask.push_back(currentTask);
                    }
                    else if (nullptr != currentTask["addExit"])
                    {
                        _vecTask.push_back(currentTask);
                    }
                    else if (nullptr != currentTask["setPlayer"])
                    {
                        _vecTask.push_back(currentTask);
                    }
                }
                ++ _lineTask;
            }
        }
        return false;
    }
    bool Stage::doTask()
    {

        if (!_vecTask.empty())
        {
            ++_countInterval;

            if (_countInterval > _interval)
            {
                Json currentTask = _vecTask.front();
                //std::cout << "currentTask : " << currentTask << "\n";

                if (nullptr != currentTask)
                {
                    if (nullptr != currentTask["addUnit"])
                    {
                        std::string strType = currentTask["addUnit"][1];

                        std::string interval = currentTask["addUnit"][2];
                        _interval = std::stoi(interval);

                        std::string strPortal = currentTask["addUnit"][3];
                        unsigned portal = Misc::getRandom(strPortal,"..");

                        // Evite le bug des portes qui existe pas !
                        // Attention tout de même au new game+
                        // supprimer les portes qui apparaissent en cours de wave
                        // avant de les réintégrer sinon le vector size grossis mais l'id reste
                        // le même ! donc segmentation fault !
                        if (portal<0)
                            portal = 0;
                        if (portal>_vecPortal.size()-1)
                            portal = _vecPortal.size()-1;

                        if (nullptr != _vecPortal[portal])
                        {
                            std::cout << "--- ADD UNIT : "<< strType
                                      << " , interval : " << interval
                                      << " , portal : "  << portal
                                      << "\n";


                            if (strType == "miniAlien" ||
                                strType == "maxiAlien" ||
                                strType == "destructor")

                            {
                                VAR x = _vecPortal[portal]->_x;
                                VAR y = _vecPortal[portal]->_y;

                                printf("*** Added Enemy Position : %.0f, %.0f \n", x,y);

                                //std::cout << "begin clone !\n";
                                if (nullptr != _clip->_mapClip[strType])
                                {
                                    int sign = Misc::random(1,4);
                                    VAR vx = -_clip->_mapClip[strType]->JUMPER->_moveX;
                                    if (sign > 2)
                                        vx = _clip->_mapClip[strType]->JUMPER->_moveX;

                                    MAKE_CLONE(_clip->_mapClip[strType], strType)
                                    ->appendTo(_clip->_mapClip["parent"])//->setupParent()
                                    ->setActive(true)
                                    ->JUMPER->setSpeed(vx,0)
                                    ->JUMPER->setJump(true)
                                    ->setPosition(x,y,2);

                                    //std::cout << "_original of clone = " << clone->_original << "\n";

                                    ++_nbActiveEnemy;
                                }
                                //std::cout << "end clone !\n";
                            }
                        }


                    }
                    else if (nullptr != currentTask["wait"])
                    {
                        std::cout << "--- WAIT :" << currentTask["wait"] << "\n";

                        std::string wait = currentTask["wait"];
                        _interval = std::stoi(wait);
                        _countInterval = 0;
                    }
                    else if (nullptr != currentTask["message"])
                    {
                        std::cout << "--- MESSAGE :" << currentTask["message"] << "\n";

                        std::string wait = currentTask["message"][1];
                        _waveTempo = std::stoi(wait);
                        _countWaveTempo = _waveTempo;
                        _ticWaveTempo = false;


                        MAKE_CLONE(_clip->_mapClip["nextWave"], "nextWave")
                        ->setActive(true);
                        //->setNumber("life",200);

                    }
                    else if (nullptr != currentTask["addPortal"])
                    {
                        std::cout << "--- ADD PORTAL :" << currentTask["addPortal"] << "\n";

                        std::string x = currentTask["addPortal"][1];
                        std::string y = currentTask["addPortal"][2];

                        Clip* clip = MAKE_CLONE(_clip->_mapClip["portal"],currentTask["addPortal"][0])
                        ->setActive(true)
                        ->setPosition(std::stoi(x),std::stoi(y),2);

                        _vecPortal.push_back(clip);
                    }
                    else if (nullptr != currentTask["addExit"])
                    {
                        std::cout << "--- ADD EXIT :" << currentTask["addExit"] << "\n";

                        std::string x = currentTask["addExit"][1];
                        std::string y = currentTask["addExit"][2];

                        Clip* clip = MAKE_CLONE(_clip->_mapClip["exit"],currentTask["addExit"][0])
                        ->setActive(true)
                        ->setPosition(std::stoi(x),std::stoi(y),2);

                        _vecExit.push_back(clip);
                    }
                    else if (nullptr != currentTask["setPlayer"])
                    {
                        std::cout << "--- ADD HERO :" << currentTask["setPlayer"] << "\n";

                        std::string id = currentTask["setPlayer"][0];
                        std::string x = currentTask["setPlayer"][1];
                        std::string y = currentTask["setPlayer"][2];
                        std::string pad = currentTask["setPlayer"][3];
                        std::string role = currentTask["setPlayer"][4];
                        int nbAmmo = currentTask["setPlayer"][5];

                        int playerRole = ROLE_BASIC;

                        if (role == "SNIPER") playerRole = ROLE_SNIPER;
                        if (role == "ATOMISER") playerRole = ROLE_ATOMISER;


                        VAR startX = std::stoi(x);
                        VAR startY = std::stoi(y);

                        std::string name = "player" + id;

                        // Attention id du premier jpueur est 1 mais l'index est 0, donc on met -1 !!!
//                        if (nullptr == _clipPlayer[std::stoi(id)-1])
//                            _clipPlayer[std::stoi(id)-1] = MAKE_CLONE(_clip->_mapClip["player"], name);


                        _clipPlayer[std::stoi(id)-1]
                        ->setActive(true)
                        ->HERO->setRole(playerRole)
                        ->HERO->setStartX(startX)
                        ->HERO->setStartY(startY)
//                        ->COMMAND->setPlayer(new ::Player(name))
//                        ->COMMAND->setKeyState(_keyState)
//                        ->COMMAND->loadController(_controllerJson[pad])
                        ->setPosition(startX, startY, 4)->JUMPER->setJump(true)->JUMPER->doFall()
                        ->HERO->_nbAmmo = nbAmmo;

                    }

                    _vecTask.pop_front();
                }
                _ticInterval = true;
            }
        }

        return false;
    }
    void Stage::update()
    {
        _onStageWin = false;

        if (_stageState == 1)
        {
            _stageState = 2;
            std::cout << "Start Level !\n";
            std::cout << "currentWave = " << _currentWave << " : name :"<<  _currentWaveName <<"\n";
        }

        if (_stageState == 2)
        {
            preprocessTask(_stage["wave"][_currentWaveName]);
//            if (preprocessTask(_level["wave"][_currentWaveName]))
//            {

            if (_stageState == 3)
            {
                if (_currentWave < _currentStage.size()-1)
                {
                    ++_currentWave;
                    _currentWaveName = _currentStage[_currentWave];
                    std::cout << "currentWave = " << _currentWave << " : name :"<<  _currentWaveName <<"\n";

                    _stageState = 2;
                }
                else
                {
                    std::cout << "--- END OF STAGE --- \n";
                    _stageState = 0;
                    _onStageEnd = true;
                }
            }

//            }


            doTask();

            if (!_ticWaveTempo)
                --_countWaveTempo;

            if (_countWaveTempo < 0)
            {
                _ticWaveTempo = true;
                _countWaveTempo = 0;

            }

            if (_ticInterval)
            {
                _ticInterval = false;
                _countInterval = 0;
            }


        }

        if (_onStageEnd && _nbActiveEnemy == 0)
        {
            _isStageEnd = true;
            _onStageEnd = false;
            _onStageWin = true;
        }


    }
    void Stage::render()
    {
//        VAR barW = 800;
//        VAR barH = 4;
//        VAR x = (_clip->_window->screenW() - barW)/2;
//        VAR y = _clip->_window->screenH() - 80;
//        VAR currentInterval = (barW*_countWaveTempo)/_waveTempo;
//
//        Draw::rectFill(Rect{x,y,barW,barH}, al_map_rgb(10,10,10));
//        Draw::rectFill(Rect{x,y,currentInterval,barH}, al_map_rgb(250,200,10));
//        Draw::rect(Rect{x,y,barW,barH}, al_map_rgb(120,110,100),0);
    }
//}

//{ GameState
    Clip* GameState::setTransitionClip(Clip* clip)
    {
        _clipTransition = clip;
        return _clip;
    }
    Clip* GameState::setCurrentClip(Clip* currentClip)
    {
        _currentClip = currentClip;
        _currentClip->setActive(true);
        return _clip;
    }
    Clip* GameState::setState(int state)
    {
        _state = state;
        return _clip;
    }
    bool GameState::onState(int state)
    {
        return _state == state;
    }
    bool GameState::onPos(int pos)
    {
        if (nullptr != _clipTransition)
        {
            if (nullptr != _clipTransition->TRANSITION)
            return _clipTransition->TRANSITION->_currentPos == pos;
        }
        return false;
    }
    Clip* GameState::run(Clip* clip, int frame, int atFrame)
    {
        if (_currentClip != clip &&
            !_clipTransition->TRANSITION->onPos(Component::Transition::IS_TRANSITION) &&
            !_clipTransition->TRANSITION->onPos(Component::Transition::OFF_TRANSITION) )
        {
            _prevClip = _currentClip;
            _currentClip = clip;
            _atFrame = atFrame;

            if (nullptr != _prevClip)
            {
                _prevClip->setActive(false);
                _clipTransition->playAt(frame);
                //std::cout << "begin transition !\n";
            }
        }
        return _clip;
    }
    void GameState::update()
    {
        if (onPos(Component::Transition::OFF_TRANSITION))
            _isChange = true;

        if (_isChange) // Switch prevClip to currentClip when play transition
        {
            _isChange = false;

            if (nullptr != _prevClip)
            {
                _prevClip->stop();
                _prevClip->hide();
            }

            //_currentClip->start();
            _currentClip->show();
            _currentClip->setActive(true);
            _currentClip->playAt(_atFrame);

        }

        if (onPos(Component::Transition::IS_END)) // Active clip when transition end !
        {
            _currentClip->setActive(true);
        }
    }

}
