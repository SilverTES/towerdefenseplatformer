#include "MyGame.h"

void MyGame::makeClipRoot()
{
    _clip["root"]//{
//    ->setup(_window, _mainFont, _mouse)
    ->setActive(true)
    ->setSize(_screenW,_screenH)
    //->attach<Component::NaviState>()
    ->attach(new Component::NaviState())
    //->NAVISTATE->pushState(_clip["title"])
    ->UPDATE(
    {
        // Debug mode !
//        if (nullptr != _clip["player1"])
//        {
//            if (!_clip["player1"]->COMMAND->onButton(PAD_START)) _NUMBER("padStart") = 0;
//            if (_clip["player1"]->COMMAND->onButton(PAD_START) && _NUMBER("padStart") == 0)
//            {
//                std::cout << "--> PAD START <--\n";
//                _NUMBER("padStart") = 1;
//
//                _msgQueue->post(BUTTON, new MsgData::Button(2,"inGame"), _clip["gameState"]);
//            }
//        }

    })
    ->RENDER(
    {


    });
    //}

    _clip["door"]//{
    ->appendTo(_clip["root"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,20)
    //->attach<Component::Tempo>()
    ->attach(new Component::Tempo())
    ->TEMPO->setTempo("stayClose",16)
    ->setNumber("y",0)
    ->setNumber("vy",0)
    ->setNumber("isClose",0)
    ->setNumber("startY",0)
    ->setNumber("goalY",_screenH/2)
    ->setNumber("frame",0)
    //->attach<Component::Transition>()
    ->attach(new Component::Transition())
    ->TRANSITION->setPos(Component::Transition::IS_BEGIN)
    ->UPDATE(
    {
        _TEMPO->update();

        ON_FRAME(0)
        {
            _NUMBER("frame") = 0;
            _TRANSITION->setPos(Component::Transition::ON_TRANSITION);
            PAUSE();
        }

        ON_FRAME(1)
        {
            _TRANSITION->setPos(Component::Transition::IS_TRANSITION);
            _NUMBER("vy") = 1;
            al_play_sample(_asset->GET_SAMPLE("key"), 0.2, 0.0, 10.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        }

        GOTO_NEXT_FRAME();

        _NUMBER("frame") += _NUMBER("vy");

        //_NUMBER("y") = Tween::bounceEaseOut
        _NUMBER("y") = Tween::expoEaseIn
        (
            _NUMBER("frame"),
            _NUMBER("startY"),
            _NUMBER("goalY") - _NUMBER("startY"),
            24
        );

        if (_NUMBER("frame") >= 24 && _NUMBER("vy") > 0 && _NUMBER("isClose") == 0)
        {
            _NUMBER("frame") = 24;
            _NUMBER("vy") = 0;
            _NUMBER("isClose") = 1;

            _TRANSITION->setPos(Component::Transition::OFF_TRANSITION);

            al_play_sample(_asset->GET_SAMPLE("closeDoor"), 0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            _TEMPO->start("stayClose");
            //std::cout << "--- Door closed !\n" ;

        }

        if (_TEMPO->tic("stayClose") &&  _NUMBER("isClose") == 1)
        {
            _NUMBER("vy") = -1;
            _NUMBER("isClose") = 0;
            _TEMPO->stop("stayClose");

            al_play_sample(_asset->GET_SAMPLE("key"), 0.2, 0.0, 10.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        }

        if (_NUMBER("frame") < 0 && _NUMBER("vy") < 0 )
        {
            _NUMBER("y") = 0;
            _NUMBER("vy") = 0;

            _TRANSITION->setPos(Component::Transition::IS_END);

            PLAY_AT(0);
            //std::cout << "--- Stop Transition !\n" ;
        }


    })
    ->RENDER(
    {
        VAR doorU = -(VAR)_screenH/2+_NUMBER("y");
        VAR doorD = (VAR)_screenH-_NUMBER("y");

        al_draw_bitmap(_asset->GET_BITMAP("transitionU"), 0, doorU, 0);
        al_draw_bitmap(_asset->GET_BITMAP("transitionD"), 0, doorD, 0);

        if (_NUMBER("isClose") == 1)
            Draw::line(0,_screenH/2,_screenW,_screenH/2,al_map_rgb(0,100,255),0);

//            al_draw_textf
//            (
//                _mainFont,
//                al_map_rgb(250,250,0), 2,2,0,
//                "Transition : %i , Frame : %i",
//                _clip["root"]->getNumber("gameState"),
//                _NUMBER("frame")
//            );

    });
    //}


    _clip["layerMenu"]//{
    ->appendTo(_clip["root"])
//    ->setup(_window,_mainFont,_mouse)
    ->setActive(true)
    ->setPosition(0,0,20)
    //->setSize(400,480)
    ->UPDATE(
    {

    })
    ->RENDER(
    {
        if (_pause)
        {
            al_draw_filled_rectangle(0,0,_screenW,_screenH,al_map_rgba(0,0,50,100));
            if (_gameOver)
                al_draw_text(_mainFont, al_map_rgb(255,0,10), _screenW/2, _screenH/2, -1, "- G A M E   O V E R -");
            else
                al_draw_text(_mainFont, al_map_rgb(250,0,160), _screenW/2, _screenH/2, -1, "- P A U S E -");
        }
        //Draw::rect(_RECT, al_map_rgb(250,250,0), 0);
    });
    //}

    _clip["mainMenu"] =
    {

        MAKE_CLONE(_clip["window"],"mainMenu")
        ->setPosition(_screenW/2-200,_screenH + 10,0)
        //->setPosition(20,_screenH + 10,0)
        ->SKIN->setId(1)
        ->setSize(400,480)

        ->INIT(
        {

            _CLIP("back") = MAKE_CLONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            //->attach(new Component::NaviNode())
            //->setActive(true)
            ->setSize(160,8)
            ->setString("message","back")
            ->setString("label", "BACK")
            ->setClip("toClip", _clip["gameState"])
            ->setPosition(200-80, 240 + 180);

            MAKE_CLONE_ALONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            ->setSize(160,16)
            ->setString("label", "RESUME")
            ->setString("message","hideMenu")
            ->setClip("toClip", _THIS)
            ->setPosition(200-80, 240);

            MAKE_CLONE_ALONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            ->setSize(160,16)
            ->setString("label", "RESTART")
            ->setString("message","hideMenu")
            ->setClip("toClip", _THIS)
            ->setPosition(200-80, 240 + 40);

            MAKE_CLONE_ALONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            ->setSize(160,16)
            ->setString("label", "OPTIONS")
            ->setString("message","options")
            ->setClip("toClip", _clip["gameState"])
            ->setPosition(200-80, 240 + 80);

            MAKE_CLONE_ALONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            ->setSize(160,16)
            ->setString("label", "SELECT STAGE")
            ->setString("message","selectStage")
            ->setClip("toClip", _clip["gameState"])
            ->setPosition(200-80, 240 + 120);

        })
        ->appendTo(_clip["layerMenu"])
        ->setActive(true)

        //->attach<Component::Navigate>()
        ->attach(new Component::Navigate())
        //->NAVIGATE->setNaviState(_ROOT->NAVISTATE) // Comment this to avoid push_back navistate !!
        ->NAVIGATE->setFocus(0)
        //->NAVIGATE->setNavigate(true)
        ->setNumber("startY", _screenH + 10)
        ->setNumber("goalY" , 20)
        ->setNumber("menuState", 0) // 0  = hide , 1 = show , 2 = choose
        ->UPDATE(
        {
            //_ORIGINAL->_update(_THIS);
            _NAVIGATE->update();

            Clip::getMaster(_THIS)->runUpdate(_THIS);

            int duration = 32;
            int showMenu = 10;
            int hideMenu = duration + showMenu + 1;

            ON_FRAME(0)
            {
                //_NAVIGATE->setNavigate(false);
            }

            ON_FRAME(2)
            {
                PLAY_AT(0);
            }

            ON_FRAME(duration+showMenu)
            {
                _Y = _NUMBER("goalY");
                _NAVIGATE->setNavigate(true);
                STOP();
            }

            ON_FRAME(duration+hideMenu)
            {
                _Y = _NUMBER("startY");
                _NAVIGATE->setNavigate(false);
                STOP();

                _pause = false; // RESUME GAME !
            }


            ON_MESSAGE_TYPE(BUTTON)
            {

                std::string message = static_cast<MsgData::Button*>(_MESSAGE->_data)->_message;

                std::cout << "Main menu RECEIVED : "<< message << " \n";

//                if (message == "title")
//                {
//                    PLAY_AT(showMenu + duration);
//                }

                if (message == "start")
                {
                    _NUMBER("menuState") = 1 - _NUMBER("menuState");

                    if (_NUMBER("menuState") == 1) message = "showMenu";
                    if (_NUMBER("menuState") == 0) message = "hideMenu";

                }

                if (message == "showMenu")
                {
                    _NUMBER("menuState") = 1;
                    PLAY_AT(showMenu);

                    _pause = true; // PAUSE GAME !

                }
                if (message == "hideMenu")
                {
                    _NUMBER("menuState") = 0;
                    PLAY_AT(hideMenu);
                }

                KILL_MESSAGE();
            }



            _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

            if (Input::Button::onPress("up"))   _NAVIGATE->toPrevGUI();
            if (Input::Button::onPress("down")) _NAVIGATE->toNextGUI();

            if (_NAVIGATE->onConfirmButton())
            {
                _msgQueue->post(BUTTON, new MsgData::Button(2,"hideMenu"), _clip["mainMenu"]) ;
            }

            if (_NAVIGATE->onChangeFocus())
                al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);


            ON_PLAY()
            {
                _NAVIGATE->setNavigate(false);

                if (_NUMBER("menuState") == 1)
                {
                    _Y = Tween::circularEaseOut
                    (
                        _CURFRAME-showMenu,
                        _NUMBER("startY"),
                        _NUMBER("goalY") - _NUMBER("startY"),
                        duration
                    );
                }

                if (_NUMBER("menuState") == 0)
                {
                    _Y = Tween::circularEaseOut
                    (
                        _CURFRAME-(showMenu+duration),
                        _NUMBER("goalY"),
                        _NUMBER("startY") - _NUMBER("goalY"),
                        duration
                    );
                }

                ON_FRAME(1+showMenu)
                {
                    al_play_sample(_asset->GET_SAMPLE("key"), 0.6, 0.0, 10.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                }

                ON_FRAME(1+hideMenu)
                {
                    al_play_sample(_asset->GET_SAMPLE("key"), 0.6, 0.0, 10.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                }

                ON_FRAME(duration+showMenu)
                {
                    al_play_sample(_asset->GET_SAMPLE("loose"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                }



            }

            GOTO_NEXT_FRAME();

        })
    };


    _clip["title"]//{
    ->INIT(
    {
        MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(80,16)
        ->setString("message","saveScreen")
        ->setString("label", "START")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, _screenH/2+80);

        MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setSize(80,16)
        ->setString("message","options")
        ->setString("label", "OPTIONS")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, _screenH/2+120);

        MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(80,16)
        ->setString("message","credits")
        ->setString("label", "CREDITS")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, _screenH/2+160);

        MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(80,16)
        ->setString("message","exit")
        ->setString("label", "QUIT")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, _screenH/2+200);

        _CLIP("configPlayer") =  MAKE_CLONE_ALONE(_clip["configPlayer"], "configPlayer")
        ->appendTo(_THIS)//->setupParent()
        ->setPosition(40,40)
        ->setActive(true);


        _CLIP("toolTipEX") = MAKE_CLONE_ALONE(_clip["toolTip"], "toolTipEx")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(160,24)
        ->setPosition(_screenW/2-40, _screenH/2-80, 1000)
        //->attach<Component::Draggable>()
        ->attach(new Component::Draggable())
        ->setMouse(_mouse)
        ->DRAGGABLE->setLimitRect(Rect{0,0,(VAR)_screenW, (VAR)_screenH},true)
        ->setNumber("arrowX", _screenW/2)
        ->setNumber("arrowY", _screenH/2)
        ->UPDATE(
        {
            _ORIGINAL->runUpdate(_THIS);

            _DRAGGABLE->update();
        })
        ->RENDER(
        {
            _ORIGINAL->runRender(_THIS);

            al_draw_text(_mainFont, al_map_rgb(0,0,0), _ABSX+8, _ABSY+4,0,"ToolTip Test..." );
        });


        _CLIP("messageBox") = MAKE_CLONE_ALONE(_clip["messageBox"], "messageBox")
        ->setSize(480,320)
        ->setPivot(CENTER)
        ->INIT(
        {
            _ORIGINAL->runInit(_THIS);

            _CLIP("back") = MAKE_CLONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            //->attach<Component::NaviNode>()
            ->attach(new Component::NaviNode())
            ->setActive(false)
            ->setSize(24,8)
            ->setPivot(MIDDLE)
            ->setString("message","ok")
            ->setString("label", "OK")
            ->setClip("toClip", _THIS)
            ->setPosition(_RECT._w/2, _RECT._h-16);

        })
        ->appendTo(_THIS)
        ->setNumber("speed",16)
        ->SKIN->setRGB(140,180,210)
        ->SKIN->setId(2)
        ->setActive(true)
        ->setPosition(_screenW/2, _screenH/2)
        ->NAVIGATE->setNavigate(true)
        ->NAVIGATE->setFocus(0)

        ->UPDATE(
        {
            _ORIGINAL->runUpdate(_THIS);

            _NAVIGATE->update();


            // Debug !
            if (!KEY_PRESS(ALLEGRO_KEY_R)) _NUMBER("keyR") = 0;
            if (KEY_PRESS(ALLEGRO_KEY_R)  && _NUMBER("keyR") == 0)
            {
                _NUMBER("keyR") = 1;

                PLAY_AT(10);
                _NAVIGATE->setNavigate(true,0,_clip["title"]);
            }

            _NAVIGATE->onConfirmButton( KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );


            ON_MESSAGE_TYPE(BUTTON)
            {
                std::string message = static_cast<MsgData::Button*>(_MESSAGE->_data)->_message;

                KILL_MESSAGE();

                //_NUMBER("kill") = 1;
                PLAY_AT(0);
                // Go back
                _NAVIGATE->toPrevNavigate();
            }

        })
        ->RENDER(
        {
            //_clip["messageBox"]->_render(_THIS);
            _ORIGINAL->runRender(_THIS);

            if (_NUMBER("show") == 2)
            {
                al_draw_bitmap(_asset->GET_BITMAP("avatar0"),_ABSX+_RECT._w-80, _ABSY+10, 0);

                al_draw_multiline_text
                (
                    _mainFont,al_map_rgb(250,200,20),
                    _ABSX+10, _ABSY+10,
                    _RECT._w - 20, 16,
                    0,
                    "                             --- MESSAGE --- \n"
                    " \n"
                    " this is a message test \n"
                    " ... \n"
                    " ... \n"
                    " ... \n"
                    " mugen is the best \n"
                );

                _CLIP("back")->setVisible(true);
                _CLIP("back")->setActive(true);
            }
            else
            {
                _CLIP("back")->setVisible(false);
                _CLIP("back")->setActive(false);
            }

        });


    })
    ->appendTo(_clip["root"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,10)

    //->attach<Component::Navigate>()
    ->attach(new Component::Navigate())
    ->NAVIGATE->setNaviState(_ROOT->NAVISTATE)
    ->NAVIGATE->setNavigate(true)
    ->NAVIGATE->setFocus(0)

    //->attach<Component::Loop>()
    ->attach(new Component::Loop())
    ->LOOP->setLoop(0,0,40,1,Component::Loop::LOOP_PINGPONG)
    ->LOOP->start()

    ->setNumber("tempoToolTip",0)
    ->setNumber("showToolTip",0)
    ->UPDATE(
    {
        _LOOP->update();

        _NAVIGATE->update();

        if (Input::Button::oncePress("start", SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_START)) ) )
        {
            _CLIP("messageBox")->playAt(10);
            _CLIP("messageBox")->NAVIGATE->setNavigate(true,0,_clip["title"]);
        }

        if (Input::Button::oncePress("showToolTip", KEY_PRESS(ALLEGRO_KEY_T)) )
        {
            _CLIP("toolTipEX")->playAt(3);
            //_CLIP("messageBox")->NAVIGATE->setNavigate(true,0,_clip["title"]);
        }

        if (!_mouse->_isMove)
        {
            _NUMBER("tempoToolTip") += 1;
        }
        else
        {
            _NUMBER("showToolTip") = 0;
            _NUMBER("tempoToolTip") = 0;
        }

        if (_NUMBER("tempoToolTip") >= 20 && _NUMBER("showToolTip") == 0)
        {
            _CLIP("toolTipEX")->setNumber("arrowX", _mouse->_x);
            _CLIP("toolTipEX")->setNumber("arrowY", _mouse->_y);
            _CLIP("toolTipEX")->playAt(3);

            _NUMBER("showToolTip") = 1;

            //std::cout << "Show ToolTip !";
        }

        if (Input::Button::onPress("up"))   _NAVIGATE->toPrevGUI();
        if (Input::Button::onPress("down")) _NAVIGATE->toNextGUI();

        _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

        if (_NAVIGATE->onChangeFocus())
            al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);


    })
    ->RENDER(
    {
        al_draw_tinted_bitmap(_asset->GET_BITMAP("sniperTitle"),al_map_rgb(100+_LOOP->_current/4,100-_LOOP->_current/4,100),0,0,0);

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
//        Draw::rectFill(Rect{_ABSX,_ABSY,(VAR)_screenW, (VAR)_screenH}, al_map_rgba(0,20,140,50));
        Draw::grid(_ABSX,_ABSY,_screenW, _screenH,32,32,al_map_rgba(120,50,80,80-_LOOP->_current/2));
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);


        //al_draw_bitmap(_asset->GET_BITMAP("titleScreen"), 0, 0, 0);
        //Draw::grid(_ABSX,_ABSY,_screenW, _screenH,24,24,al_map_rgb(20,50,185));
        //al_draw_bitmap(_asset->GET_BITMAP("titlescreen"), 0, 0, 0);
        al_draw_bitmap(_asset->GET_BITMAP("title"), 0, 220, 0);

        al_draw_textf
        (
            _mainFont,
            al_map_rgb(250,150,55),
            _screenW/2,
            _screenH - 12,
            -1,
            "(C)%i MugenStudio",
            2017
            //_CLIP("menuTitle")->_y
        );
    });
    //};


    _clip["saveScreen"]//{
    ->INIT(
    {
        _THIS->hide();

        MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(240,16)
        ->setString("message","selectStage")
        ->setString("label", "1 - NEW GAME")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-120, 120);

        MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(240,16)
        ->setString("message","selectStage")
        ->setString("label", "2 - NEW GAME")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-120, 180);

        MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(240,16)
        ->setString("message","selectStage")
        ->setString("label", "3 - NEW GAME")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-120, 240);

        MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(240,16)
        ->setString("message","selectStage")
        ->setString("label", "4 - NEW GAME")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-120, 300);


        MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(80,8)
        ->setString("message","back")
        ->setString("label", "BACK")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, _screenH-20);
    })
    ->appendTo(_clip["root"])//->setupParent()
    //->setActive(false)
    ->setPosition(0,0,0)

    //->attach<Component::Navigate>()
    ->attach(new Component::Navigate())
    ->NAVIGATE->setNaviState(_ROOT->NAVISTATE)
    ->NAVIGATE->setFocus(0)
    //->NAVIGATE->setNavigate(true)

    ->UPDATE(
    {
        _NAVIGATE->update();

        _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

        if (Input::Button::onPress("up"))   _NAVIGATE->toPrevGUI();
        if (Input::Button::onPress("down")) _NAVIGATE->toNextGUI();

        if (_NAVIGATE->onChangeFocus())
            al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);

    })
    ->RENDER(
    {
        Draw::grid(_ABSX,_ABSY,_screenW, _screenH,16,16,al_map_rgb(20,50,60));

        al_draw_text
        (
            _mainFont,
            al_map_rgb(250,150,55),
            _screenW/2,
            8,
            -1,
            "--- SAVE ---"
        );

    });
    //};

    _clip["credits"]//{
    ->INIT(
    {
        _THIS->hide();

        MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(80,8)
        ->setString("message","back")
        ->setString("label", "BACK")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, _screenH-20);

        MAKE_CLONE_ALONE(_clip["window"], "creditWindow")
        ->appendTo(_THIS)//->setupParent()
        ->SKIN->setId(1)
        ->setActive(true)
        ->setSize(400,400)
        ->setPosition(_screenW/2,40)
        ->setPivot(200,0)
        ->RENDER(
        {
            _clip["window"]->runRender(_THIS);

            al_draw_multiline_text
            (
                _asset->GET_FONT("gameFont"),
                al_map_rgb(250,250,55),
                _ABSX+_OX,_ABSY+20,
                320,
                16,
                -1,
                "Programmer : Silver TES \n"
                "2D Artist : Silver TES \n"
                "Music/Sound : Silver TES \n"
                "\n"
                "\n"
                "\n"
                "\n"
                "Powered by :\n"
                "MugenEngine 1.0 \n"

            );
        });

    })
    ->appendTo(_clip["root"])//->setupParent()
    //->setActive(true)
    ->setPosition(0,0,0)

    //->attach<Component::Navigate>()
    ->attach(new Component::Navigate())
    ->NAVIGATE->setNaviState(_ROOT->NAVISTATE)
    ->NAVIGATE->setFocus(0)
    //->NAVIGATE->setNavigate(true)

    ->UPDATE(
    {
        _NAVIGATE->update();

        _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

    })
    ->RENDER(
    {
        Draw::grid(_ABSX,_ABSY,_screenW, _screenH,16,16,al_map_rgb(20,50,60));




        al_draw_text
        (
            _mainFont,
            al_map_rgb(250,150,55),
            _screenW/2,
            8,
            -1,
            "--- CREDITS ---"
        );

    });
    //};


    _clip["inGame"]//{
    ->INIT(
    {
        _THIS->hide();
    })
    ->appendTo(_clip["root"])//->setupParent()
//    ->setFont(_asset->GET_FONT("gameFont"))
    //->setActive(false)
    ->setPosition(0,0,0)


    //->attach<Component::Navigate>()
    ->attach(new Component::Navigate())
    ->NAVIGATE->setNaviState(_ROOT->NAVISTATE)
    //->NAVIGATE->setNavigate(true)

    // *** debug
    ->setNumber("keyR",0)

    ->UPDATE(
    {
//        ON_FRAME(0) // Pause when back in game !
//        {
//            _pause = true;
//        }
//
//        GOTO_NEXT_FRAME();

        Input::Button::oncePress("menu", _clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_START));

        if (Input::Button::oncePress("menu"))
        {
            _msgQueue->post(BUTTON, new MsgData::Button(2,"start"), _clip["mainMenu"]);
        }

        if (!KEY_PRESS(ALLEGRO_KEY_R)) _NUMBER("keyR") = 0;
        if (KEY_PRESS(ALLEGRO_KEY_R) && _NUMBER("keyR") == 0)
        {
            _NUMBER("keyR") = 1;

            _msgQueue->post(BUTTON, new MsgData::Button(2,"start"), _clip["mainMenu"]);
        }

    })
    ->RENDER(
    {

    });
    //};

    _clip["options"]//{
    ->INIT(
    {
        _THIS->hide();

       _CLIP("back") = MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        //->attach<Component::NaviNode>()
        ->attach(new Component::NaviNode())
        ->setActive(true)
        ->setSize(80,8)
        ->setString("message","back")
        ->setString("label", "BACK")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, _screenH-20);


        _CLIP("playerPAD4") = MAKE_CLONE_ALONE(_clip["configGamePad"], "configGamePad")
        ->appendTo(_THIS)
        ->setActive(true)
        ->setVisible(true)
        ->setPosition(_screenW/2+140, 200,0)
        ->setSize(256,100)
        ->setPivot(128,50);

        _CLIP("playerPAD3") = MAKE_CLONE_ALONE(_clip["configGamePad"], "configGamePad")
        ->appendTo(_THIS)
        ->setActive(true)
        ->setVisible(true)
        ->setPosition(_screenW/2+140, 380,0)
        ->setSize(256,100)
        ->setPivot(128,50);

        _CLIP("playerPAD2") = MAKE_CLONE_ALONE(_clip["configGamePad"], "configGamePad")
        ->appendTo(_THIS)
        ->setActive(true)
        ->setVisible(true)
        ->setPosition(_screenW/2-140, 200,0)
        ->setSize(256,100)
        ->setPivot(128,50);

        _CLIP("playerPAD1") = MAKE_CLONE_ALONE(_clip["configGamePad"], "configGamePad")
        ->appendTo(_THIS)
        ->setActive(true)
        ->setVisible(true)
        ->setPosition(_screenW/2-140, 380,0)
        ->setSize(256,100)
        ->setPivot(128,50);


       _CLIP("save") = MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        //->attach<Component::NaviNode>()
        ->attach(new Component::NaviNode())
        ->setActive(true)
        ->setSize(80,8)
        ->setString("message","save")
        ->setString("label", "SAVE")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, 100)
        ->FUNCTION(ON_PRESS,
        {
            std::cout << "- SAVE GAMEPAD CONFIGURATION -\n";

            for (int p=0; p<_nbPlayer; ++p)
            {
                std::string padName = "pad" + std::to_string(p+1);
                std::cout << "--- SAVE : " << padName << "\n";

                for (int i=1; i<MAX_BUTTONS; ++i)
                {
                    _inputConfigJson["controller"][padName][i][0] = Controller::SNESButtonDico[i];
                    _inputConfigJson["controller"][padName][i][1] = _clipPlayer[p]->COMMAND->_player->getController()->getButtonSetup(i)->_idKey;
                    _inputConfigJson["controller"][padName][i][2] = _clipPlayer[p]->COMMAND->_player->getController()->getButtonSetup(i)->_idJoy;
                    _inputConfigJson["controller"][padName][i][3] = _clipPlayer[p]->COMMAND->_player->getController()->getButtonSetup(i)->_idStick;
                    _inputConfigJson["controller"][padName][i][4] = _clipPlayer[p]->COMMAND->_player->getController()->getButtonSetup(i)->_idAxis;
                    _inputConfigJson["controller"][padName][i][5] = _clipPlayer[p]->COMMAND->_player->getController()->getButtonSetup(i)->_idDirection;
                    _inputConfigJson["controller"][padName][i][6] = _clipPlayer[p]->COMMAND->_player->getController()->getButtonSetup(i)->_idButton;
                }
            }

            File::saveJson("data/gamepad_save_config.json", _inputConfigJson);

            al_play_sample(_asset->GET_SAMPLE("warp"), 0.4, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        });

       _CLIP("load") = MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        //->attach<Component::NaviNode>()
        ->attach(new Component::NaviNode())
        ->setActive(true)
        ->setSize(80,8)
        ->setString("message","load")
        ->setString("label", "LOAD")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, 60)
        ->FUNCTION(ON_PRESS,
        {
            std::cout << "- LOAD GAMEPAD CONFIGURATION -\n";

            _inputConfigJson = File::loadJson("data/gamepad_save_config.json");

            for (int p=0; p<_nbPlayer; ++p)
            {
                std::string padName = "pad" + std::to_string(p+1);
                std::cout << "--- LOAD : " << padName << "\n";

                for (int i=1; i<MAX_BUTTONS; ++i)
                {
                    _clipPlayer[p]->COMMAND->_player->getController()->setButton
                    (
                        i,
                        _inputConfigJson["controller"][padName][i][1],
                        _inputConfigJson["controller"][padName][i][2],
                        _inputConfigJson["controller"][padName][i][3],
                        _inputConfigJson["controller"][padName][i][4],
                        _inputConfigJson["controller"][padName][i][5],
                        _inputConfigJson["controller"][padName][i][6]
                    );
                }
            }

            al_play_sample(_asset->GET_SAMPLE("loose"), 0.4, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        });

        _CLIP("back")->NAVINODE->setNode(N, _CLIP("playerPAD1"));
        _CLIP("back")->NAVINODE->setNode(S, _CLIP("load"));
        _CLIP("save")->NAVINODE->setNode(N, _CLIP("load"));
        _CLIP("save")->NAVINODE->setNode(S, _CLIP("playerPAD2"));
        _CLIP("load")->NAVINODE->setNode(N, _CLIP("back"));
        _CLIP("load")->NAVINODE->setNode(S, _CLIP("save"));

        _CLIP("playerPAD1")->NAVINODE->setNode(S, _CLIP("back"));
        _CLIP("playerPAD1")->NAVINODE->setNode(E, _CLIP("playerPAD3"));
        _CLIP("playerPAD1")->NAVINODE->setNode(N, _CLIP("playerPAD2"));

        _CLIP("playerPAD2")->NAVINODE->setNode(S, _CLIP("playerPAD1"));
        _CLIP("playerPAD2")->NAVINODE->setNode(E, _CLIP("playerPAD4"));
        _CLIP("playerPAD2")->NAVINODE->setNode(N, _CLIP("save"));

        _CLIP("playerPAD3")->NAVINODE->setNode(S, _CLIP("back"));
        _CLIP("playerPAD3")->NAVINODE->setNode(W, _CLIP("playerPAD1"));
        _CLIP("playerPAD3")->NAVINODE->setNode(N, _CLIP("playerPAD4"));

        _CLIP("playerPAD4")->NAVINODE->setNode(S, _CLIP("playerPAD3"));
        _CLIP("playerPAD4")->NAVINODE->setNode(W, _CLIP("playerPAD2"));
        _CLIP("playerPAD4")->NAVINODE->setNode(N, _CLIP("save"));


    })
    ->appendTo(_clip["root"])->setActive(false)
    ->setPosition(0,0,0)
    ->setSize(_screenW,_screenH)

    //->attach<Component::Navigate>()
    ->attach(new Component::Navigate())
    ->NAVIGATE->setNaviState(_ROOT->NAVISTATE)
    ->NAVIGATE->setFocus(0)
    //->NAVIGATE->setNavigate(true)

    //->attach<Component::Loop>()
    ->attach(new Component::Loop())
    ->LOOP->setLoop(0,0,40,1,Component::Loop::LOOP_PINGPONG)
    ->LOOP->start()

    ->UPDATE(
    {
        _LOOP->update();

        _CLIP("playerPAD1")->CONFIG_GAMEPAD->setPlayer(_clipPlayer[PLAYER_1]->COMMAND->_player);
        _CLIP("playerPAD2")->CONFIG_GAMEPAD->setPlayer(_clipPlayer[PLAYER_2]->COMMAND->_player);
        _CLIP("playerPAD3")->CONFIG_GAMEPAD->setPlayer(_clipPlayer[PLAYER_3]->COMMAND->_player);
        _CLIP("playerPAD4")->CONFIG_GAMEPAD->setPlayer(_clipPlayer[PLAYER_4]->COMMAND->_player);

        _NAVIGATE->update();

        _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

        if (Input::Button::onPress("up"))    _NAVIGATE->toDirection(N);
        if (Input::Button::onPress("down"))  _NAVIGATE->toDirection(S);
        if (Input::Button::onPress("left"))  _NAVIGATE->toDirection(W);
        if (Input::Button::onPress("right")) _NAVIGATE->toDirection(E);

        if (_NAVIGATE->onChangeFocus())
            al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);


    })
    ->RENDER(
    {
        al_clear_to_color(al_map_rgb(0,20,50));

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
//        Draw::rectFill(Rect{_ABSX,_ABSY,(VAR)_screenW, (VAR)_screenH}, al_map_rgba(0,20,140,50));
        Draw::grid(_ABSX,_ABSY,_screenW, _screenH,32,32,al_map_rgba(120,50,80,80-_LOOP->_current/2));
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);


        al_draw_text
        (
            _mainFont,
            al_map_rgb(250,150,55),
            _screenW/2,
            8,
            -1,
            "--- O P T I O N S ---"
        );

//        for (int i = 0; i < Controller::getNumJoystick(); ++i)
//        {
//            std::string name = sf::Joystick::getIdentification(i).name;
//
//            al_draw_textf
//            (
//                _mainFont,
//                al_map_rgb(250,250,55),
//                2,
//                2 + i * 10,
//                0,
//                "GamePad %i : %s",
//                i,
//                name.c_str()
//            );
//        }

//        al_draw_textf
//        (
//            _mainFont,
//            al_map_rgba(250,100,100,250),
//            _screenW/2,16,
//            -1,
//            "OPTIONS _isNavigate = %i",
//            _NAVIGATE->_isNavigate
//        );

    });
    //};

    _clip["selectStage"]//{
    ->INIT(
    {
        _THIS->hide();

        _CLIP("back") = MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        //->attach<Component::NaviNode>()
        ->attach(new Component::NaviNode())
        ->setActive(true)
        ->setSize(80,8)
        ->setString("message","back")
        ->setString("label", "BACK")
        ->setClip("toClip", _clip["gameState"])
        ->setPosition(_screenW/2-40, _screenH-20);

        int bw = 64;
        int bh = 8;


        _CLIP("level0") = MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        //->attach<Component::NaviNode>()
        ->attach(new Component::NaviNode())
        ->setActive(true)
        ->setSize(bw,bh)
        ->setString("message","0")
        ->setString("label", "STAGE 0")
        ->setClip("toClip", nullptr)
        ->FUNCTION(ON_PRESS,
        {
            _currentStage = 0;
        })
        ->setPosition(160, 440);

        _CLIP("level1") = MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        //->attach<Component::NaviNode>()
        ->attach(new Component::NaviNode())
        ->setActive(true)
        ->setSize(bw,bh)
        ->setString("message","1")
        ->setString("label", "STAGE 1")
        ->setClip("toClip", nullptr)
        ->FUNCTION(ON_PRESS,
        {
            _currentStage = 1;
        })
        ->setPosition(128, 320);

        _CLIP("level2") = MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        //->attach<Component::NaviNode>()
        ->attach(new Component::NaviNode())
        ->setActive(true)
        ->setSize(bw,bh)
        ->setString("message","2")
        ->setString("label", "STAGE 2")
        ->setClip("toClip", nullptr)
        ->FUNCTION(ON_PRESS,
        {
            _currentStage = 2;
        })
        ->setPosition(160, 192);

        _CLIP("level3") = MAKE_CLONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        //->attach<Component::NaviNode>()
        ->attach(new Component::NaviNode())
        ->setActive(true)
        ->setSize(bw,bh)
        ->setString("message","3")
        ->setString("label", "STAGE 3")
        ->setClip("toClip", nullptr)
        ->FUNCTION(ON_PRESS,
        {
            _currentStage = 3;
        })
        ->setPosition(256, 96);

        _CLIP("back")->NAVINODE->setNode(N, _CLIP("level0"));

        _CLIP("level0")->NAVINODE->setNode(N, _CLIP("level1"));
        _CLIP("level0")->NAVINODE->setNode(S, _CLIP("back"));
        //_CLIP("level0")->NAVINODE->setNode(W, _CLIP("level2"));

        _CLIP("level1")->NAVINODE->setNode(N, _CLIP("level2"));
        _CLIP("level1")->NAVINODE->setNode(S, _CLIP("level0"));

        _CLIP("level2")->NAVINODE->setNode(N, _CLIP("level3"));
        _CLIP("level2")->NAVINODE->setNode(S, _CLIP("level1"));

        _CLIP("level3")->NAVINODE->setNode(N, _CLIP("level0"));
        _CLIP("level3")->NAVINODE->setNode(S, _CLIP("level2"));


        _CLIP("optionBox") = MAKE_CLONE_ALONE(_clip["messageBox"], "optionBox")
        ->setSize(640,360)
        ->setPivot(CENTER)
        ->INIT(
        {
            _ORIGINAL->runInit(_THIS);

            _CLIP("ok") = MAKE_CLONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            //->attach<Component::NaviNode>()
            ->attach(new Component::NaviNode())
            ->setActive(false)
            ->setSize(64,8)
            ->setPivot(MIDDLE)
            ->setString("message","inGame")
            ->setString("label", "START")
            ->setClip("toClip", _clip["gameState"])
            ->setPosition(_RECT._w/2, _RECT._h-48);

            _CLIP("back") = MAKE_CLONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            //->attach<Component::NaviNode>()
            ->attach(new Component::NaviNode())
            ->setActive(false)
            ->setSize(64,8)
            ->setPivot(MIDDLE)
            ->setString("message","cancel")
            ->setString("label", "CANCEL")
            ->setClip("toClip", _THIS)
            ->setPosition(_RECT._w/2, _RECT._h-16);

        })
        ->appendTo(_THIS)
        ->setNumber("speed",24)
        ->SKIN->setRGB(140,180,210)
        ->SKIN->setId(2)
        ->setActive(true)
        ->setPosition(_screenW/2, _screenH/2)
        ->NAVIGATE->setNavigate(false)
        ->NAVIGATE->setFocus(1)

        ->UPDATE(
        {
            _ORIGINAL->runUpdate(_THIS);
            _NAVIGATE->update();

            // Debug !!
            if (!KEY_PRESS(ALLEGRO_KEY_R)) _NUMBER("keyR") = 0;
            if ((KEY_PRESS(ALLEGRO_KEY_R) && _NUMBER("keyR") == 0) ||
                Input::Button::oncePress("start", SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_START)) ) )
            {
                _NUMBER("keyR") = 1;

                PLAY_AT(10);
                _NAVIGATE->setNavigate(true,1,_clip["selectStage"]);
            }

            if (Input::Button::onPress("up"))   _NAVIGATE->toPrevGUI();
            if (Input::Button::onPress("down")) _NAVIGATE->toNextGUI();

            _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

            ON_MESSAGE_TYPE(BUTTON)
            {
                std::string message = static_cast<MsgData::Button*>(_MESSAGE->_data)->_message;
                KILL_MESSAGE();
                //_NUMBER("kill") = 1;
                PLAY_AT(0);
                // Go back
                _NAVIGATE->toPrevNavigate();
            }

            if (_NAVIGATE->onChangeFocus())
                al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);

        })
        ->RENDER(
        {
            //_clip["messageBox"]->_render(_THIS);
            _ORIGINAL->runRender(_THIS);

            if (_NUMBER("show") == 2)
            {
                //al_draw_bitmap(_asset->GET_BITMAP("avatar0"),_ABSX+_RECT._w-80, _ABSY+10, 0);

                al_draw_multiline_textf
                (
                    _mainFont,al_map_rgb(250,200,20),
                    _ABSX+10, _ABSY+10,
                    _RECT._w - 20, 16,
                    0,
                    "                                 --- STAGE %i OPTIONS --- \n"
                    " \n"
                    " Select the difficulty : \n"
                    " ... \n"
                    " ... \n"
                    " ... \n",
                    _currentStage
                );

                _CLIP("ok")->setVisible(true)->setActive(true);
                _CLIP("back")->setVisible(true)->setActive(true);
            }
            else
            {
                _CLIP("ok")->setVisible(false)->setActive(false);
                _CLIP("back")->setVisible(false)->setActive(false);
            }

        });



    })
    ->appendTo(_clip["root"])//->setupParent()
    //->setActive(false)
    ->setPosition(0,0,0)

    //->attach<Component::Navigate>()
    ->attach(new Component::Navigate())
    ->NAVIGATE->setNaviState(_ROOT->NAVISTATE)
    ->NAVIGATE->setFocus(0)
    //->NAVIGATE->setNavigate(true)

    ->UPDATE(
    {
        _NAVIGATE->update();

        ON_FRAME(0)
        {
            _CLIP("optionBox")->NAVIGATE->setNavigate(false);
            _CLIP("optionBox")->playAt(0);
            if (_CLIP("optionBox")->NAVIGATE->_isNavigate)
                _NAVIGATE->toPrevNavigate();
        }

        ON_FRAME(2)
        {
            PLAY_AT(1);
        }

        GOTO_NEXT_FRAME();


        _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

        if (Input::Button::onPress("up"))    _NAVIGATE->toDirection(N);
        if (Input::Button::onPress("down"))  _NAVIGATE->toDirection(S);
        if (Input::Button::onPress("left"))  _NAVIGATE->toDirection(W);
        if (Input::Button::onPress("right")) _NAVIGATE->toDirection(E);

        if (_NAVIGATE->onConfirmButton())
        {
            if (_NAVIGATE->focusedGUI() != _CLIP("back"))
            {
                _NAVIGATE->focusedGUI()->runMapFunction(ON_PRESS,nullptr);

                _CLIP("optionBox")->playAt(10);
                _CLIP("optionBox")->NAVIGATE->setNavigate(true,1,_clip["selectStage"]);
            }
        }

        if (_NAVIGATE->onChangeFocus())
            al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);


    })
    ->RENDER(
    {
        al_draw_bitmap(_asset->GET_BITMAP("worldMap"),0,0,0);

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
        Draw::grid(_ABSX,_ABSY,_screenW, _screenH,16,16,al_map_rgba(20,120,160,50));
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

        al_draw_text
        (
            _mainFont,
            al_map_rgb(250,100,55),
            _screenW/2,
            8,
            -1,
            "--- SELECT STAGE ---"
        );

    });
    //};

    _clip["gameState"]//{
    ->appendTo(_clip["root"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,100)
    //->attach<Component::GameState>()
    ->attach(new Component::GameState())
    ->GAMESTATE->setTransitionClip(_clip["door"])
    ->GAMESTATE->setCurrentClip(_clip["title"])
    ->setNumber("keyTitle",0)
    ->setNumber("keyGame",0)

    ->setNumber("keyPop",0)
    ->UPDATE(
    {
        if (!KEY_PRESS(ALLEGRO_KEY_Q)) _NUMBER("keyPop") = 0;
        if (KEY_PRESS(ALLEGRO_KEY_Q) && _NUMBER("keyPop") == 0)
        {
            _NUMBER("keyPop") = 1;

            _ROOT->NAVISTATE->popState();
        }

        _GAMESTATE->update();

        ON_MESSAGE()
        {
            ON_MESSAGE_TYPE(BUTTON)
            {
                std::string message = static_cast<MsgData::Button*>(_MESSAGE->_data)->_message;

                int atFrame = 0;
                Clip* clipToRun = nullptr;

                // go Back
                if (message == "back")
                {
                    std::cout << "BACK TO PREV NAVIGATE ! \n";

                    _ROOT->NAVISTATE->popState();

                    std::cout << "stateSize ="<< _ROOT->NAVISTATE->_stackClip.size() << " \n";

                    clipToRun = _ROOT->NAVISTATE->current();

                    //clipToRun = _ROOT->NAVISTATE->_stackClip[_ROOT->NAVISTATE->_stackClip.size()-2];

                    //_ROOT->NAVISTATE->popState();
                    //_ROOT->NAVISTATE->popState();
                }

                // goto selected clip
                if (nullptr != _clip[message])
                    clipToRun = _clip[message];

                // Run Transition !
                if (nullptr != clipToRun)
                {
                    if (nullptr != clipToRun->NAVIGATE)
                        clipToRun->NAVIGATE->setNavigate(true);

                    _GAMESTATE->run(clipToRun,1, atFrame);
                }

                if (message == "exit")
                    _quit = true;

                KILL_MESSAGE();
            }
        }

        //*** Debug gameState !
        if (!KEY_PRESS(ALLEGRO_KEY_F4)) _NUMBER("keyTitle") = 0;
        if (KEY_PRESS(ALLEGRO_KEY_F4) && _NUMBER("keyTitle") == 0)
        {
            _NUMBER("keyTitle") = 1;
            //Debug
            _clip["title"]->NAVIGATE->setNavigate(true);

            _GAMESTATE->run(_clip["title"],1);
        }

        if (!KEY_PRESS(ALLEGRO_KEY_F5)) _NUMBER("keyGame") = 0;
        if (KEY_PRESS(ALLEGRO_KEY_F5) && _NUMBER("keyGame") == 0)
        {
            _NUMBER("keyGame") = 1;
            _GAMESTATE->run(_clip["inGame"],1);
        }
    })
    ->RENDER(
    {
        int index = 0;
        for (auto& it: _ROOT->NAVISTATE->_stackClip)
        {
            if (nullptr != it)
                al_draw_textf
                (
                    _asset->GET_FONT("gameFont"),
                    al_color_name("gold"),
                    10, 10 + index*12, 0,
                    "%i = %s", // : size = %u",
                    index,
                    it->_name.c_str()
                    //_ROOT->NAVISTATE->current()->_name.c_str(),
                    //_ROOT->NAVISTATE->_stackClip.size()

                );

            ++index;
        }

        al_draw_textf
        (
            _asset->GET_FONT("gameFont"),
            al_map_rgb(250,250,50),
            120,10,0,
            "_currentStage = %i",
            _currentStage
        );

    });
    //}

}
