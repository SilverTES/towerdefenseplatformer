#include "MyGame.h"

void MyGame::makeClipInGame()
{

    _clip["camera"]//{
        ->appendTo(_clip["inGame"])//->setupParent()
        ->setActive(true)
        ->setPosition(0,0,0)
        //->attach<Component::Camera>()
        ->attach(new Component::Camera())
        ->CAMERA->setRectView(Rect{0,0,(VAR)_screenW,(VAR)_screenH})
        ->setNumber("shake",0)
        ->setNumber("keyShake",0)
        ->UPDATE(
        {
            if (KEY_PRESS(ALLEGRO_KEY_ENTER)) _CAMERA->setPosition(0,0);
//            if (KEY_PRESS(ALLEGRO_KEY_UP))    _CAMERA->moveY(-2);
//            if (KEY_PRESS(ALLEGRO_KEY_DOWN))  _CAMERA->moveY(2);
//            if (KEY_PRESS(ALLEGRO_KEY_LEFT))  _CAMERA->moveX(-2);
//            if (KEY_PRESS(ALLEGRO_KEY_RIGHT)) _CAMERA->moveX(2);

            if (!KEY_PRESS(ALLEGRO_KEY_RCTRL)) _NUMBER("keyShake") = 0;
            if (KEY_PRESS(ALLEGRO_KEY_RCTRL) && _NUMBER("keyShake") == 0)
            {
                _NUMBER("keyShake") = 1;
                _msgQueue->post(DAMAGE, new MsgData::Damage(80), _THIS);
            }

            ON_MESSAGE_TYPE(DAMAGE)
            {
                int damage = static_cast<MsgData::Damage*>(_MESSAGE->_data)->_damage;

                _NUMBER("shake") = damage;
                al_play_sample(_asset->GET_SAMPLE("explosion"), 0.8, 0.0, 0.3, ALLEGRO_PLAYMODE_ONCE, NULL);

                KILL_MESSAGE();
            }

            if (_NUMBER("shake") > 0)
            {
                int intensity = _NUMBER("shake")/10;

                VAR sx = Misc::random(-intensity,intensity);
                VAR sy = Misc::random(-intensity,intensity);

                _CAMERA->setPosition(sx,sy);

                --_NUMBER("shake");
            }

            _CAMERA->update();

//            VAR playerX  = _clipPlayer[PLAYER_1]->_x;
//            VAR playerY  = _clipPlayer[PLAYER_1]->_y;
//            VAR playerVX = _clipPlayer[PLAYER_1]->JUMPER->_vx;
//            VAR playerVY = _clipPlayer[PLAYER_1]->JUMPER->_vy;
//
//            if (KEY_PRESS(ALLEGRO_KEY_0))
//                _CAMERA->setPosition(playerX-(VAR)(_screenW/2), playerY-(VAR)(_screenH/2));
//
//            if (playerX < _CAMERA->_rectView._x + _window->screenW()/3)
//                if (playerVX<=0)
//                    _CAMERA->moveX(playerVX);
//
//            if (playerX > _CAMERA->_rectView._x+_CAMERA->_rectView._w - _window->screenW()/3)
//                if (playerVX>=0)
//                    _CAMERA->moveX(playerVX);
//
//            if (playerY < _CAMERA->_rectView._y + _window->screenH()/3)
//                if (playerVY<=0)
//                    _CAMERA->moveY(playerVY);
//
//            if (playerY > _CAMERA->_rectView._y+_CAMERA->_rectView._h - _window->screenH()/3)
//                if (playerVY>=0)
//                    _CAMERA->moveY(playerVY);
//
//            _THIS->updateClipRect();

        })
        ->RENDER(
        {

        });
    //};

    _clip["layer0"]//{
    ->appendTo(_clip["inGame"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,1)
    ->setCameraMoveFactor(1)
    ->UPDATE(
    {
        _grid->setPosition(_ABSX,_ABSY);

        Collision2D::resetAllZone(_THIS);
        //Collision2D::bruteSystemClip(_THIS);
        Collision2D::gridSystemZone(_THIS,_grid);

//            if (_mouse->_button & 2 && !_mouseButtonR)
//            {
//                _mouseButtonR = true;
//                MAKE_CLONE(_clip["canon"], "canonCopy")
//                ->setPosition(_xMouse, _yMouse, 2);
//
//            }
    })
    ->RENDER(
    {

    });
    //};

    _clip["layer1"]//{
    ->appendTo(_clip["inGame"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,0)
    ->setCameraMoveFactor(1)
    ->UPDATE(
    {

    })
    ->RENDER(
    {
        //Draw::grid(_ABSX,_ABSY,_screenW, _screenH,8,8,al_map_rgb(20,50,85));
    });

    //};

    _clip["layerFG"]//{
    ->appendTo(_clip["inGame"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,2)
    ->setCameraMoveFactor(1)
    ->UPDATE(
    {

    })
    ->RENDER(
    {


        //Draw::grid(_ABSX,_ABSY,_screenW, _screenH,8,8,al_map_rgb(20,50,85));
    });
    //};

    _clip["layerHUD"]//{
    ->INIT(
    {
//        MAKE_CLONE(_clip["button"],"button")
//        ->appendTo(_THIS)//->setupParent()
//        ->setActive(true)
//        ->setSize(48,8)
//        ->setString("message","selectStage")
//        ->setString("label", "MENU")
//        ->setClip("toClip", _clip["gameState"])
//        ->setPosition(8, 8);

//        for (int i=0; i<_nbPlayer; ++i)
//        {
//            MAKE_CLONE(_clip["playerHUD"],"playerHUD"+std::to_string(i+1))
//            ->appendTo(_THIS)//->setupParent()
//            ->setString("name","Player "+std::to_string(i+1))
//            ->setString("avatar","avatar0")
//            ->setActive(true)
//            ->setX(0)
//            ->UPDATE(
//            {
//                _DRAGGABLE->update();
//                _NUMBER("nbPoint") = _clipPlayer[i]->HERO->_nbPoint;
//            });
//        }


        MAKE_CLONE_ALONE(_clip["playerHUD"],"playerHUD1")
        ->appendTo(_THIS)//->setupParent()
        ->setString("name","Player 1")
        ->setString("avatar","avatar0")
        ->setNumber("player", PLAYER_1)
        ->setClip("player", _clipPlayer[PLAYER_1])
        ->setActive(true)
        ->setX(0);

        MAKE_CLONE_ALONE(_clip["playerHUD"],"playerHUD2")
        ->appendTo(_THIS)//->setupParent()
        ->setString("name","Player 2")
        ->setString("avatar","avatar1")
        ->setNumber("player", PLAYER_2)
        ->setClip("player", _clipPlayer[PLAYER_2])
        ->setActive(true)
        ->setX(240);

        MAKE_CLONE_ALONE(_clip["playerHUD"],"playerHUD3")
        ->appendTo(_THIS)//->setupParent()
        ->setString("name","Player 3")
        ->setString("avatar","avatar2")
        ->setNumber("player", PLAYER_3)
        ->setClip("player", _clipPlayer[PLAYER_3])
        ->setActive(true)
        ->setX(480);

        MAKE_CLONE_ALONE(_clip["playerHUD"],"playerHUD4")
        ->appendTo(_THIS)//->setupParent()
        ->setString("name","Player 4")
        ->setString("avatar","avatar3")
        ->setNumber("player", PLAYER_4)
        ->setClip("player", _clipPlayer[PLAYER_4])
        ->setActive(true)
        ->setX(720);

        _CLIP("boxVictory") = MAKE_CLONE_ALONE(_clip["messageBox"], "messageBox")
        ->setSize(480,320)
        ->setPivot(CENTER)
        ->INIT(
        {
            _ORIGINAL->runInit(_THIS);

            _CLIP("back") = MAKE_CLONE(_clip["button"],"button")
            ->appendTo(_THIS)//->setupParent()
            //->attach<Component::NaviNode>()
            ->attach(new Component::NaviNode())
            ->setActive(false)
            ->setSize(24,8)
            ->setPivot(MIDDLE)
            //->setString("message","ok")
            //->setClip("toClip", _THIS)
            ->setString("label", "OK")
            ->setString("message","selectStage")
            ->setClip("toClip", _clip["gameState"])
            ->setPosition(_RECT._w/2, _RECT._h-16);

        })
        ->appendTo(_THIS)
        ->setNumber("speed",16)
        ->SKIN->setRGB(140,180,210)
        ->SKIN->setId(2)
        ->setActive(true)
        ->setPosition(_screenW/2, _screenH/2)
        ->NAVIGATE->setNavigate(true)
        ->NAVIGATE->setFocus(0)
        ->UPDATE(
        {
            _ORIGINAL->runUpdate(_THIS);

            if (!KEY_PRESS(ALLEGRO_KEY_M)) _NUMBER("key") = 0;
            if (KEY_PRESS(ALLEGRO_KEY_M) && _NUMBER("key") == 0)
            {
                _NUMBER("key") = 1;

                PLAY_AT(10);

                //_pause = true; // PAUSE GAME !
                //_NAVIGATE->setNavigate(true,0,_clip["title"]);
            }

            _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

            ON_MESSAGE_TYPE(BUTTON)
            {
                std::string message = static_cast<MsgData::Button*>(_MESSAGE->_data)->_message;

                KILL_MESSAGE();

                //_NUMBER("kill") = 1;
                PLAY_AT(0);
                // Go back
                _NAVIGATE->toPrevNavigate();
            }

        })
        ->RENDER(
        {
            //_clip["messageBox"]->_render(_THIS);
            _ORIGINAL->runRender(_THIS);

            if (_NUMBER("show") == 2)
            {
                //al_draw_bitmap(_asset->GET_BITMAP("avatar0"),_ABSX+_RECT._w-80, _ABSY+10, 0);

                al_draw_multiline_text
                (
                    _mainFont,al_map_rgb(250,200,20),
                    _ABSX+10, _ABSY+10,
                    _RECT._w - 20, 16,
                    0,
                    "                             --- MESSAGE --- \n"
                    " \n"
                    " \n"
                    " \n"
                    " \n"
                    " \n"
                    "                        **** V I C T O R Y **** \n"
                    " ... \n"
                    " ... \n"
                    " ... \n"
                    " ... \n"
                );

                _CLIP("back")->setVisible(true);
                _CLIP("back")->setActive(true);

                //_pause = true;
            }
            else
            {
                _CLIP("back")->setVisible(false);
                _CLIP("back")->setActive(false);
            }

        });


    })
    ->appendTo(_clip["inGame"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,10)

    ->setNumber("Y", 0)
    ->setNumber("VY", 1)
    ->UPDATE(
    {
        if (_clip["stage"]->STAGE->_onStageWin)
        {
            _CLIP("boxVictory")->playAt(10);

            std::cout << "-- ON STAGE WIN --\n";
        }

        //std::cout << "LayerHud" ;
    })
    ->RENDER(
    {
//            for (int i=0; i<8; ++i)
//            {
//                al_draw_bitmap(_asset->GET_BITMAP("heart"),24+i*18,_screenH - 20,0);
//            }

        //_NUMBER("Y") += _NUMBER("VY");

        if (_NUMBER("Y") > 4) _NUMBER("VY") = -1;
        if (_NUMBER("Y") < 0) _NUMBER("VY") =  1;


//        for (int i=0; i<MAX_PLAYER; ++i)
//        {
//            if (nullptr != _clipPlayer[i])
//                al_draw_bitmap(_asset->GET_BITMAP("indicator1"), _clipPlayer[i]->absX(),  _clipPlayer[i]->absY() - 32+_NUMBER("Y"),0);
//
//        }

        al_draw_textf(_mainFont, al_map_rgb(25,250,150),_screenW/2, 10, -1, "BANK : %i",_bank);

        al_draw_bitmap(_asset->GET_BITMAP("indicator1"), _clipPlayer[PLAYER_1]->absX(),  _clipPlayer[PLAYER_1]->absY() - 32+_NUMBER("Y"),0);
        al_draw_bitmap(_asset->GET_BITMAP("indicator2"), _clipPlayer[PLAYER_2]->absX(),  _clipPlayer[PLAYER_2]->absY() - 32+_NUMBER("Y"),0);
        al_draw_bitmap(_asset->GET_BITMAP("indicator3"), _clipPlayer[PLAYER_3]->absX(),  _clipPlayer[PLAYER_3]->absY() - 32+_NUMBER("Y"),0);
        al_draw_bitmap(_asset->GET_BITMAP("indicator4"), _clipPlayer[PLAYER_4]->absX(),  _clipPlayer[PLAYER_4]->absY() - 32+_NUMBER("Y"),0);

        if (Clip::_showClipInfo)
            _grid->render();


    });
    //};

    _clip["tilemap0"]//{
    ->appendTo(_clip["layer1"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,1)
    //->attach<Component::TileMap2D>()
    ->attach(new Component::TileMap2D())
    ->TILEMAP2D->setup(Rect{0,0,(VAR)_screenW,(VAR)_screenH})
    ->setCameraMoveFactor(1)

    ->UPDATE(
    {
        _TILEMAP2D->update();
    })
    ->RENDER(
    {
        _TILEMAP2D->render();
        _TILEMAP2D->showGrid(al_map_rgba(10,40,60,150));
    });
    //};

    _clip["tilemapFG"]//{
    ->appendTo(_clip["layerFG"])//->setupParent()
    ->setActive(true)
    ->setPosition(0,0,5)
    //->attach<Component::TileMap2D>()
    ->attach(new Component::TileMap2D())
    ->TILEMAP2D->setup(Rect{0,0,(VAR)_screenW,(VAR)_screenH})
    ->setCameraMoveFactor(1)

    ->setNumber("mouseButton",0)
    ->UPDATE(
    {
        _TILEMAP2D->update();
    })
    ->RENDER(
    {
        _TILEMAP2D->render();
        _TILEMAP2D->showGrid(al_map_rgba(10,40,60,150));

        // Debug Clip "atom"

        VAR mouseMapX = std::floor(_xMouse/24);
        VAR mouseMapY = std::floor(_yMouse/24);

        Draw::rect(Rect{mouseMapX*24,mouseMapY*24,24,24},al_map_rgb(250,250,0),0);

        if (_mouse->_button != 2) _NUMBER("mouseButton") = 0;
        if (_mouse->_button == 2 && _NUMBER("mouseButton") == 0)
        {
            _NUMBER("mouseButton") = 1;

            VAR posY = mouseMapY*24+20;

            MAKE_CLONE(_clip["atom"],"atom")
            ->setPosition(mouseMapX*24+12,posY,5)
            ->setNumber("startY", posY)
            ->setNumber("goalY", posY-32);

        }

    });
    //};

}
