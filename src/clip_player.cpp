#include "MyGame.h"


void MyGame::makeClipPlayer()
{
    _clip["hero"]//{
    ->setParent(_clip["layer0"])
//    ->setup(_window, _asset->GET_FONT("gameFont"), _mouse)
    ->setType(CLIP_PLAYER)
    ->setActive(false)
    ->setSize(16,28)
    ->setPivot(8,14)
    ->setPosition(0,0,4)
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,16,16})     // Player himself
    //->setCollideZone(1,Rect{-24,-24,24,24}) // Player zone
    ->setCollideZone(2,Rect{0,0,0,0})       // Player viewFinder/Sniper
    //->attach<Component::Draggable>()
    ->attach(new Component::Draggable())
    ->setMouse(_mouse)
    ->DRAGGABLE->setLimitRect(Rect{0,0,(VAR)_screenW,(VAR)_screenH-84})
    //->attach<Component::Jumper>()
    ->attach(new Component::Jumper())
    ->JUMPER->setGravity(0.28)
    ->JUMPER->setSpeedMax(2,6.8)
    ->JUMPER->setTileMap(_clip["tilemap0"]->TILEMAP2D)
    //->JUMPER->setLimitMoveRect(Rect{0,0,(VAR)_screenW,(VAR)_screenH})
    ->JUMPER->setJump(true)
    ->JUMPER->doFall()
    //->attach<Component::Command>()
    ->attach(new Component::Command())
    //->attach<Component::Unit>()
    ->attach(new Component::Unit())
    ->UNIT->setEnergy(20)
    ->UNIT->setEnergyBar(20)
    ->UNIT->setEnergyColor(al_map_rgb(10,220,20))
    ->UNIT->setEnergyColorBG(al_map_rgb(220,10,20))

    ->setNumber("colorR",255)
    ->setNumber("colorG",255)
    ->setNumber("colorB",255)

    ->setNumber("padStart",0)

    ->setNumber("nbPoint",0)

    ->setNumber("type", ROLE_SNIPER)
    ->setNumber("special", 0)

    //->attach<Component::Hero>()
    ->attach(new Component::Hero())
    ->INIT(
    {
        //_HERO->_nbAmmo =  _gameConfig["nbAmmo"];

        // Atomiser init
        _HERO->_speedAtom = 8;
    })
    ->UPDATE(
    {
        _THIS->updateCollideZone(0,_RECT);
        //_THIS->updateCollideZone(1,_RECT+Rect{-12,-8,24,16});
        _THIS->updateCollideZone(2, Rect{_HERO->_sniperX-1,_HERO->_sniperY-1, 2, 2});

        ON_MESSAGE()
        {
            ON_MESSAGE_TYPE(HEAL)
            {
                int energy = static_cast<MsgData::Heal*>(_MESSAGE->_data)->_energy;

                _UNIT->_energy += energy;

                if (_UNIT->_energy > _UNIT->_energyMax)
                    _UNIT->_energy = _UNIT->_energyMax;

            }

            ON_MESSAGE_TYPE(AMMO)
            {
                int nb = static_cast<MsgData::Item*>(_MESSAGE->_data)->_nb;

                int result = _HERO->_nbAmmo + nb;
                if (result <= _HERO->_nbAmmoMax)
                {
                    _HERO->_nbAmmo += nb;
                }
                else
                {
                    _HERO->_nbAmmo = _HERO->_nbAmmoMax;
                    int exces = result - _HERO->_nbAmmo;

                    _HERO->_nbPoint += exces;
                    _bank += exces;

                }

                KILL_MESSAGE();
            }

            ON_MESSAGE_TYPE(SCORE)
            {
                int score = static_cast<MsgData::Score*>(_MESSAGE->_data)->_score;
                _HERO->_nbPoint += score;

                KILL_MESSAGE();
            }
        }

        ON_COLLIDE_ZONE(0)
        {
            ON_COLLIDE_ZONE_CLIP_NAME(0,"exit0",0)
            {
                if (_COMMAND->onButton(PAD_X))
                    if (_start == 0 )
                    {
                        _start = 1;
                        al_play_sample(_asset->GET_SAMPLE("alert"), 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                        _centerX = _ABSX + _OX;
                        _centerY = _ABSY + _OY;
                        _maxRadius = 0.8;
                        _radius = 0.0;
                        _speed = 0.01;
                        _useShader = true;
                    }

            }
        }

        --_HERO->_hitEnemy;

        if (_HERO->_hitEnemy <= 0)
        {
            _HERO->_hitEnemy = 0;
            _HERO->_hitEnemyVX = 0;

            ON_COLLIDE_ZONE(0)
            {
                ON_COLLIDE_ZONE_CLIP_NAME(0,"miniAlien",0)
                {
                    if (_HERO->_kill == 0)
                        _HERO->_kill = 1;

                    _HERO->_hitEnemy = _HERO->_hitTimer;

                    _HERO->_hitEnemyX = _PARENT->index(_ID_ZONE_COLLIDE_BY(0))->_x;
                    _HERO->_hitEnemyY = _PARENT->index(_ID_ZONE_COLLIDE_BY(0))->_y;

                }

                ON_COLLIDE_ZONE_CLIP_NAME(0,"maxiAlien",0)
                {
                    if (_HERO->_kill == 0)
                        _HERO->_kill = 1;

                    _HERO->_hitEnemy = _HERO->_hitTimer;

                    _HERO->_hitEnemyX = _PARENT->index(_ID_ZONE_COLLIDE_BY(0))->_x;
                    _HERO->_hitEnemyY = _PARENT->index(_ID_ZONE_COLLIDE_BY(0))->_y;
                }

                ON_COLLIDE_ZONE_CLIP_NAME(0,"destructor",0)
                {
                    if (_HERO->_kill == 0)
                        _HERO->_kill = 1;

                    _HERO->_hitEnemy = _HERO->_hitTimer;

                    _HERO->_hitEnemyX = _PARENT->index(_ID_ZONE_COLLIDE_BY(0))->_x;
                    _HERO->_hitEnemyY = _PARENT->index(_ID_ZONE_COLLIDE_BY(0))->_y;
                }
            }

            if (_HERO->_isBlinking)
                _HERO->_isBlinking = false;
        }
        else
        {
            ++_HERO->_tempoBlink;

            if (_HERO->_tempoBlink > 3)
            {
                _HERO->_tempoBlink = 0;
                _HERO->_isBlinking = ! _HERO->_isBlinking;
            }

            if (_HERO->_hitEnemy > _HERO->_looseControl)
                _JUMPER->doMove(_HERO->_hitEnemyVX);
        }

        if (_HERO->_kill == 2)
        {
            //_HERO->_nbAmmo -= 20;
            _HERO->_kill = 0;
            _UNIT->_energy -= 5;

            if (_UNIT->_energy <= 0)
            {
                _UNIT->_energy = _UNIT->_energyMax;

                _X = _clip["stage"]->STAGE->_vecExit[0]->_x;
                _Y = _clip["stage"]->STAGE->_vecExit[0]->_y;

                _JUMPER->_isJump = true;
                _JUMPER->_vy = -1;
                _JUMPER->doFall();

                al_play_sample(_asset->GET_SAMPLE("loose"), 0.6, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            }

        }

        if (_HERO->_kill == 1)
        {

            MAKE_CLONE(_clip["score"], "cloneScore")
            ->setX(_X)
            ->setY(_Y-16)
            ->setNumber("startY",_Y)
            ->setNumber("goalY", _Y-24)
            ->setString("message","5") //+std::to_string(25))
            ->setNumber("r",250)
            ->setNumber("g",10)
            ->setNumber("b",10)
            ->setNumber("nbPoint", 10);

            al_play_sample(_asset->GET_SAMPLE("hurt"), 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            _HERO->_kill = 2;

            //std::cout << "Hit enemyVX : "<< _HERO->_enemyVX << "\n";

            if (_X < _HERO->_hitEnemyX)
                _HERO->_hitEnemyVX = -2;
            else
                _HERO->_hitEnemyVX =  2;


            if (_Y <= _HERO->_hitEnemyY)
            {
                _JUMPER->_jump = false;
                _JUMPER->doJump(-4);
            }
            else
            {
                _JUMPER->doFall();
            }



        }


        _DRAGGABLE->update();

        if (!_DRAGGABLE->_isDrag)
            _JUMPER->update();
        else
            _JUMPER->_isJump = true;

        _UNIT->update();

// Process Input !

        if (_COMMAND->onButton(PAD_L))
        {
            _NUMBER("special") = 1;
            _JUMPER->_vx = 0;
        }
        else
        {
            _NUMBER("special") = 0;
            _HERO->_sniperX = _X;
            _HERO->_sniperY = _Y;
        }

        if (_NUMBER("special") == 0)
        {
            if (!_COMMAND->onButton(PAD_UP)) _HERO->_teleport = 0;
            if (!_COMMAND->onButton(PAD_X)) _HERO->_refund = 0;


            // Create teleport
            if (_COMMAND->onButton(PAD_UP) &&
                _COMMAND->onButton(PAD_A) &&
                _HERO->_teleport == 0 &&
                _HERO->_nbAmmo >= 80)
            {
                _HERO->_teleport = 1;
                al_play_sample(_asset->GET_SAMPLE("warp"), 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                MAKE_CLONE(_clip["teleport"], "teleport")
                ->setPosition(_X,_Y-8,2);

                _HERO->_nbAmmo -= 80;
            }

            // Get back Ammo/Money
            if (_COMMAND->onButton(PAD_X) && _HERO->_refund == 0)
            {
                _HERO->_refund = 1;

                if (_clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,BUILDABLE) &&
                    !_clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,EMPTY) &&
                    _clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,ID) != -1)
                {
                    int id = _clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,ID);

                    Clip* clip = _PARENT->index(id);

                    if (nullptr != clip)
                    {
                        if (!clip->UNIT->_isBuild)
                        {
                            int price = 0;
                            if (clip->_mapNumber["level"] == 1) price = 10;
                            if (clip->_mapNumber["level"] == 2) price = 30;
                            if (clip->_mapNumber["level"] == 3) price = 80;

                            al_play_sample(_asset->GET_SAMPLE("coin"), 0.5, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                            //printf("Canon Level UP !\n");
                            _HERO->_nbAmmo += price;


                            MAKE_CLONE(_clip["score"], "cloneScore")
                            ->setX(_X)
                            ->setY(_Y-16)
                            ->setNumber("startY",_Y-16)
                            ->setNumber("goalY", _Y-24)
                            ->setString("message","REFUND "+std::to_string(price))
                            ->setNumber("r",50)
                            ->setNumber("g",120)
                            ->setNumber("b",250)
                            ->setNumber("nbPoint", 25);

                            if (nullptr != clip->UNIT)
                                clip->UNIT->setEnergy(0);
                        }



                    }

                }

            }


            // Create + Level Up Tower !
            if (!_COMMAND->onButton(PAD_A)) _HERO->_create = 0;

            if (_COMMAND->onButton(PAD_A) && _HERO->_create == 0)
            {
                _HERO->_create = 1;

                //bool levelUp = false;

                bool levelUp = false;

                // Upgrade Canon level 2

                if (_clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,BUILDABLE))
                {
                    if (_HERO->_nbAmmo >= 60 && !levelUp)
                    {
                        if (_clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,ID) != -1)
                        {
                            int id = _clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,ID);

                            Clip* clip = _PARENT->index(id);

                            if (nullptr != clip)
                            {
                                if (!clip->UNIT->_isBuild)
                                {
                                    if (clip->_mapNumber["level"] == 1)
                                    {
                                        clip->setNumber("level",2);

                                        if (nullptr != clip->UNIT)
                                        {
                                            clip->UNIT->startBuild(8,4);
                                            clip->UNIT->setEnergyMax(120);
                                            //clip->UNIT->setEnergy(120);
                                        }

                                        //al_play_sample(_asset->GET_SAMPLE("build"), 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                                        _HERO->_nbAmmo -= 60;

                                        levelUp = true;

                                        MAKE_CLONE(_clip["score"], "cloneScore")
                                        ->setX(_X)
                                        ->setY(_Y-16)
                                        ->setNumber("startY",_Y-16)
                                        ->setNumber("goalY", _Y-24)
                                        ->setString("message","CANON N2") //+std::to_string(25))
                                        ->setNumber("r",250)
                                        ->setNumber("g",10)
                                        ->setNumber("b",50)
                                        ->setNumber("nbPoint", 25);

                                        //printf("Canon Level UP 1 !\n");
                                    }
                                }


                            }


                        }

                    }

                    // Upgrade Canon level 3

                    if (_HERO->_nbAmmo >= 160 && !levelUp)
                    {
                        if (_clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,ID) != -1)
                        {
                            int id = _clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,ID);

                            Clip* clip = _PARENT->index(id);

                            if (nullptr != clip)
                            {

                                if (!clip->UNIT->_isBuild)
                                {
                                    if (clip->_mapNumber["level"] == 2)
                                    {
                                        clip->setNumber("level",3);

                                        if (nullptr != clip->UNIT)
                                        {
                                            clip->UNIT->startBuild(8,4);
                                            clip->UNIT->setEnergyMax(200);
                                            //clip->UNIT->setEnergy(200);
                                        }

                                        //al_play_sample(_asset->GET_SAMPLE("build2"), 0.6, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                                        _HERO->_nbAmmo -= 160;

                                        levelUp = true;

                                        MAKE_CLONE(_clip["score"], "cloneScore")
                                        ->setX(_X)
                                        ->setY(_Y-16)
                                        ->setNumber("startY",_Y-16)
                                        ->setNumber("goalY", _Y-24)
                                        ->setString("message","CANON N3") //+std::to_string(25))
                                        ->setNumber("r",250)
                                        ->setNumber("g",210)
                                        ->setNumber("b",50)
                                        ->setNumber("nbPoint", 25);

                                        //printf("Canon Level UP 2 !\n");
                                    }
                                }


                            }

                        }

                    }

                    // Create Canon !

                    if (_clip["tilemap0"]->TILEMAP2D->getTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,EMPTY)  && _HERO->_nbAmmo >= 20)
                    {
                        Clip* clip = MAKE_CLONE(_clip["canon"], "canon")
                        ->setActive(true)
                        ->UNIT->startBuild(8,4)
                        ->setPosition(_JUMPER->_posMapX*_JUMPER->_tileMap2D->_tileW+_OX+4,
                                      _JUMPER->_posMapY*_JUMPER->_tileMap2D->_tileH+_OY-2, 2);

                        _clip["tilemap0"]->TILEMAP2D->setTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,EMPTY,0);
                        _clip["tilemap0"]->TILEMAP2D->setTileProperty(_JUMPER->_posMapX,_JUMPER->_posMapY,ID,clip->_id);


                        //al_play_sample(_asset->GET_SAMPLE("build"), 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                        _HERO->_nbAmmo -= 20;

                        levelUp = true;

                        //Clip* test =
                        MAKE_CLONE(_clip["score"], "cloneScore")
                        ->setX(_X)
                        ->setY(_Y-16)
                        ->setNumber("startY",_Y-16)
                        ->setNumber("goalY", _Y-24)
                        ->setString("message","CANON N1") //+std::to_string(25))
                        ->setNumber("r",50)
                        ->setNumber("g",250)
                        ->setNumber("b",50)
                        ->setNumber("nbPoint", 25);

                        //printf("Canon Builded : %s, %i !\n", test->_name.c_str(), test->id());

                        //_PARENT->showAll();
                    }


                }
            }


            if (!_COMMAND->onButton(PAD_Y)) _HERO->_shoot = false;
            if (_COMMAND->onButton(PAD_Y) && !_HERO->_shoot)
            {
                _HERO->_shoot = true;

                if (_HERO->_nbAmmo > 0)
                {
                    al_play_sample(_asset->GET_SAMPLE("laserBlast"), 0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                    int dx = 1;
                    if (_JUMPER->_moveL) dx = -1;
                    //if (_JUMPER->_moveR) dx =  4;

                    MAKE_CLONE(_clip["laser"],"laserCopy")
                    ->setX(_X)
                    ->setY(_Y)
                    ->setNumber("ownerID", _ID)
                    ->VELOCITY->setVXMax(8)
                    ->VELOCITY->setAX(.4)
                    ->VELOCITY->setVX(dx);
                    //->setNumber("direction",dx);

                    --_HERO->_nbAmmo ;
                }

            }

            if (_COMMAND->onButton(PAD_B))
            {
                if (!_JUMPER->_keyJump)
                {
                    _JUMPER->_keyJump = true;

                    if (!_JUMPER->_jump)
                        al_play_sample(_asset->GET_SAMPLE("jump"), 0.3, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);


                    // Platform isCollidable == 2, can go down when jump + down
                    int posMapX = std::floor(_X/_clip["tilemap0"]->TILEMAP2D->_tileW);
                    int posMapXL = std::floor((_X-12)/_clip["tilemap0"]->TILEMAP2D->_tileW);
                    int posMapXR = std::floor((_X+12)/_clip["tilemap0"]->TILEMAP2D->_tileW);
                    int posMapY = std::floor(_Y/_clip["tilemap0"]->TILEMAP2D->_tileH);

                    if (_COMMAND->onButton(PAD_DOWN) && _JUMPER->_isLand)
                    {
                        if (_clip["tilemap0"]->TILEMAP2D->getTileCollidable(posMapX, posMapY+1) == 2 ||
                            (_clip["tilemap0"]->TILEMAP2D->getTileCollidable(posMapXL, posMapY+1) == 2 &&
                             _clip["tilemap0"]->TILEMAP2D->getTileCollidable(posMapX, posMapY+1) == 0) ||
                            (_clip["tilemap0"]->TILEMAP2D->getTileCollidable(posMapXR, posMapY+1) == 2 &&
                             _clip["tilemap0"]->TILEMAP2D->getTileCollidable(posMapX, posMapY+1) == 0) )

                        {
                            //_JUMPER->_isForceFall = true;
                            //std::cout << "GO DOWN !\n";

                            _JUMPER->doJump(2);
                        }


                    }
                    else
                    {
                        _JUMPER->doJump();
                    }

                }
            }
            else
            {
                _JUMPER->_keyJump = false;
                if (_JUMPER->_isJump && _HERO->_hitEnemy <= 0)
                    _JUMPER->doFall();
            }

            if (!_COMMAND->onButton(PAD_LEFT) &&
                !_COMMAND->onButton(PAD_RIGHT))
            {
                _JUMPER->doFriction(1);
            }

            if (_HERO->_hitEnemy < _HERO->_looseControl)
            {
                if (_COMMAND->onButton(PAD_LEFT) && _JUMPER->_canMoveL)
                    _JUMPER->doMove(-2);

                if (_COMMAND->onButton(PAD_RIGHT) && _JUMPER->_canMoveR)
                    _JUMPER->doMove( 2);
            }

        }

        // Player Role
        if (_NUMBER("special") == 1)
        {

            if (_HERO->_playerRole == ROLE_SNIPER)
            {
                if (_COMMAND->onButton(PAD_UP))
                    _HERO->_sniperY -= 2;
                if (_COMMAND->onButton(PAD_DOWN))
                    _HERO->_sniperY += 2;
                if (_COMMAND->onButton(PAD_LEFT))
                    _HERO->_sniperX -= 2;
                if (_COMMAND->onButton(PAD_RIGHT))
                    _HERO->_sniperX += 2;

                if (!_COMMAND->onButton(PAD_Y))
                    _HERO->_shoot = false;

                if (_COMMAND->onButton(PAD_Y) && !_HERO->_shoot && _HERO->_tempoSniper == 0)
                {
                    _HERO->_shoot = true;

                    if (_HERO->_nbAmmo >= 20 && !_HERO->_startSniper)
                    {

                        _HERO->_startSniper = true;


                        al_play_sample(_asset->GET_SAMPLE("sniperShoot"), 0.3, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                        _HERO->_nbAmmo -= 20; // Sniper cost

                        _centerX =  _HERO->_sniperX;
                        _centerY =  _HERO->_sniperY;
                        _maxRadius = 0.2;
                        _radius = 0.0;
                        _speed = 0.01;
                        _useShader = true;

                        MAKE_CLONE(_clip["laserLine"], "laserLine")
                        ->LINE->setLine(_X,_Y,_HERO->_sniperX,_HERO->_sniperY, al_map_rgb(255,0,0),1);


                        ON_COLLIDE_ZONE(2)
                        {
                            ON_COLLIDE_ZONE_CLIP_NAME(2,"miniAlien",0)
                            {
                                _msgQueue->post(DAMAGE, new MsgData::Damage(1000,0), _PARENT->index(_ID_ZONE_COLLIDE_BY(2)) );
                            }

                            ON_COLLIDE_ZONE_CLIP_NAME(2,"maxiAlien",0)
                            {
                                _msgQueue->post(DAMAGE, new MsgData::Damage(1000,0), _PARENT->index(_ID_ZONE_COLLIDE_BY(2)) );
                            }

                            ON_COLLIDE_ZONE_CLIP_NAME(2,"destructor",0)
                            {
                                _msgQueue->post(DAMAGE, new MsgData::Damage(1000,0), _PARENT->index(_ID_ZONE_COLLIDE_BY(2)) );
                            }
                        }

                    }
                    else
                    {
                        al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.3, 0.0, 1.2, ALLEGRO_PLAYMODE_ONCE, NULL);
                    }

                }

                if (_HERO->_startSniper)
                {
                    ++_HERO->_tempoSniper;

                    if (_HERO->_tempoSniper == _HERO->_speedSniper - 10)
                        al_play_sample(_asset->GET_SAMPLE("ammoGun"), 0.4, 0.0, 1.2, ALLEGRO_PLAYMODE_ONCE, NULL);

                    if (_HERO->_tempoSniper > _HERO->_speedSniper)
                    {
                        _HERO->_startSniper = false;
                        _HERO->_tempoSniper = 0;
                        std::cout << "reload gun \n";

                    }

                }


            }

            if (_HERO->_playerRole == ROLE_ATOMISER)
            {
                if (!_COMMAND->onButton(PAD_Y))
                    _HERO->_shoot = false;

                if (_COMMAND->onButton(PAD_Y) && !_HERO->_shoot && !_HERO->_startAtom && _HERO->_nbAmmo >= 20)
                {
                    _HERO->_nbAmmo -= 20; // Atomiser cost

                    _HERO->_shoot = true;
                    _HERO->_startAtom = true;

                    if (_JUMPER->_moveL)
                        _HERO->_atomDirection = -1;
                    else
                        _HERO->_atomDirection =  1;

                    _HERO->_atomPosMapX = _JUMPER->_posMapX + _HERO->_atomDirection;
                    _HERO->_atomPosMapY = _JUMPER->_posMapY;

                }

                if (_COMMAND->onButton(PAD_LEFT) && _JUMPER->_canMoveL)
                {
                    _JUMPER->_moveL = true;                    _JUMPER->_moveR = false;
                    _JUMPER->doMove(-1);
                }
                if (_COMMAND->onButton(PAD_RIGHT) && _JUMPER->_canMoveR)
                {
                    _JUMPER->_moveL = false;                    _JUMPER->_moveR = true;
                    _JUMPER->doMove(1);
                }
            }

        }

                VAR w = _JUMPER->_tileMap2D->_tileW;
                VAR h = _JUMPER->_tileMap2D->_tileH;
                VAR posX = ( _HERO->_atomPosMapX + (_HERO->_atomPosX*_HERO->_atomDirection) )*w;
                VAR posY = _HERO->_atomPosMapY*h + 24;

                if (_HERO->_startAtom)
                {
                    ++_HERO->_tempoAtom;
                    if (_HERO->_tempoAtom > _HERO->_speedAtom)
                    {
                        _HERO->_tempoAtom = 0;

                        // check if atom is on Collidable Tile
                        if (_clip["tilemap0"]->TILEMAP2D->getTileCollidable(std::floor(posX/w), std::floor(posY/h)) > 0 )
                        {
                            MAKE_CLONE(_clip["atom"],"atom")
                            ->setPosition(posX+12, posY, 5)
                            ->setNumber("solY", posY)
                            ->setNumber("startY", posY)
                            ->setNumber("goalY", posY-32);

                            ++_HERO->_atomPosX;
                            if (_HERO->_atomPosX > _HERO->_atomSize)
                            {
                                _HERO->_atomPosX = 0;
                                _HERO->_startAtom = false;
                            }
                        }
                        else
                        {
                            _HERO->_atomPosX = 0;
                            _HERO->_startAtom = false;
                        }


                    }

                }


        if (_HERO->_nbAmmo < 0 )
            _HERO->_nbAmmo = 0;

    })
    ->RENDER(
    {
        int direction = 0;
        if (_JUMPER->_moveL) direction = 1;

//            al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
//            Draw::rectFill(_THIS->getCollideZone(1)->_rect, al_map_rgba(100,250,150,8));
//            al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

//            al_draw_tinted_bitmap
//            (
//                _asset->GET_BITMAP("slime"),
//                al_map_rgb
//                (
//                    _NUMBER("colorR"),
//                    _NUMBER("colorG"),
//                    _NUMBER("colorB")
//                ),
//                std::floor(_ABSX), std::floor(_ABSY),
//                direction
//            );

        int r = _NUMBER("colorR");
        int g = _NUMBER("colorG");
        int b = _NUMBER("colorB");

        if (_HERO->_isBlinking)
        {
            r = 0;
            g = 0;
            b = 0;
        }

        if (_HERO->_playerRole == ROLE_SNIPER)
        {
            al_draw_tinted_bitmap
            (
                _asset->GET_BITMAP("chara0"),
                al_map_rgb
                (
                    r,
                    g,
                    b
                ),
                std::floor(_ABSX)-4, std::floor(_ABSY),
                direction
            );
        }

        if (_HERO->_playerRole == ROLE_ATOMISER)
        {
            al_draw_tinted_bitmap
            (
                _asset->GET_BITMAP("chara1"),
                al_map_rgb
                (
                    r,
                    g,
                    b
                ),
                std::floor(_ABSX)-4, std::floor(_ABSY),
                direction
            );
        }


        al_draw_textf
        (
            _mainFont,
            al_map_rgb(150,150,150),
            _ABSX+_OX, _ABSY - 16,
            -1,
            "%i",
            //_COMMAND->_player->_name.c_str(),
            _HERO->_nbAmmo
        );

//            DRAW_TEXTF
//            (
//                al_map_rgb(50,250,100),
//                _ABSX+_OX-24, _ABSY - 8,
//                -1,
//                "%i",
//                _JUMPER->_isLimitMoveRect
//            );

        _JUMPER->render();

        _THIS->showRect(al_map_rgba(115,120,0,50))
        ->showPivot(al_map_rgb(255,0,255),2);
        //->renderCollideZone(1,al_map_rgb(0,150,25));


        if (_NUMBER("special") == 1)
        {

            if (_HERO->_playerRole == ROLE_SNIPER)
            {
                //glEnable (GL_LINE_STIPPLE);

                //glLineStipple (1, 0x0101);  /*  dotted  */
                //glLineStipple (1, 0x00FF);  /*  dashed  */
                //glLineStipple (1, 0x1C47);  /*  dash/dot/dash  */

                Draw::lineAA(_ABSX+_OX, _ABSY+_OY, _HERO->_sniperX, _HERO->_sniperY, RGBA{155,0,0,100},1);

                //glDisable(GL_LINE_STIPPLE);

                VAR barMax = 17;
                VAR barY = -12;

                Draw::line
                (
                    _HERO->_sniperX - _OX,
                    _HERO->_sniperY + barY,
                    _HERO->_sniperX - _OX + barMax,
                    _HERO->_sniperY + barY,
                    al_map_rgb(250,0,0),
                    0
                );

                VAR bar = (_HERO->_tempoSniper * barMax) / _HERO->_speedSniper;

                Draw::line
                (
                    _HERO->_sniperX - _OX,
                    _HERO->_sniperY + barY,
                    _HERO->_sniperX - _OX + bar,
                    _HERO->_sniperY + barY,
                    al_map_rgb(250,250,0),
                    0
                );

                //std::string sniperState = "LOAD";

                if (_HERO->_tempoSniper == 0)
                {
                    //sniperState = "READY";

                    al_draw_tinted_bitmap
                    (
                        _asset->GET_BITMAP("sniper"),
                        al_map_rgb
                        (
                            _NUMBER("colorR"),
                            _NUMBER("colorG"),
                            _NUMBER("colorB")
                        ),
                        std::floor(_HERO->_sniperX) - 7,
                        std::floor(_HERO->_sniperY) - 7,
                        0
                    );
                }
                else
                {
                    al_draw_tinted_bitmap
                    (
                        _asset->GET_BITMAP("sniper"),
                        al_map_rgba
                        (
                            250,
                            250,
                            250,
                            50
                        ),
                        std::floor(_HERO->_sniperX) - 7,
                        std::floor(_HERO->_sniperY) - 7,
                        0
                    );
                }

//                    DRAW_TEXTF
//                    (
//                        al_map_rgb(250,50,10),
//                        std::floor(_HERO->_sniperX),
//                        std::floor(_HERO->_sniperY) - 16,
//                        -1,
//                        "%s",
//                        sniperState.c_str()
//
//                    );

            }

            if (_HERO->_playerRole == ROLE_ATOMISER)
            {
                VAR w = _JUMPER->_tileMap2D->_tileW;
                VAR h = _JUMPER->_tileMap2D->_tileH;
                VAR posX = 0;
                VAR posY = std::floor((_ABSY+_OY)/h);


                if (_JUMPER->_moveL)
                    posX = std::floor((_ABSX+_OX)/w)-1;
                else
                    posX = std::floor((_ABSX+_OX)/w)+1;

                Draw::lineAA(_ABSX+_OX, _ABSY+_OY,posX*w+w/2,posY*h+h, RGBA{0,120,250,150},2);
                Draw::rect(Rect{posX*w,posY*h+h,w,-2}, al_map_rgba(0,120,250,200), 0);
            }

        }

        _UNIT->render();

        // Debug
        if (Collision2D::pointRect(Vec2{_xMouse, _yMouse}, _RECT))
            _THIS->showComponent(_mainFont,_xMouse, _yMouse+32, al_map_rgb(255,25,150));

    });
    //};


    for (auto i = 0; i < MAX_PLAYER; ++i)
    {
        std::string name = "player" + std::to_string(i+1);

        _clipPlayer[i] = MAKE_CLONE(_clip["hero"], "player" + std::to_string(i))
        ->COMMAND->setPlayer(new ::Player(name))
        ->COMMAND->setKeyState(&_keyState)
        ->COMMAND->loadController(_inputConfigJson["controller"]["pad"+std::to_string(i+1)]);
    }



}
