#include "MyGame.h"

int MyGame::init()
{

//    std::cout << "Skin type         = " << Component::Skin::type << "\n";
//    std::cout << "Command type      = " << Component::Command::type << "\n";
//    std::cout << "Draggable type    = " << Component::Draggable::type << "\n";
//    std::cout << "Gui type          = " << Component::Gui::type << "\n";
//    std::cout << "Line type         = " << Component::Line::type << "\n";
//    std::cout << "TileMap2D type    = " << Component::TileMap2D::type << "\n";
//    std::cout << "Transition type   = " << Component::Transition::type << "\n";
//
//    std::cout << "NaviState type   = " << Component::NaviState::type << "\n";
//    std::cout << "NaviNode type   = " << Component::NaviNode::type << "\n";
//    std::cout << "Navigate type   = " << Component::Navigate::type << "\n";

    srand(time(0));

    _gameConfig = File::loadJson("data/gameConfig.json");

    std::string title = _gameConfig["name"];

    _name         = title.c_str();
    _screenW      = _gameConfig["screenW"];
    _screenH      = _gameConfig["screenH"];

    _scaleWin     = _gameConfig["scaleWin"];
    _scaleFull    = _gameConfig["scaleFull"];
    _isFullScreen = _gameConfig["fullScreen"];
    _isVsync      = _gameConfig["vsync"];
    _limitFPS     = _gameConfig["limitFPS"];
    _isSmooth     = _gameConfig["smooth"];

    //std::cout << "-- gameconfig : " << _gameConfig["data"][0]["type"] << "\n";
    _eventQueue = al_create_event_queue();

    _window->init(_name,
                  _screenW, _screenH,
                  _scaleWin, _scaleFull,
                  _isFullScreen, _isVsync, _isSmooth);

    _framerate = new Framerate(_eventQueue, _gameConfig["framerate"]);

    //printf("%d",al_get_display_option(_window->display(),ALLEGRO_SAMPLE_BUFFERS));

    al_hide_mouse_cursor(_window->display());
    sf::Joystick::update();
    _msgQueue = new MessageQueue();
    _mouse = new Input::Mouse(); // Create Mouse before make Clip who need mouse !!

    initAsset();

    makeClip(); // Toujours � la fin du init, sinon risque bug : ressource non charger avant utilisation !
    Clip::hideClipInfo();







    return mlog("init OK \n");
}

int MyGame::done()
{
//    for (auto i = 0; i < MAX_PLAYER; ++i)
//    {
//        Misc::kill(_clipPlayer[i]);
//    }

    Misc::kill(_clip["root"]);
    _clip.clear();

    Misc::kill(_mouse);
    Misc::kill(_grid);
    Misc::kill(_asset);
    Misc::kill(_msgQueue);

    Misc::kill(_framerate);

    doneAsset();

    al_destroy_shader(_myShader);
    al_destroy_event_queue(_eventQueue);

    return mlog("done OK \n");
}

void MyGame::update()
{

    if (_limitFPS)
        al_wait_for_event(_eventQueue, &_event);
    //_timerTic = al_get_next_event(_eventQueue, &_event);

    al_flush_event_queue(_eventQueue);

    al_get_keyboard_state(&_keyState);
    al_get_mouse_state(&_mouseState);
    _window->getMouse(&_mouseState, _xMouse, _yMouse);

    Input::Button::onPress("up", KEY_PRESS(ALLEGRO_KEY_UP) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_UP)) );
    Input::Button::onPress("down", KEY_PRESS(ALLEGRO_KEY_DOWN) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_DOWN)) );
    Input::Button::onPress("left", KEY_PRESS(ALLEGRO_KEY_LEFT) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_LEFT)) );
    Input::Button::onPress("right", KEY_PRESS(ALLEGRO_KEY_RIGHT) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_RIGHT)) );


    Input::Button::oncePress("valid",KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

//    if (_mouseState.buttons & 1)
//    {
////        int zoom = 1;
////        _window->isFullScreen() ?
////            zoom = _window->scaleFull():
////            zoom = _window->scaleWin();
//
//        _centerX = _xMouse;
//        _centerY = _yMouse;
//        _radius = 0.0;
//        _speed = 0.01;
//        _useShader = true;
//    }

    // ShockWave Update !
    _radius += _speed;

    if (_radius > _maxRadius)
    {
        //_speed = -0.01;
        _useShader = false;
    }

//    if (_radius < 0)
//    {
//        _radius = 0;
//        _speed = 0;
//        _useShader = false;
//    }


    if (!(_mouseState.buttons & 1)) _mouseButtonL = false;
    if (!(_mouseState.buttons & 2)) _mouseButtonR = false;

    _shoot = false;

    if (KEY_PRESS(ALLEGRO_KEY_ESCAPE))
        _quit = true;

    if (!KEY_PRESS(ALLEGRO_KEY_P)) _keyPause = false;
    if (KEY_PRESS(ALLEGRO_KEY_P) && !_keyPause)
    {
        _keyPause = true;
        _pause = !_pause;

        if (_gameOver)
            _quit = true;
    }


    if (!KEY_PRESS(ALLEGRO_KEY_SPACE)) _keyFull = false;
    if (KEY_PRESS(ALLEGRO_KEY_SPACE) && !_keyFull)
    {
        _keyFull = true;
        //_window->toggleFullScreen(-1);
        _window->toggleFullScreen(0);
    }

    if (!KEY_PRESS(ALLEGRO_KEY_TAB)) _keySwitch = false;
    if (KEY_PRESS(ALLEGRO_KEY_TAB) && !_keySwitch)
    {
        _keySwitch = true;
        _window->switchMonitor(-1);
    }

    if (!KEY_PRESS(ALLEGRO_KEY_ALT)) _keyAlt = false;
    if (KEY_PRESS(ALLEGRO_KEY_ALT) && !_keyAlt)
    {
        _keyAlt = true;
        std::cout << "--- MESSAGES ---\n";
        //_msgQueue->showAll();
    }

    if (KEY_PRESS(ALLEGRO_KEY_F1))
        Clip::showClipInfo();

    if (KEY_PRESS(ALLEGRO_KEY_F2))
        Clip::hideClipInfo();


    _mouse->update(_xMouse, _yMouse, _mouseState.buttons);

    sf::Joystick::update();

    if (!_pause)
    {
        _clip["layer0"]->setActive(true);
        _clip["stage"]->setActive(true);
    }
    else
    {
        _clip["layer0"]->setActive(false);
        _clip["stage"]->setActive(false);
    }

    //if (_timerTic)
    _clip["root"]->update();
    _msgQueue->dispatch();

    _framerate->pollFramerate();



    _newTime = al_get_time();
    _deltaTime = _newTime - _oldTime;
    _fps = 1.0f/_deltaTime;
    _oldTime = _newTime;

    ++_currentFps;
    _vecFps.push_back(_fps);

    if (_currentFps > 8)
    {
        _currentFps = 0;
        _averageFPS = accumulate(_vecFps.begin(), _vecFps.end(), 0.0) / _vecFps.size();
        _vecFps.clear();
    }

}

void MyGame::render()
{
    _window->beginRender();

    //al_clear_to_color(al_map_rgb(15,30,45));
    al_clear_to_color(al_map_rgb(0,0,0));

    _clip["root"]->render();

    if (nullptr != _framerate)
        al_draw_textf(_mainFont, al_map_rgb(205,200,200), _screenW-80, 12, 0, "FPS: %i", _framerate->getFramerate());

    al_draw_textf(_mainFont, al_map_rgb(205,200,200), _screenW -80, 2, 0, "FPS: %.1f", _averageFPS);

//
//   _newTime = al_get_time();
//   float fps = 1.0f/(_newTime - _oldTime);
//   _oldTime = _newTime;
//
//    al_draw_textf(_mainFont, al_map_rgb(205,200,200), _screenW -80, 24, 0, "FPS: %.1f", fps);

    al_draw_bitmap(_mouseCursor, _xMouse, _yMouse, 0);
    al_draw_textf(_mainFont, al_map_rgb(205,200,200), _xMouse, _yMouse+14, 0, "%.0f,%.0f", _xMouse, _yMouse);


 // Rotate the three flares in the background while we wait for input
//    angle1 += PI/540; if(angle1 > ALLEGRO_PI*2) angle1 -= ALLEGRO_PI*2;
//    angle2 -= PI/720; if(angle2 < 0) angle2 += ALLEGRO_PI*2;
//    angle3 += PI/900; if(angle3 > ALLEGRO_PI*2) angle3 -= ALLEGRO_PI*2;
//
//    VAR fx = 400 + _clip["layer0"]->absX();
//    VAR fy = 40 + _clip["layer0"]->absY();
//
//    int fw = al_get_bitmap_width(_asset->GET_BITMAP("flare"));
//    int fh = al_get_bitmap_height(_asset->GET_BITMAP("flare"));
//
//    al_draw_tinted_scaled_rotated_bitmap(_asset->GET_BITMAP("flare"), al_map_rgba(250, 0, 0,10), fw/2, fh/2, fx, fy, .2, .2, angle1, 0);
//    al_draw_tinted_scaled_rotated_bitmap(_asset->GET_BITMAP("flare"), al_map_rgba(0, 250, 0,10), fw/2, fh/2, fx, fy, .2, .2, angle2, 0);
//    al_draw_tinted_scaled_rotated_bitmap(_asset->GET_BITMAP("flare"), al_map_rgba(0, 0, 250,10), fw/2, fh/2, fx, fy, .2, .2, angle3, 0);

        // Blit buffer to display !
        al_set_target_backbuffer(_window->display());
        al_clear_to_color(al_map_rgb(0,0,0));

        // Use shader here before blit buffer to display !!

        if (_useShader) al_use_shader(_myShader);
        al_set_shader_sampler("tex", _window->buffer(), 0);

        float center[2] = {_centerX, _screenH-_centerY};
        //float surface[2] = { al_get_bitmap_width(myImage), al_get_bitmap_height(myImage) };
        float surface[2] = { float(_screenW), float(_screenH) };
        //float center[2] = { mouseXf, mouseYf };
        //float resolution[2] = { 200, 200 };
        float shockParams[3] = {10.0, 0.8, 0.1};

        al_set_shader_float("time", _radius);
        al_set_shader_float_vector("center", 2, &center[0], 1);
        al_set_shader_float_vector("surface", 2, &surface[0], 1);
        //al_set_shader_float_vector("resolution", 2, &resolution[0], 1);
        al_set_shader_float_vector("shockParams", 3, &shockParams[0], 1);

    _window->endRender();

    if (_event.type == ALLEGRO_EVENT_TIMER)
        al_flip_display();
}

