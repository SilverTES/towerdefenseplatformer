#include "MyGame.h"

int MyGame::initAsset()
{

    _asset = Asset::loadJSON("data/mainAsset.json");

    _mainFont    = _asset->GET_FONT("myFont");
    _mouseCursor = _asset->GET_BITMAP("mouseCursor");

    _spriteBoom =
    {
        (new SpriteFX(_asset->GET_BITMAP("boom")))
        ->addFrame(new Frame( 0, Rect{0,4,73,65},    0,36,45))
        ->addFrame(new Frame( 1, Rect{83,4,73,65},   0,36,45))
        ->addFrame(new Frame( 2, Rect{167,24,52,41}, 0,26,41))
        ->addFrame(new Frame( 3, Rect{228,23,55,41}, 0,27,41))
        ->addFrame(new Frame( 4, Rect{290,22,58,42}, 0,29,42))
        ->addFrame(new Frame( 5, Rect{355,20,60,44}, 0,30,44))
        ->addFrame(new Frame( 6, Rect{425,20,60,44}, 0,30,44))
        ->addFrame(new Frame( 7, Rect{497,21,63,44}, 0,31,44))
        ->addFrame(new Frame( 8, Rect{571,19,60,45}, 0,30,45))
        ->addFrame(new Frame( 9, Rect{11,84,54,44},  0,27,44))
        ->addFrame(new Frame(10, Rect{79,86,49,43},  0,25,43))
        ->addFrame(new Frame(11, Rect{141,87,45,43}, 0,22,43))
        ->addFrame(new Frame(12, Rect{200,87,45,43}, 0,22,43))
        ->addFrame(new Frame(13, Rect{260,89,40,39}, 0,20,39))
        ->addFrame(new Frame(14, Rect{314,89,40,39}, 0,20,39))
        ->addFrame(new Frame(15, Rect{366,90,40,39}, 0,20,39))
        ->addFrame(new Frame(16, Rect{415,91,40,39}, 0,20,39))
        ->addFrame(new Frame(17, Rect{461,90,40,39}, 0,20,39))

        ->addFrame(new Frame(18, Rect{19,161,50,40},  0,25,45))
        ->addFrame(new Frame(19, Rect{80,159,54,41},  0,26,45))
        ->addFrame(new Frame(20, Rect{142,158,57,42}, 0,28,41))
        ->addFrame(new Frame(21, Rect{206,157,59,43}, 0,29,41))
        ->addFrame(new Frame(22, Rect{276,157,60,43}, 0,30,42))
        ->addFrame(new Frame(23, Rect{349,158,61,43}, 0,30,44))
        ->addFrame(new Frame(24, Rect{423,156,59,44}, 0,19,44))
        ->addFrame(new Frame(25, Rect{17,221,54,44},  0,27,44))
        ->addFrame(new Frame(26, Rect{85,223,49,43},  0,24,45))
        ->addFrame(new Frame(27, Rect{147,224,45,43}, 0,23,44))
        ->addFrame(new Frame(28, Rect{207,225,43,41}, 0,22,43))
        ->addFrame(new Frame(29, Rect{266,226,40,39}, 0,20,39))
        ->addFrame(new Frame(30, Rect{320,226,40,39}, 0,20,39))
        ->addFrame(new Frame(31, Rect{375,226,40,39}, 0,20,39))
        ->addFrame(new Frame(32, Rect{421,228,40,39}, 0,20,39))
        ->addFrame(new Frame(33, Rect{468,228,40,39}, 0,20,39))
    };

    _animeBoom = (new Animation())
    ->addSequence(_spriteBoom);

    _level = File::loadJson("data/levelZero.json");

    VAR mapW = 960;
    VAR mapH = 540;

    _grid = new Collision2D::GridSystem(mapW/64,mapH/64, 64);

    _inputConfigJson = File::loadJson("data/gamepad_save_config.json");

    std::string _myShaderVertFileName = _gameConfig["shader"][0]["shockWave"][0];
    std::string _myShaderFragFileName = _gameConfig["shader"][0]["shockWave"][1];

    _myShader = Draw::createShader(_myShaderVertFileName,_myShaderFragFileName);

    if(!_myShader)
        return mlog(" Error in create myShader !\n",1);

    return mlog(" Init Asset OK ! \n",0);
}

int MyGame::doneAsset()
{
    Misc::kill(_animeBoom); // This kill _spriteBoom too !

    return mlog(" Done Asset OK ! \n",0);
}
