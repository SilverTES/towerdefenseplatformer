#include "MyGame.h"

void MyGame::makeClipEnemy()
{
    _clip["enemy"]//{
    ->setActive(true)
    ->setType(CLIP_ENEMY)
//    ->setSize(16,16)
//    ->setPivot(8,8)
//    ->setPosition(_screenW-80,40,2)
    ->setCollidable(true)
    ->setCollideZone(0,Rect{0,0,24,24})

    //->attach<Component::Unit>()
    ->attach(new Component::Unit())
    ->UNIT->setEnergy(20)
    ->UNIT->setEnergyBar(20)
    ->UNIT->setEnergyColor(al_map_rgb(10,220,20))
    ->UNIT->setEnergyColorBG(al_map_rgb(220,10,20))

    //->attach<Component::Jumper>()
    ->attach(new Component::Jumper())
    ->JUMPER->setGravity(0.35)
    ->JUMPER->setTileMap(_clip["tilemap0"]->TILEMAP2D)
    ->JUMPER->setSpeedMax(2,8)
    ->JUMPER->setSpeed(-0.5,0)
    ->JUMPER->setMoveX(0.8)
    ->JUMPER->setSlowMax(0)
    ->JUMPER->setLimitMoveRect(Rect{0,0,(VAR)_screenW,(VAR)_screenW})
    ->setNumber("level",1)
    //->attach<Component::Draggable>()
    ->attach(new Component::Draggable())
    ->setMouse(_mouse)
    //->DRAGGABLE->setLimitRect(Rect{0,0,(float)_screenW,(float)_screenH})
    ->UPDATE(
    {
        _THIS->updateCollideZone(0,_RECT);

        if (_UNIT->_isHit > 0)
        {
            --_UNIT->_isHit;
        }

        ON_MESSAGE()
        {
            ON_MESSAGE_TYPE(DEAD)
            {

                --_clip["stage"]->STAGE->_nbActiveEnemy;

                al_play_sample(_asset->GET_SAMPLE("explosion"), 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                MAKE_CLONE(_clip["explosion"], "cloneExplosion")
                ->setX(_X)
                ->setY(_Y+8);

//                    MAKE_CLONE(_clip["score"], "cloneScore")
//                    ->setX(_X)
//                    ->setY(_Y-16)
//                    ->setNumber("startY",_Y)
//                    ->setNumber("goalY", _Y-24)
//                    ->setString("message","KILL") //+std::to_string(25))
//                    ->setNumber("r",250)
//                    ->setNumber("g",10)
//                    ->setNumber("b",10)
//                    ->setNumber("nbPoint", 25);

                int chance = 0;

                if (_NUMBER("level") == 1) chance = 0;
                if (_NUMBER("level") == 2) chance = 0;
                if (_NUMBER("level") == 3) chance = 0;

                if (Misc::random(1,10)>chance)
                {
                    MAKE_CLONE(_clip["ammo"], "ammo")
                    ->setNumber("nbAmmo",Misc::random(10,10+_NUMBER("level")*8))
                    ->setX(_X)
                    ->setY(_Y);
                }

                KILL_MESSAGE();
                KILL_THIS();
            }

            ON_MESSAGE_TYPE(DAMAGE)
            {
                _UNIT->_isHit = 8;

                int damage = static_cast<MsgData::Damage*>(_MESSAGE->_data)->_damage;
                int damageVX = static_cast<MsgData::Damage*>(_MESSAGE->_data)->_vx;
                _UNIT->_energy -= damage;

                _X += damageVX;

                MAKE_CLONE(_clip["score"], "cloneScore")
                ->setX(_X)
                ->setY(_Y-4)
                ->setString("message",std::to_string(damage))
                ->setNumber("startY",_Y)
                ->setNumber("goalY", _Y-24)
                ->setNumber("r",200)
                ->setNumber("g",50)
                ->setNumber("b",10)
                ->setNumber("nbPoint", damage);
                //auto score = new MsgData::Score(damage);
                //_msgQueue->post(SCORE, score, _clip["hudScore"]);

                KILL_MESSAGE();
            }
        }

        ON_COLLIDE_ZONE(0)
        {

            ON_COLLIDE_ZONE_CLIP_NAME(0,"exit0",0)
            {
                Clip* clip = _PARENT->index(_ID_ZONE_COLLIDE_BY(0));

                _msgQueue->post(DAMAGE, new MsgData::Damage(2), clip );

                _msgQueue->post(DAMAGE, new MsgData::Damage(80), _clip["camera"]);

                al_play_sample(_asset->GET_SAMPLE("gunShot"),
                               0.2, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                _msgQueue->post(DEAD, new MsgData::Dead(), _THIS);
                //KILL_THIS();
                std::cout << "-- ENEMY COLLIDE EXIT -- \n";

                printf("Exit Rect = %.2f, %.2f, %.2f, %.2f \n",
                       clip->_rect._x,
                       clip->_rect._y,
                       clip->_rect._w,
                       clip->_rect._h
                       );

                printf("Enemy Rect = %.2f, %.2f, %.2f, %.2f \n",
                       _RECT._x,
                       _RECT._y,
                       _RECT._w,
                       _RECT._h
                       );

            }
        }


        _DRAGGABLE->update();
        _UNIT->update();


        if (!_DRAGGABLE->_isDrag)
            _JUMPER->update();
        else
            _JUMPER->doFall(); //_JUMPER->_isJump = true;


        if (_JUMPER->_hitL)
            _JUMPER->_vx = _JUMPER->_moveX;

        if (_JUMPER->_hitR)
            _JUMPER->_vx = -_JUMPER->_moveX;


        if (_UNIT->_energy <= 0)
        {
            _msgQueue->post(DEAD, new MsgData::Dead(), _THIS);
        }

        //_THIS->updateClipRect();
    });
//    ->RENDER(
//    {
//
//        int direction = 0;
//        if (_JUMPER->_vx > 0) direction = 1;
//
//        if (_UNIT->_isHit > 0)
//        {
//            al_draw_tinted_bitmap(_asset->GET_BITMAP("mob0"),al_map_rgb(50,200,50),std::floor(_ABSX)-2, std::floor(_ABSY), direction);
//        }
//        else
//        {
//            al_draw_bitmap(_asset->GET_BITMAP("mob0"),std::floor(_ABSX)-2, std::floor(_ABSY), direction);
//        }
//
//        //al_draw_bitmap(_asset->GET_BITMAP("mob0"),std::floor(_ABSX)-2, std::floor(_ABSY), direction);
//
//        _UNIT->render();
//
//        //Draw::rect(_RECT,al_map_rgb(50,150,200),0);
//        _THIS->showRect(al_map_rgba(115,120,0,50))
//        ->showPivot(al_map_rgb(255,0,255),2);
//    });
    //};

    //std::cout << "_clip['enemy'] pointer = " << _clip["enemy"] << "\n";


    _clip["miniAlien"] =
    {
        MAKE_CLONE(_clip["enemy"], "miniAlien")
        ->setActive(true)
        ->setType(CLIP_ENEMY)
        ->UNIT->setEnergy(80)
        ->UNIT->setEnergyMax(80)
        ->UNIT->setEnergyBar(20)
        //->UNIT->setEnergyColor(al_map_rgb(250,50,100))
        ->setSize(16,16)
        ->setPivot(8,8)
        ->setNumber("level",1)
        ->JUMPER->setMoveX(0.8)
        ->JUMPER->setSlowMax(0)

        ->setNumber("tempoJump",0)
        ->setNumber("isJump",0)
        ->UPDATE(
        {
            //_clip["enemy"]->_update(_THIS);

            //_MASTER->_update(_THIS);
            _MASTER->runUpdate(_THIS);

            //_ORIGINAL->_update(_THIS);
            // ne marchera pas parceque le clone cr�er a partir de ce clip aura comme original
            // non pas _clip["enemy"] mais _clip["miniAlien"] !

            ++_NUMBER("tempoJump");

            if (_NUMBER("tempoJump") > 60)
            {
                _NUMBER("tempoJump") = 0;
                _NUMBER("isJump") = 1;

                int randomJump = Misc::random(0,10);

                if (randomJump > 4 && _JUMPER->_isLand)
                {
                    _JUMPER->doJump(-6);
                }
            }

            if (_NUMBER("isJump"))
            {
                if (_JUMPER->_isLand)
                {
                    _NUMBER("isJump") = false;
                }
            }

        })
        ->RENDER(
        {

            int direction = 0;
            if (_JUMPER->_vx > 0) direction = 1;

            if (_UNIT->_isHit > 0)
            {
                al_draw_tinted_bitmap(_asset->GET_BITMAP("mob0"),al_map_rgb(50,200,50),std::floor(_ABSX)-2, std::floor(_ABSY), direction);
            }
            else
            {
                al_draw_bitmap(_asset->GET_BITMAP("mob0"),std::floor(_ABSX)-2, std::floor(_ABSY), direction);
            }

            //al_draw_bitmap(_asset->GET_BITMAP("mob0"),std::floor(_ABSX)-2, std::floor(_ABSY), direction);

            _UNIT->render();

            //Draw::rect(_RECT,al_map_rgb(50,150,200),0);
            _THIS->showRect(al_map_rgba(115,120,0,50))
            ->showPivot(al_map_rgb(255,0,255),2);
        })
    };

    _clip["maxiAlien"] =
    {
        MAKE_CLONE(_clip["enemy"], "maxiAlien")
        ->setActive(true)
        ->setType(CLIP_ENEMY)
        ->UNIT->setEnergy(400)
        ->UNIT->setEnergyMax(400)
        ->UNIT->setEnergyBar(40)
        //->UNIT->setEnergyColor(al_map_rgb(250,50,100))
        ->setSize(32,24)
        ->setPivot(16,12)
        ->setNumber("level",2)
        ->JUMPER->setMoveX(0.4)
        ->JUMPER->setSlowMax(0)
        ->UPDATE(
        {
            //_MASTER->_update(_THIS);
            _MASTER->runUpdate(_THIS);
        })
        ->RENDER(
        {
            int direction = 0;
            if (_JUMPER->_vx > 0) direction = 1;

            if (_UNIT->_isHit > 0)
            {
                al_draw_tinted_bitmap(_asset->GET_BITMAP("snail"),al_map_rgb(20,250,250),std::floor(_ABSX)-4, std::floor(_ABSY), direction);
            }
            else
            {
                al_draw_bitmap(_asset->GET_BITMAP("snail"),std::floor(_ABSX)-4, std::floor(_ABSY), direction);
            }

            //al_draw_bitmap(_asset->GET_BITMAP("snail"),std::floor(_ABSX)-4, std::floor(_ABSY), direction);

            _UNIT->render();

            //Draw::rect(_RECT,al_map_rgb(50,150,200),0);
            _THIS->showRect(al_map_rgba(115,120,0,50))
            ->showPivot(al_map_rgb(255,0,255),2);
        })
    };

    _clip["destructor"] =
    {
        MAKE_CLONE(_clip["enemy"], "destructor")
        ->setActive(true)
        ->setType(CLIP_ENEMY)
        ->UNIT->setEnergy(800)
        ->UNIT->setEnergyMax(800)
        ->UNIT->setEnergyBar(40)
        //->UNIT->setEnergyColor(al_map_rgb(250,150,200))
        ->setSize(32,24)
        ->setPivot(16,12)
        ->setNumber("level",3)
        ->JUMPER->setMoveX(0.6)
        ->JUMPER->setSlowMax(0)
        ->RENDER(
        {
            int direction = 0;
            if (_JUMPER->_vx > 0) direction = 1;

            if (_UNIT->_isHit > 0)
            {
                al_draw_tinted_bitmap(_asset->GET_BITMAP("destructor"),al_map_rgb(250,10,150),std::floor(_ABSX), std::floor(_ABSY)-4, direction);
            }
            else
            {
                al_draw_bitmap(_asset->GET_BITMAP("destructor"),std::floor(_ABSX), std::floor(_ABSY)-4, direction);
            }

            //al_draw_bitmap(_asset->GET_BITMAP("destructor"),std::floor(_ABSX), std::floor(_ABSY)-4, direction);

            _UNIT->render();

            //Draw::rect(_RECT,al_map_rgb(50,150,200),0);
            _THIS->showRect(al_map_rgba(115,120,0,50))
            ->showPivot(al_map_rgb(255,0,255),2);
        })
    };




}
