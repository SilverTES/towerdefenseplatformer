#include "MyGame.h"

void MyGame::makeClipGui()
{
    _clip["toolTip"]//{
//    ->setup(_window,_mainFont,_mouse)
    ->setPosition(0,0,6)
    ->setActive(true)
    ->setNumber("alpha", 0)
    ->setNumber("roundSize",6)
    ->setNumber("arrowX", 0)
    ->setNumber("arrowY", 0)
    ->setNumber("arrowSize",4)
    ->UPDATE(
    {
        ON_FRAME(0)
        {
            _NUMBER("alpha") = 0;
            PAUSE();
        }

        ON_FRAME(2)
        {
            PLAY_AT(1);
        }

        ON_FRAME(3)
        {
            _NUMBER("alpha") = 0;
        }

        ON_FRAME(25)
        {
            _NUMBER("alpha") = 255;
            PAUSE();
        }

        ON_PLAY()
        {
            _NUMBER("alpha") += 10;

            if (_NUMBER("alpha") >= 255) _NUMBER("alpha") = 255;
        }

        GOTO_NEXT_FRAME();

    })
    ->RENDER(
    {
        VAR alpha = _NUMBER("alpha");
        VAR roundSize = _NUMBER("roundSize");
        VAR arrowSize = (VAR)_NUMBER("arrowSize");
        VAR arrowX = (VAR)_NUMBER("arrowX");
        VAR arrowY = (VAR)_NUMBER("arrowY");
        VAR shadowSize = 4;


        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);


        // Shadow of toolTip !
        Draw::roundRectFill(_ABSRECT + Rect{shadowSize,shadowSize,0,0},roundSize,roundSize,al_map_rgba(25,25,25,alpha));

        ON_FRAME(25)
        {
            Draw::triangleFill
            (
                Triangle
                {
                    arrowX, arrowY,
                    _ABSX+_RECT._w/2-arrowSize+shadowSize, _ABSY+_RECT._h/2+shadowSize,
                    _ABSX+_RECT._w/2+arrowSize+shadowSize, _ABSY+_RECT._h/2+shadowSize
                },
                al_map_rgba(25,25,25,alpha)
            );

            Draw::triangleFill
            (
                Triangle
                {
                    arrowX, arrowY,
                    _ABSX+_RECT._w/2+shadowSize, _ABSY+_RECT._h/2-arrowSize+shadowSize,
                    _ABSX+_RECT._w/2+shadowSize, _ABSY+_RECT._h/2+arrowSize+shadowSize
                },
                al_map_rgba(25,25,25,alpha)
            );

//            Draw::lineAA
//            (
//                arrowX, arrowY,
//                _ABSX+_RECT._w/2-arrowSize+shadowSize, _ABSY+_RECT._h/2+shadowSize,
//                RGBA{25,25,25,alpha},
//                0
//            );


        }


        // ToolTip himself !
        ON_FRAME(25)
        {
            Draw::triangleFill
            (
                Triangle
                {
                    arrowX, arrowY,
                    _ABSX+_RECT._w/2-arrowSize, _ABSY+_RECT._h/2,
                    _ABSX+_RECT._w/2+arrowSize, _ABSY+_RECT._h/2
                },
                al_map_rgba(250,250,250,alpha)
            );

            Draw::triangleFill
            (
                Triangle
                {
                    arrowX, arrowY,
                    _ABSX+_RECT._w/2, _ABSY+_RECT._h/2-arrowSize,
                    _ABSX+_RECT._w/2, _ABSY+_RECT._h/2+arrowSize
                },
                al_map_rgba(250,250,250,alpha)
            );

//            Draw::lineAA
//            (
//                arrowX, arrowY,
//                _ABSX+_RECT._w/2-arrowSize, _ABSY+_RECT._h/2,
//                RGBA{250,250,250,alpha},
//                0
//            );
        }


        Draw::roundRectFill(_ABSRECT, roundSize,roundSize,al_map_rgba(250,250,250,alpha));
        Draw::roundRect(_ABSRECT+Rect{1,1,-2,-2}, roundSize,roundSize,al_map_rgba(50,50,50,alpha),0);


        //Draw::line(_ABSX+_RECT._w/2, _ABSY+_RECT._h/2, _NUMBER("arrowX"), _NUMBER("arrowY"),al_map_rgba(250,250,250,200),0);

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
    });
    //}

    _clip["window"]//{
//    ->setup(_window,_mainFont,_mouse)
    ->setType(CLIP_GUI)
    ->setPosition(0,0,6)
    ->setActive(true)

    //->attach<Component::Skin>()
    ->attach(new Component::Skin())
    ->SKIN->setId(0)

//    ->setNumber("r",255)
//    ->setNumber("g",255)
//    ->setNumber("b",255)
//    ->setNumber("alpha",255)
//    ->setNumber("skin",0) // default skin
    ->UPDATE(
    {
        // *** Debug skin mode
        if (KEY_PRESS(ALLEGRO_KEY_PAD_0)) _SKIN->_id = 0;
        if (KEY_PRESS(ALLEGRO_KEY_PAD_1)) _SKIN->_id = 1;
        if (KEY_PRESS(ALLEGRO_KEY_PAD_2)) _SKIN->_id = 2;
        if (KEY_PRESS(ALLEGRO_KEY_PAD_3)) _SKIN->_id = 3;
        if (KEY_PRESS(ALLEGRO_KEY_PAD_4)) _SKIN->_id = 4;
    })
    ->RENDER(
    {

        int ox = 0;
        int oy = 0;

        int w = _RECT._w/8;
        int h = _RECT._h/8;

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);

        Draw::Color color = al_map_rgba( _SKIN->_color._r, _SKIN->_color._g, _SKIN->_color._b, _SKIN->_color._a );

        if (_SKIN->_id == 1)
        {
            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY-8,w,1,_asset->GET_BITMAP("skinGUI"),8+ox,0+oy,8,8, color);
            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY+_RECT._h,w,1,_asset->GET_BITMAP("skinGUI"),8+ox,16+oy,8,8, color);

            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX-8,_ABSY,1,h,_asset->GET_BITMAP("skinGUI"),0+ox,8+oy,8,8, color);
            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX+_RECT._w,_ABSY,1,h,_asset->GET_BITMAP("skinGUI"),16+ox,8+oy,8,8, color);

            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,0+ox,0+oy,8,8,_ABSX-8,_ABSY-8,0);
            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,16+ox,0+oy,8,8,_ABSX+_RECT._w,_ABSY-8,0);

            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,0+ox,16+oy,8,8,_ABSX-8,_ABSY+_RECT._h,0);
            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,16+ox,16+oy,8,8,_ABSX+_RECT._w,_ABSY+_RECT._h,0);

            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY,w,h,_asset->GET_BITMAP("skinGUI"),8+ox,8+oy,8,8, color);
        }

        if (_SKIN->_id == 2)
        {
            ox = 24;

            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY-8,w,1,_asset->GET_BITMAP("skinGUI"),8+ox,0+oy,8,8, color);
            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY+_RECT._h,w,1,_asset->GET_BITMAP("skinGUI"),8+ox,16+oy,8,8, color);

            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX-8,_ABSY,1,h,_asset->GET_BITMAP("skinGUI"),0+ox,8+oy,8,8, color);
            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX+_RECT._w,_ABSY,1,h,_asset->GET_BITMAP("skinGUI"),16+ox,8+oy,8,8, color);

            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,0+ox,0+oy,8,8,_ABSX-8,_ABSY-8,0);
            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,16+ox,0+oy,8,8,_ABSX+_RECT._w,_ABSY-8,0);

            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,0+ox,16+oy,8,8,_ABSX-8,_ABSY+_RECT._h,0);
            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,16+ox,16+oy,8,8,_ABSX+_RECT._w,_ABSY+_RECT._h,0);

            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY,w,h,_asset->GET_BITMAP("skinGUI"),8+ox,8+oy,8,8, color);
        }

        if (_SKIN->_id == 3)
        {
            oy = 24;

            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY-8,w,1,_asset->GET_BITMAP("skinGUI"),8+ox,0+oy,8,8, color);
            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY+_RECT._h,w,1,_asset->GET_BITMAP("skinGUI"),8+ox,16+oy,8,8, color);

            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX-8,_ABSY,1,h,_asset->GET_BITMAP("skinGUI"),0+ox,8+oy,8,8, color);
            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX+_RECT._w,_ABSY,1,h,_asset->GET_BITMAP("skinGUI"),16+ox,8+oy,8,8, color);

            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,0+ox,0+oy,8,8,_ABSX-8,_ABSY-8,0);
            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,16+ox,0+oy,8,8,_ABSX+_RECT._w,_ABSY-8,0);

            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,0+ox,16+oy,8,8,_ABSX-8,_ABSY+_RECT._h,0);
            al_draw_tinted_bitmap_region(_asset->GET_BITMAP("skinGUI"),color,16+ox,16+oy,8,8,_ABSX+_RECT._w,_ABSY+_RECT._h,0);

            Draw::mosaic(Rect{0,0,(VAR)_screenW,(VAR)_screenH},_ABSX,_ABSY,w,h,_asset->GET_BITMAP("skinGUI"),8+ox,8+oy,8,8, color);
        }

        if (_SKIN->_id == 4)
        {
            Draw::Color color2 = al_map_rgba( _SKIN->_color._r, _SKIN->_color._g, _SKIN->_color._b, _SKIN->_color._a/2 );

            al_draw_filled_rounded_rectangle(_ABSX-4, _ABSY-4,_ABSX+_RECT._w+4,_ABSY+_RECT._h+4,4,4,color2);
            al_draw_filled_rounded_rectangle(_ABSX-2, _ABSY-2,_ABSX+_RECT._w+2,_ABSY+_RECT._h+2,4,4, color2);
            al_draw_filled_rounded_rectangle(_ABSX, _ABSY,_ABSX+_RECT._w,_ABSY+_RECT._h,4,4,color);

            //Draw::grid(_RECT, 8, 8, al_map_rgba(0,50,50,32));
        }

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
    });
    //}

    _clip["button"] =
    {
        MAKE_CLONE_ALONE(_clip["window"], "button")
        ->SKIN->setId(2) //  skin 1
        ->setString("label", "")
        ->setString("message","")
        ->setClip("toClip", nullptr)

        //->attach<Component::Gui>()
        ->attach(new Component::Gui())

        //->attach<Component::Loop>()
        ->attach (new Component::Loop())
        ->LOOP->setLoop(0,0,240,20,Component::Loop::LOOP_PINGPONG)
        ->LOOP->start()
        ->UPDATE(
        {
            // *** Debug skin mode
            _clip["window"]->runUpdate(_THIS);
            _LOOP->update();

            _THIS->_isOver = Collision2D::pointRect(Vec2{_mouse->_x,_mouse->_y},_RECT);

            if (nullptr != _PARENT->NAVIGATE)
            {
                if (_PARENT->NAVIGATE->_isNavigate)
                {
                    if ((_mouse->_onClick && _mouse->_button == 1 && _THIS->_isOver) || (_GUI->_onPress && _GUI->_isFocus) )
                    {
                        RUN_FUNCTION(ON_PRESS);
                        _msgQueue->post(BUTTON, new MsgData::Button(2,_STRING("message")), _CLIP("toClip"));
                        al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    }
                }
            }

        })
        ->RENDER(
        {
            _clip["window"]->runRender(_THIS);

            al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);

            Draw::Color labelColor = al_map_rgba(10,100,150,250);
            Draw::Color labelBGColor = al_map_rgba(0,10,100,150);

            if (SAFE_IF(_PARENT->NAVIGATE, _PARENT->NAVIGATE->_isNavigate))
            {

                if (_THIS->_isOver)
                    Draw::rectFill(_RECT+Rect{-4,-4,8,8}, al_map_rgba(55,100,125,150));

                if (_THIS->_isClick)
                    Draw::rectFill(_RECT+Rect{-4,-4,8,8}, al_map_rgba(5,50,120,200));




                if (_THIS->GUI->_isFocus || _THIS->_isClick)
                {
                    labelColor = al_map_rgba(250,250,50,200);
                    labelBGColor = al_map_rgba(200,100,100,150);
                }

                // Decoration Label
                al_draw_textf
                (
                    _mainFont,
                    labelBGColor,
                    _ABSX+_RECT._w/2,_ABSY+_RECT._h/2-5,
                    -1,
                    "%s",
                    _STRING("label").c_str()
                );

                al_draw_textf
                (
                    _mainFont,
                    labelBGColor,
                    _ABSX+_RECT._w/2,_ABSY+_RECT._h/2-7,
                    -1,
                    "%s",
                    _STRING("label").c_str()
                );

                al_draw_textf
                (
                    _mainFont,
                    labelBGColor,
                    _ABSX+_RECT._w/2-1,_ABSY+_RECT._h/2-6,
                    -1,
                    "%s",
                    _STRING("label").c_str()
                );

                al_draw_textf
                (
                    _mainFont,
                    labelBGColor,
                    _ABSX+_RECT._w/2+1,_ABSY+_RECT._h/2-6,
                    -1,
                    "%s",
                    _STRING("label").c_str()
                );

                if (_THIS->GUI->_isFocus)
                    Draw::rect(_RECT+Rect{-4,-4,8,8}, al_map_rgba(255,200,25,_LOOP->_current),0);

            }

            // Label
            al_draw_textf
            (
                _mainFont,
                labelColor,
                _ABSX+_RECT._w/2,_ABSY+_RECT._h/2-6,
                -1,
                "%s",
                _STRING("label").c_str()
            );

            al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
//            DRAW_TEXTF
//            (
//                al_map_rgba(0,100,100,200),
//                _ABSX+_RECT._w/2,_ABSY-18,
//                -1,
//                "%i",
//                _ID
//            );

        })
    };

    _clip["messageBox"] =
    {
        MAKE_CLONE_ALONE(_clip["window"], "messageBox")
        ->setSize(160,80)
        ->setPivot(CENTER)
        ->INIT(
        {
            _NUMBER("rectW") = _RECT._w;
            _NUMBER("rectH") = _RECT._h;

        })
        ->SKIN->setId(4)
        //->attach<Component::Draggable>()
        ->attach(new Component::Draggable())
        ->setMouse(_mouse)
        //->attach<Component::Navigate>()
        ->attach(new Component::Navigate())
        ->NAVIGATE->setNaviState(_ROOT->NAVISTATE)

        ->setNumber("startAlpha",255) // alpha at start
        ->setNumber("endAlpha",255) // alpha at end

        ->setNumber("kill",0)  // kill this messageBox
        ->setNumber("speed",8) // speed of animation
        ->setNumber("show",0)  // show info in messageBox
        // *** debug
        ->setNumber("keyR",0)

        ->setVisible(false)

        ->UPDATE(
        {
            _MASTER->runUpdate(_THIS);

            _DRAGGABLE->update();

            ON_FRAME(0)
            {
                _NUMBER("show") = 0;
            }

            ON_FRAME(2)
            {
                PLAY_AT(1);
            }

            ON_FRAME(10)
            {
                _NUMBER("show") = 1;

                _THIS->setVisible(true);
                al_play_sample(_asset->GET_SAMPLE("message"), 0.6, 0.0, 2.4, ALLEGRO_PLAYMODE_ONCE, NULL);
                _SKIN->_color._a = _NUMBER("startAlpha");
                _NUMBER("maxi") = std::max(_NUMBER("rectW"), _NUMBER("rectH"));
                _NUMBER("X") = _X;
                _NUMBER("Y") = _Y;
                _X = _NUMBER("X") + _OX;
                _Y = _NUMBER("Y") + _OY;
                _RECT._w = 0;
                _RECT._h = 0;

            }

            ON_FRAME(_NUMBER("maxi")/_NUMBER("speed"))
            {
                _NUMBER("show") = 2;
                PAUSE();
            }

            ON_PLAY()
            {
                if (_SKIN->_color._a >= _NUMBER("endAlpha"))
                    _SKIN->_color._a = _NUMBER("endAlpha");
                else
                    _SKIN->_color._a += ( (_NUMBER("endAlpha")-_NUMBER("startAlpha"))/(_NUMBER("maxi")/_NUMBER("speed")));

                _X -= _NUMBER("speed");
                _Y -= _NUMBER("speed");

                if (_X <= _NUMBER("X")) _X = _NUMBER("X");
                if (_Y <= _NUMBER("Y")) _Y = _NUMBER("Y");

                _RECT._w += _NUMBER("speed")*2;
                _RECT._h += _NUMBER("speed")*2;

                if (_RECT._w >= _NUMBER("rectW")) _RECT._w = _NUMBER("rectW");
                if (_RECT._h >= _NUMBER("rectH")) _RECT._h = _NUMBER("rectH");

            }

            GOTO_NEXT_FRAME();

            if (_NUMBER("show") == 0)
            {
                _THIS->setVisible(false);
            }

            if (_NUMBER("show") == 1)
            {

            }

            if (_NUMBER("show") == 2)
            {
                _SKIN->_color._a = _NUMBER("endAlpha");
            }



            if (_NUMBER("kill") == 1)
            {
                //_THIS->del(_CLIP("back")->id());
                // Go back
                _NAVIGATE->toPrevNavigate();

                KILL_THIS();
            }

        })
        ->RENDER(
        {
            _clip["window"]->runRender(_THIS);
        })
    };

    _clip["playerHUD"]//{
//    ->setup(_window,_asset->GET_FONT("gameFont"),_mouse)
    ->setPosition(0,466,10)
    ->setSize(238,74)
    ->setString("name","Player")
    ->setString("avatar","avatar")
    ->setNumber("player", -1) // ID of the player ex : PLAYER_1 !
    ->setNumber("level", 1)
    ->setNumber("nbPoint", 0)
    //->attach<Component::Draggable>()
    ->attach(new Component::Draggable())
    ->setMouse(_mouse)
    ->DRAGGABLE->setLimitRect(Rect{0,0,(VAR)_screenW,(VAR)_screenH})
    //->attach<Component::Loop>()
    ->attach(new Component::Loop())
    ->LOOP->setLoop(100,100,240,5,Component::Loop::LOOP_PINGPONG)
    ->LOOP->start()
    ->UPDATE(
    {

        if (Input::Button::oncePress("start"+_clipPlayer[_NUMBER("player")]->_name,_clipPlayer[_NUMBER("player")]->COMMAND->onButton(PAD_SELECT)) &&
            !_clipPlayer[_NUMBER("player")]->HERO->_isRun)
        {
            _clipPlayer[_NUMBER("player")]->HERO->_isRun = true;
            std::cout << "- Player press start button ! \n";
            al_play_sample(_asset->GET_SAMPLE("start"), 0.8, 0.0, 1.6, ALLEGRO_PLAYMODE_ONCE, NULL);

            VAR startX = 920;
            VAR startY = 440;

            //int playerRole = ROLE_BASIC;
            //int playerRole = ROLE_SNIPER;

            int playerRole = ROLE_BASIC;
            int randomRole = Misc::random(0,4);

            if (randomRole <= 2) playerRole = ROLE_SNIPER;
            if (randomRole > 2) playerRole = ROLE_ATOMISER;


            _clipPlayer[_NUMBER("player")]
            ->setActive(true)
            ->HERO->setRole(playerRole)
            ->HERO->setStartX(startX)
            ->HERO->setStartY(startY)
//                        ->COMMAND->setPlayer(new ::Player(name))
//                        ->COMMAND->setKeyState(_keyState)
//                        ->COMMAND->loadController(_controllerJson[pad])
            ->setPosition(startX, startY, 4)->JUMPER->setJump(true)->JUMPER->doFall()
            ->HERO->_nbAmmo = 400;

        }
        _DRAGGABLE->update();
        _NUMBER("nbPoint") = _clipPlayer[_NUMBER("player")]->HERO->_nbPoint;
    })
    ->RENDER(
    {
        _LOOP->update();

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);

        //Draw::rectFill(_RECT+Rect{0,0,0,-1},al_map_rgba(10,50,60,200));
        //Draw::rect(_RECT+Rect{0,0,0,-1},al_map_rgb(25,125,150),0);

        //al_draw_bitmap(_asset->GET_BITMAP("playerHUD"),_ABSX, _ABSY, 0);

        if (!_clipPlayer[_NUMBER("player")]->HERO->_isRun)
            al_draw_tinted_bitmap(_asset->GET_BITMAP("pressStart"),al_map_rgba(250,250,250,_LOOP->_current), _ABSX, _ABSY, 0);

        //Draw::rectFill(Rect{_ABSX,_ABSY+4,64,64},al_map_rgb(25,25,15));
        //al_draw_bitmap(_asset->GET_BITMAP(_STRING("avatar")),_ABSX+4, _ABSY+4, 0);
        //Draw::rect(Rect{_ABSX,_ABSY+4,64,64},al_map_rgb(25,125,150),0);

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

        al_draw_textf
        (
            _mainFont,
            al_map_rgb(250,200,0),
            _ABSX+74,_ABSY+2,
            0,
            "%s",
            _STRING("name").c_str()
        );

        al_draw_textf
        (
            _mainFont,
            al_map_rgb(50,200,0),
            _ABSX+74,_ABSY+20,
            0,
            "STAGE  : %i",
            _NUMBER("level")
        );

        al_draw_textf
        (
            _mainFont,
            al_map_rgb(100,150,250),
            _ABSX+74,_ABSY+28,
            0,
            "POINTS : %i",
            _NUMBER("nbPoint")
        );

        _THIS->showRect(al_map_rgba(115,120,0,50))
        ->showPivot(al_map_rgb(255,0,255),2);

    });
    //}

    _clip["configGamePad"]//{
//    ->setup(_window,_mainFont,_mouse)
    ->setType(CLIP_GUI)
    ->setPosition(0,0,0)
    ->INIT(
    {
        _CLIP("setup") = MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(48,8)
        ->setString("message","setup")
        ->setString("label", "SETUP")
        ->setClip("toClip", _THIS)
        ->setPosition(128-120, -24);

        _CLIP("ok") = MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(24,8)
        ->setString("message","ok")
        ->setString("label", "OK")
        ->setClip("toClip", _THIS)
        ->setPosition(128-12, -24);


        _CLIP("test") = MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(48,8)
        ->setString("message","test")
        ->setString("label", "TEST")
        ->setClip("toClip", _THIS)
        ->setPosition(128+70, -24);

    })
    //->setActive(false)
    //->setVisible(false)
    //->attach<Component::NaviNode>()
    ->attach(new Component::NaviNode())
    //->attach<Component::Gui>()
    ->attach(new Component::Gui())

    //->attach<Component::ConfigGamePad>()
    ->attach(new Component::ConfigGamePad())
    ->CONFIG_GAMEPAD->initButton()
    ->CONFIG_GAMEPAD->setBitmap(_asset->GET_BITMAP("padSNES"))

    //->attach<Component::Navigate>()
    ->attach(new Component::Navigate())
    ->NAVIGATE->setNaviState(_ROOT->NAVISTATE)

    //->attach<Component::Loop>()
    ->attach(new Component::Loop())
    ->LOOP->setLoop(0,0,240,20,Component::Loop::LOOP_PINGPONG)
    ->LOOP->start()

    ->UPDATE(
    {
        _LOOP->update();
        _NAVIGATE->update();

        if (_GUI->_onPress)
        {
            _NAVIGATE->setNavigate(true,1);
            _PARENT->NAVIGATE->setNavigate(false);
            std::cout << "RUN CONFIG_GAMEPAD \n";
        }

        if (_NAVIGATE->_isNavigate)
        {
            _CLIP("setup")->setVisible(true);
            _CLIP("ok")->setVisible(true);
            _CLIP("test")->setVisible(true);
        }
        else
        {
            _CLIP("setup")->setVisible(false);
            _CLIP("ok")->setVisible(false);
            _CLIP("test")->setVisible(false);
        }

        _NAVIGATE->onConfirmButton(KEY_PRESS(ALLEGRO_KEY_ENTER) || SAFE_IF(_clipPlayer[PLAYER_1],_clipPlayer[PLAYER_1]->COMMAND->onButton(PAD_A)) );

        ON_MESSAGE_TYPE(BUTTON)
        {
            std::string message = static_cast<MsgData::Button*>(_MESSAGE->_data)->_message;

            if (message == "setup")
            {
                _CONFIG_GAMEPAD->recButton();
                _NAVIGATE->setNavigate(false);
                al_play_sample(_asset->GET_SAMPLE("loose"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            }

            if (message == "test")
            {
                _CONFIG_GAMEPAD->testButton();
                _NAVIGATE->setNavigate(false);
                al_play_sample(_asset->GET_SAMPLE("loose"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            }

            if (message == "ok")
            {
                _CONFIG_GAMEPAD->wait();
                _NAVIGATE->setNavigate(false);
                _NAVIGATE->setFocus(-1);
                _PARENT->NAVIGATE->setNavigate(true);
            }

            KILL_MESSAGE();
        }

        if (_CONFIG_GAMEPAD->_isWait) // Not config Controller !
        {

            if (Input::Button::onPress("left"))  _NAVIGATE->toPrevGUI();
            if (Input::Button::onPress("right")) _NAVIGATE->toNextGUI();

            if (_NAVIGATE->onChangeFocus())
                al_play_sample(_asset->GET_SAMPLE("buttonClick"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        }

        if (_CONFIG_GAMEPAD->_isRec || _CONFIG_GAMEPAD->_isTest) // On config controller !
        {

            if (_CONFIG_GAMEPAD->_isRec) // Evite d'enregistrer le bouton de confirmation lors du premier button � enregistrer !
                _CONFIG_GAMEPAD->setButtonExec(_NAVIGATE->isConfirmButton()); // Don't forget this for make a safe record controller button

            _CONFIG_GAMEPAD->update();

            if (_CONFIG_GAMEPAD->_endConfig) // End config gamepad button !
            {
                _CONFIG_GAMEPAD->wait();
                _NAVIGATE->setNavigate(true);
                al_play_sample(_asset->GET_SAMPLE("loose"), 0.8, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            }

            if (_CONFIG_GAMEPAD->_isRecOK) // Record button OK !
            {
                al_play_sample(_asset->GET_SAMPLE("warp"), 0.4, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            }
        }

    })
    ->RENDER(
    {
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);

        if (_THIS->GUI->_isFocus && _PARENT->NAVIGATE->_isNavigate)
        {
            Draw::rect(_RECT+Rect{-6,-6,12,12}, al_map_rgba(250,200,50, 240-_LOOP->_current),0);
            Draw::rect(_RECT+Rect{-4,-4,8,8}, al_map_rgba(250,20,10, _LOOP->_current),0);
        }
        if (_NAVIGATE->_isNavigate)
        {
            Draw::rect(_RECT+Rect{-4,-4,8,8}, al_map_rgba(250,40,20, 100),0);
            //Draw::rectFill(_RECT+Rect{-4,-4,8,8}, al_map_rgba(10,20,50, 200));
        }

        _CONFIG_GAMEPAD->render();

        if (nullptr != _CONFIG_GAMEPAD->_player)
            al_draw_textf
            (
                _mainFont,
                al_map_rgb(255,250,200),
                _ABSX+_OX,
                _ABSY+90,
                -1,
                "- %s -",
                _CONFIG_GAMEPAD->_player->_name.c_str()
    //            "Record : %i",
    //            _CONFIG_GAMEPAD->_isRec

            );

        if (nullptr != _CONFIG_GAMEPAD->_controller)
        {
            if (_CONFIG_GAMEPAD->_controller->isAssignButton() && _CONFIG_GAMEPAD->_isRec)
            {
            al_draw_textf
            (
                _mainFont,
                    al_map_rgb(255,25,0),
                    _ABSX+_OX,
                    _ABSY+_RECT._h+4,
                    -1,
                    "Rec button : %s",
                    Controller::SNESButtonDico[_CONFIG_GAMEPAD->_currentRecButton].c_str()


                );
            }

            if (_CONFIG_GAMEPAD->_isTest)
            {
                al_draw_text
                (
                    _mainFont,
                    al_map_rgb(255,25,0),
                    _ABSX+_OX,
                    _ABSY+_RECT._h+4,
                    -1,
                    "EXIT : HOLD ANY BUTTON or PRESS <L+START>"
                );
            }

        }

//                DRAW_TEXTF
//                (
//                    al_map_rgb(255,25,0),
//                    _ABSX+_OX,
//                    _ABSY+_RECT._h+16,
//                    -1,
//                    "Button Valid = %i , isRecSafe = %i",
//                    _NAVIGATE->getDirectButton("exec"),
//                    _CONFIG_GAMEPAD->_isRecSafe
//
//
//                );


        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

//        DRAW_TEXTF
//        (
//            al_map_rgba(200,100,100,200),
//            _ABSX+_RECT._w/2,_ABSY+_RECT._h+16,
//            -1,
//            "CONFIG_GAMEPAD _isNavigate = %i",
//            _NAVIGATE->_isNavigate
//        );
        // Debug
        if (Collision2D::pointRect(Vec2{_xMouse, _yMouse}, _RECT))
            _THIS->showComponent(_mainFont, _xMouse, _yMouse+32, al_map_rgb(255,25,150));

    });
    //}

    _clip["configPlayer"]//{
//    ->setup(_window,_mainFont,_mouse)
    ->setType(CLIP_GUI) // Important : for parent Navigate to set Component::Gui attributes
    ->setPosition(0,0)
    ->setSize(120,240)
    ->INIT(
    {

        _CLIP("up") = MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(48,8)
        ->setString("message","up")
        ->setString("label", "UP")
        ->setClip("toClip", _THIS)
        ->setPosition(_RECT._w/2-24, 16);


        _CLIP("down") = MAKE_CLONE_ALONE(_clip["button"],"button")
        ->appendTo(_THIS)//->setupParent()
        ->setActive(true)
        ->setSize(48,8)
        ->setString("message","down")
        ->setString("label", "DOWN")
        ->setClip("toClip", _THIS)
        ->setPosition(_RECT._w/2-24, _RECT._h-24);


    })

    //->attach<Component::Loop>()
    ->attach (new Component::Loop())
    ->LOOP->setLoop(0,0,240,20,Component::Loop::LOOP_PINGPONG)
    ->LOOP->start()
    //->attach(new Component::NaviNode())
    //->attach<Component::Gui>()
    ->attach(new Component::Gui())
    //->attach(new Component::Navigate())->NAVIGATE->setNaviState(_ROOT->NAVISTATE)
    //->attach<Component::Draggable>()
    ->attach(new Component::Draggable())
    ->setMouse(_mouse)
    ->DRAGGABLE->setLimitRect(Rect{0,0,(VAR)_screenW,(VAR)_screenH})
    ->UPDATE(
    {
        _LOOP->update();

        if (_GUI->_onPress)
        {
            std::cout << "ConfigPlayer pressed !\n";
        }

        _DRAGGABLE->update();

    })
    ->RENDER(
    {
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);

        if (_GUI->_isFocus)
            Draw::rect(_ABSRECT, al_map_rgba(250,250,25,_LOOP->_current),0);
        else
            Draw::rect(_ABSRECT, al_map_rgba(25,50,25,250),0);

        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
    });
    //}
}
